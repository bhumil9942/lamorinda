<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//WOF API

Route::post('deviceRegistration', 'API\DeviceTokenController@deviceRegistration');
Route::post('signup', 'API\WOFAuthController@signUp');
Route::post('signin', 'API\WOFAuthController@signIn');
Route::post('changepasswordapp', 'API\WOFAuthController@changePassword');
Route::post('forgotpasswordapp', 'API\WOFAuthController@forgotPassword');
Route::post('logoutapp', 'API\WOFAuthController@Logout');
Route::post('uploadimage', 'API\ImageUploadController@uploadImage');
Route::post('create_tourny', 'API\TournamentController@createTournament');
Route::post('getprofile', 'API\WOFAuthController@getProfile');
Route::post('setprofile', 'API\WOFAuthController@setProfile');
Route::post('tournylist', 'API\TournamentController@tournyList');
Route::post('specieslist', 'API\TournamentController@spiceList');
Route::post('contactus', 'API\ContactController@contactUs');
Route::post('jointourny', 'API\TournamentController@joinTournament');
Route::post('follow', 'API\FollowerController@Follow');
Route::post('unfollow', 'API\FollowerController@UnFollow');
Route::post('sendmessage', 'API\ChatController@sendMesssage');
Route::post('mychatlist', 'API\ChatController@myChatList');
Route::post('chatdetails', 'API\ChatController@chatDetails');
Route::post('tourny_details', 'API\TournamentController@tournamentDetails');
Route::post('paymentinfo', 'API\WOFAuthController@paymentInfo');
Route::post('addToWatchList', 'API\TournamentController@addToWatchList');
Route::post('getfollowerslist', 'API\FollowerController@FollowerList');
Route::post('submitfish', 'API\TournamentController@submitYourFish');
Route::post('getattendeeslist', 'API\TournamentController@attendeesList');
Route::post('addrating', 'API\TournamentController@addRating');
Route::post('addresults', 'API\TournamentController@addResults');
Route::post('getnotifications', 'API\TournamentController@getNotificationList');
Route::post('cartsummary', 'API\WOFAuthController@cartSummary');
Route::post('getwallethistory', 'API\WOFAuthController@walletHistory');
Route::post('getawardslist', 'API\TournamentController@getAwardsList');
Route::post('getcontestantslist', 'API\TournamentController@getContestantsList');
Route::post('votenow', 'API\TournamentController@voteNow');
Route::post('alltournylist', 'API\TournamentController@allTournyList');
Route::post('requestpayment', 'API\WOFAuthController@requestPayment');
Route::post('getresults', 'API\TournamentController@getResults');
Route::post('edittourny', 'API\TournamentController@editTournament');
Route::post('deletetourny', 'API\TournamentController@deleteTournament');
Route::get('about_us', 'API\ContactController@aboutUs');
Route::get('faqs', 'API\ContactController@faq');
Route::get('terms_condition', 'API\ContactController@termsCondition');
Route::get('privacy_policy', 'API\ContactController@privacyPolicy');
Route::post('filtertourny', 'API\TournamentController@filterTourny');
Route::post('create-payment', 'API\PaymentController@makePayment');
Route::post('stripe-checkout', 'API\PaymentController@stripeCheckout');
Route::post('getpaymenthistory', 'API\WOFAuthController@paymentHistory');
Route::post('specieslisthome', 'API\TournamentController@speciesListHome');
Route::post('hoster-commission', 'API\TournamentController@HosterCommission');


