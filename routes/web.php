<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//FrontEnd Routes
Route::post('/students/register', 'StudentsController@register')->name('students.register');
Route::post('/students/login', 'StudentsController@login')->name('students.login');
Route::delete('/students/logout', 'StudentsController@logout')->name('students.logout');

//Route::get('dashboard', [RegisterController::class, 'dashboard']);
//Route::get('login', [CustomAuthController::class, 'index'])->name('login');
//Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom');
//Route::get('registration', [CustomAuthController::class, 'registration'])->name('register-user');
//Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom');
//Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');

// Route::get('/registration', function(){
//     return view('common.front.registration');
// });

Route::get('/', function(){
    return view('common.front.homepage');
})->name('home');
Route::get('/homepage', function(){
    return view('common.front.homepage');
});
Route::get('/schedules-maps', function(){
    return view('common.front.schedules-maps');
});
Route::get('/community-links', function(){
    return view('common.front.community-links');
});
Route::get('/public-meetings', function(){
    return view('common.front.public-meetings');
});
Route::get('/safety-information', function(){
    return view('common.front.safety-information');
});

Route::get('cities/{session_key?}',function(){
    return view('common.front.select-your-city');
})->name('cities');
Route::group(['middleware' => 'auth:students'], function () {
    Route::get('{city}/trips/{session_key?}', function () {
        return view('common.front.select-your-trip');
    })->name('trips');

    Route::get('{city}/{trip}/schools/{session_key?}', function () {
        return view('common.front.select-your-school');
    })->name('schools');

    Route::get('{city}/{trip}/{school}/buses/{session_key?}', function () {
        return view('common.front.select-your-bus');
    })->name('buses');

    Route::any('{city}/{trip}/{school}/{bus}/trip_details/{session_key?}', 'BookingsController@step1')->name('trip_details');
    Route::any('{city}/{trip}/{school}/{bus}/student_details/{session_key?}', 'BookingsController@step2')->name('student_details');
    Route::any('{city}/{trip}/{school}/{bus}/booking_details/{session_key?}', 'BookingsController@step3')->name('booking_details');
    Route::any('{city}/{trip}/{school}/{bus}/confirmation_details', 'BookingsController@step4')->name('confirmation_details');
});


//CityRouteStart
Route::get('city', 'API\CityController@cityList')->name('city');
//CityRouteEnd

//SchoolRouteStart
Route::get('/select-your-school/{city_id}', 'API\SchoolController@getSchoolsByCityId');
//SchoolRouteStart

Route::get('/choose-bus-pases/{school_city_id}', 'API\PassTypeController@getPassTypeBySchoolId');
// Route::get('/choose-bus-pases', function(){
//     return view('common.front.choose-bus-pases');
// });
Route::get('/bus-pass-detail-step1/{pass_id}', function(){
    return view('common.front.bus-pass-detail-step1');
});
Route::get('/bus-pass-detail-step2', function(){
    return view('common.front.bus-pass-detail-step2');
});
Route::get('/bus-pass-detail-step3', function(){
    return view('common.front.bus-pass-detail-step3');
});
Route::get('/bus-pass-detail-step4', function(){
    return view('common.front.bus-pass-detail-step4');
});
Route::get('/bus-pass-detail-step4.1', function(){
    return view('common.front.bus-pass-detail-step4.1');
});

//FrontRouteEnd






// BackEnd Routes
Route::get('/admin', 'Auth\LoginController@showLoginForm')->name('home');
Route::get('password-reset-success', function () {
    return view('auth.passwords.reset-success');
});

Route::group(['prefix' => 'admin', 'as' => 'lamorinda.', 'namespace' => 'Backend', 'middleware' => 'auth:mnre'], function () {
    Route::get('/dashboard', 'MnreController@dashboard')->name('dashboard');

    #City Module Start
    Route::get('city', 'CityController@cityList')->name('city');
    Route::any('create-city', 'CityController@createCity')->name('create-city');
    Route::get('edit-city/{id}', 'CityController@editCity')->name('edit-city');
    Route::get('delete-city/{id}', 'CityController@deleteCity')->name('delete-city');
    #City Module End

    #Bus Route Module Start
    Route::get('route', 'BusRouteController@busRouteList')->name('route');
    Route::any('create-route', 'BusRouteController@createRoute')->name('create-route');
    Route::get('edit-route/{id}', 'BusRouteController@editRoute')->name('edit-route');
    Route::get('delete-route/{id}', 'BusRouteController@deleteRoute')->name('delete-route');
    #Bus Route Module End

    #School Module Start
    Route::get('school', 'SchoolController@schoolsList')->name('school');
    Route::any('create-school', 'SchoolController@createSchool')->name('create-school');
    Route::get('edit-school/{id}', 'SchoolController@editSchool')->name('edit-school');
    Route::get('delete-school/{id}', 'SchoolController@deleteSchool')->name('delete-school');
    Route::any('view-school_bus_route/{id}', 'SchoolController@schoolRoutesList')->name('view-school_bus_route');
    #School Module End

    #PassType Module Start
    Route::get('pass_type', 'PassTypeController@passTypeList')->name('pass_type');
    Route::any('create-pass_type', 'PassTypeController@createPassType')->name('create-pass_type');
    Route::get('edit-pass_type/{id}', 'PassTypeController@editPassType')->name('edit-pass_type');
    Route::get('delete-pass_type/{id}', 'PassTypeController@deletePassType')->name('delete-pass_type');
    #PassType Module End

    #Student Module Start
    Route::get('students', 'StudentController@studentsList')->name('students');
    Route::any('create-student', 'StudentController@createStudent')->name('create-student');
    Route::get('edit-student/{id}', 'StudentController@editStudent')->name('edit-student');
    Route::get('delete-student/{id}', 'StudentController@deleteStudent')->name('delete-student');
    Route::get('delete-student/{id}', 'StudentController@deleteStudent')->name('delete-student');
    #Student Module End

    #Booking Module Start
    Route::get('bookings', 'BookingsController@citiesList')->name('bookings');
    Route::any('getStudentsByCityId/{city_id}', 'BookingsController@getStudentsByCityId')->name('getStudentsByCityId');
    Route::get('studentDetails/{student_id}', 'BookingsController@studentDetails')->name('studentDetails');
    #Booking Module End

    #Payment Module Start
    Route::get('payments', 'BookingsController@payments')->name('payments');
    #Payment Module End

    Route::get('frameworks','FrameworkController@index');
    Route::post('frameworks','FrameworkController@store')->name('frameworks.store');

    Route::get('logout', 'MnreController@logout');
    Route::any('changepassword', 'MnreController@changePassword');

});


Route::group(['prefix' => 'ajax'], function () {
    Route::post('passwordcheck','AjaxController@passwordcheck');
});


//Payment
Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
