<?php

return  [
    "specialAreas" => ['SE', 'ST', 'LWE', 'NONE'],
    "apiMessage" => [
    "1000" => "Login Successful!",
    "1002" => "A user with this email address doesn't exist!",
    "1003" => "Incorrect Password!",
    "1004" => "Profile data",
    "1005" => "Logged out successfully",
    "1006" => "Could not log out",
    "1007" => "Dashboard data",
    "1008" => "Validations failed!",
    "1009" => "Projects list!",
    "1010" => "Project Details!",
    "1011" => "Meta data!",
    "1012" => "New system created!",
    "1013" => "System Updated!",
    "1014" => "System Details!",
    "1015" => "Cannot create any more systems in this project!",
    "1016" => "Project submitted for inspection!",
    "1017" => "Systems list!",
    "1018" => "System inspection data saved!",
    "1019" => "Project Inspection data saved!",
    "1020" => "System Inspection Data!",
    "1021" => "Password reset link sent to your email address!",
    "1022" => "No user registered with this email address!",
    "4001" => "Not Authenticated!",
    "4002" => "Not Authorized!",
    "4003" => "Server error"
    ],
    'pv_module_technologies' => [
        'mono' => 'Monocrystalline',
        'poly' => 'Polycrystalline',
        'thfm' => 'Thin Film',
        'othr' => 'Other',
        'NA' => 'N/A'
    ],
    'battery_types' => [
        'TLA' => 'Tubular Lead Acid',
        'VRLA' => 'VRLA',
        'GEL' => 'GEL',
        'NA' => 'N/A'
    ],
    'light_source_types' => [
        'LED' => 'LED',
        'OTH' => 'Other',
        'NA' => 'N/A'
    ],
    'pole_materials' => [
        'HDGP' => 'Hot dip galvanized pipe',
        'OTHR' => 'Other',
        'NA' => 'N/A'
    ]
];
