<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('student_id');
            $table->string('school_year');
            $table->string('date_downloaded');
            $table->string('fname');
            $table->string('lname');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->string('grade');
            $table->string('school');
            $table->string('school_code');
            $table->string('rte');
            $table->string('pass_type');
            $table->string('service_requested');
            $table->string('waitlist');
            $table->string('am_bus_stop');
            $table->string('pm_bus_stop');
            $table->string('p1_fname');
            $table->string('p2_fname');
            $table->string('email');
            $table->string('phone');
            $table->string('alt_phone');
            $table->string('authorize_1');
            $table->string('authorize_2');
            $table->string('authorize_3');
            $table->string('disability');
            $table->string('comments');
            $table->string('monthly_payments');
            $table->string('discount');
            $table->string('free_transportation');
            $table->string('f_payment_type');
            $table->string('f_cc');
            $table->string('f_exp_date');
            $table->string('f_amt_paid');
            $table->string('f_date_payment_processed');
            $table->string('s_payment_type');
            $table->string('s_cc');
            $table->string('s_exp_date');
            $table->string('s_amt_paid');
            $table->string('s_date_payment_processed');
            $table->string('t_payment_type');
            $table->string('t_cc');
            $table->string('t_exp_date');
            $table->string('t_amt_paid');
            $table->string('t_date_payment_processed');
            $table->string('received_by');
            $table->string('refund_requested');
            $table->string('refund_issued');
            $table->string('amt_refunded');
            $table->string('notes');
            $table->string('release_for_required');
            $table->string('release_for_received');
            $table->string('ride_bus_last_yr');
            $table->string('date_printed');
            $table->string('sub_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
