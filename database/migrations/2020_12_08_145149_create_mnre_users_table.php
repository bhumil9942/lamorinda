<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMnreUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mnre_users', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('name', 50);
            $table->string('email', 50)->unique();
            $table->string('password', 200);
            $table->tinyInteger('admin')->length(4)->default(0);
            $table->string('designation', 100)->nullable();
            $table->tinyInteger('status')->length(1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mnre_users');
    }
}
