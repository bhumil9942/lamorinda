<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

	<title><?php echo e(config('app.name', 'ST Laravel')); ?></title>

	<!-- Scripts -->
	
	

	<!-- Fonts -->
	
</head>
<body>
	<div id="app">
		<div class="main">
			<?php echo $__env->yieldContent('content'); ?>
		</div>
	</div>		
</body>
</html><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/layouts/front-app.blade.php ENDPATH**/ ?>