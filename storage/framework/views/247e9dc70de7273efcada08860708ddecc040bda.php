
<?php $__env->startSection('button'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Manage Users'); ?>
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($user->user_id); ?></td>
                            <td><img src="<?php echo e(URL::to('/api/assets/user'). '/'.$user->image); ?>" width="120px" height="70px"></td>
                            <td><?php echo e($user->fullname); ?></td>
                            <td><?php echo e($user->email); ?></td>
                            <td><?php echo e($user->mobile); ?></td>
                            <td>
                                <?php if($user->status == '1'): ?>
                                <a class="btn btn-xs btn-success" href="<?php echo e(url('/'.$prefix.'/updateusersstatus/'.base64_encode($user->user_id))); ?>">Active</a>                                                               
                                <?php else: ?>
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/'.$prefix.'/updateusersstatus/'.base64_encode($user->user_id))); ?>">Blocked</a>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\wof\resources\views/common/wofUsers.blade.php ENDPATH**/ ?>