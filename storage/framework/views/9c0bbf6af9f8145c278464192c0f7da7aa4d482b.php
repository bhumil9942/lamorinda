<!DOCTYPE html>
<html lang="en">

<body>

    <?php echo $__env->make('partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <!-- Hero Section Start-->
    <section class="hero hero--small" style="background-image: url('<?php echo e(asset('images/select-city.png')); ?>');">
        <h1 class="hero__heading"> Select Your School </h1>
    </section>
    <!-- Hero Section End -->

    <!-- Our category Section Start-->
    <section class="our-category">
        <div class="container">
            <div class="our-category__block">
                <h2>Our Category</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text.</p>
            </div>
            <div class="row">
                <?php $__currentLoopData = $schools; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $school): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($loop->iteration % 2 != 0): ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="our-category__column blue-background">
                            <div class="our-category__image">
                                <img src="<?php echo e(URL::asset('/api/assets/user'). '/'.$school->school_image); ?>" height="250" width="350">
                            </div>
                            <div class="our-category__contant">
                            <a href=<?php echo e(url('/choose-bus-pases/'.$school->school_city_id.'/')); ?>> <p><?php echo e($school->school_name); ?> </p>  </a>
                            </div>
                        </div>
                    </div>
                    <?php else: ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="our-category__column yellow-background">
                            <div class="our-category__image">
                                <img src="<?php echo e(URL::asset('/api/assets/user'). '/'.$school->school_image); ?>" height="250" width="350">
                            </div>
                            <div class="our-category__contant">
                            <a href=<?php echo e(url('/choose-bus-pases/'.$school->school_city_id.'/')); ?>> <p><?php echo e($school->school_name); ?> </p>  </a>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </section>
    <!-- Our category Section End-->

    <?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>

</html><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/front/select-your-school.blade.php ENDPATH**/ ?>