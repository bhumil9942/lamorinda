<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<!--Icons-->
<link rel="shortcut icon" href="<?php echo e(asset('images/fav.ico')); ?>" type="image/x-icon" />
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nanum+Gothic&display=swap">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

<link rel="stylesheet" href="<?php echo e(asset('css/_all-skins.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/AdminLTE.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/ionicons.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/iCheck/all.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/common.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/select2.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/dataTables.bootstrap.min.css')); ?>">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"><?php /**PATH C:\xampp\htdocs\wof\resources\views/elements/CommonElements/_head.blade.php ENDPATH**/ ?>