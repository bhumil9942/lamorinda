
<?php $__env->startSection('content'); ?>
<div class="login-box text-center container-fluid">
    <h1>Login</h1>
    <span>For Lamorinda Login</span>
    <div id="loginform" class="mt-30 row">
        <form id="formLogin" action="<?php echo e(route('login')); ?>" method="POST" autocomplete="off"><?php echo e(csrf_field()); ?>

            <div class="form-group has-feedback col-sm-12 pr-0 pl-0">
                <span class="fa fa-user form-control-feedback"></span>
                <input type="email" id="email" class="form-control required mb-15" placeholder="Login email" name="email" autocomplete="off" autofocus>
            </div>
            <div class="form-group has-feedback col-sm-12 pr-0 pl-0">
                <span class="fa fa-lock form-control-feedback"></span>
                <input type="password" id="password" class="form-control required pwcheck" placeholder="Password" name="password" autocomplete="off">
            </div>
            <div class="clearfix"></div>
            <?php if(!env('DEV_ENVIRONMENT')): ?>
                <div class="col-sm-12 p-0">
                    <div class="captcha login-captcha col-sm-12">
                        <?php echo captcha_img('flat'); ?>
                        <i id="refresh-captcha" class="fa fa-refresh pull-right captcha-refresh" aria-hidden="true"></i>
                        <div class="clearfix"></div>
                        <input type="text" id="captcha-input" class="form-control required" name="captcha" placeholder="Captcha">
                    </div>
                </div>
            <?php else: ?>
                <span class="req fs12">Application is in DEV MODE, captcha disabled</span>
            <?php endif; ?>
            <div class="clearfix"></div>
            <div>
                <?php if(count($errors)): ?>
                <div class="alert alert-danger alert-validations text-center mb-0">
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <span><?php echo e($error); ?></span>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <?php endif; ?>
            </div>
            <button type="submit" id="btn-login" class="btn btn-primary">Login</button>
            <div class="m-5"><a href="<?php echo e(route('password.request')); ?>">Forgot your password?</a></div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
    $(function(){
        $('#formLogin').validate();
        $('#refresh-captcha').click(function(){
            let captcha_array = $('.captcha > img').attr('src').split('?');
            let new_captcha = captcha_array[0] + '?' + makeid(8);
            $('.captcha > img').attr('src', new_captcha);
        });
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
    })

</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('styles'); ?>
<style>
    label.error{ bottom: initial; right: 0px; top: 35px; }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/auth/login.blade.php ENDPATH**/ ?>