

<?php $__env->startSection('content'); ?>
<div class="login-box text-center container-fluid">
    <?php if(session('status')): ?>
        <div class="alert alert-success" role="alert">
            <?php echo e(session('status')); ?>

        </div>
    <?php endif; ?>
    <h2>Reset Password</h2>
    <div id="loginform" class="mt-30 row">
        <form method="POST" action="<?php echo e(route('password.email')); ?>">
            <?php echo csrf_field(); ?>
            <div class="form-group has-feedback col-sm-12 pr-0 pl-0">
                <span class="fa fa-user form-control-feedback"></span>
                <input type="email" id="email" class="form-control required" placeholder="Login email" name="email" autocomplete="off" autofocus>
            </div>
            <div class="clearfix"></div>
            <div>
                <?php if(count($errors)): ?>
                <div class="alert alert-danger alert-validations text-left">
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
                <?php endif; ?>
            </div>
            <button type="submit" class="btn btn-primary">
                <?php echo e(__('Send Password Reset Link')); ?>

            </button>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/auth/passwords/email.blade.php ENDPATH**/ ?>