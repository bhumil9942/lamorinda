
<?php $__env->startSection('button'); ?>
<?php if(Auth::getDefaultDriver() == "mnre"): ?>
<!-- <a href="<?php echo e(URL::to('/'.$prefix.'/view-school_bus_route')); ?>" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE BUS ROUTE
</a> -->
<div class="box-header with-border">
    <a href="<?php echo e(URL::to($prefix.'/school')); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
</div>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Manage School Bus Routes'); ?>
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>School Name</th>
                            <th>Route Type</th>
                            <th>Route</th>
                            <th>Alternate Route</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo e($schools['school_id']); ?></td>
                            <td><?php echo e($schools['school_name']); ?></td>
                            <td>
                                <?php if($schools['route_type'] == 1): ?> 
                                    AM
                                <?php elseif($schools['route_type'] == 2): ?>
                                    PM
                                <?php endif; ?>
                            </td>
                            <td><?php echo e($rName); ?></td>
                            <td><?php echo e($aName); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/schoolBusRouteList.blade.php ENDPATH**/ ?>