
<?php echo $__env->make('partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!-- Hero Section Start-->
<section class="hero hero--small" style="background-image: url(images/select-city.png);">
    <h1 class="hero__heading"> Select Your City </h1>
</section>
<!-- Hero Section End -->

<!-- Select Your City Section Start-->
<section class="select-your-city">
    <div class="container">
      <h2 class="yellow-color">WHAT WE OFFER</h2>
      <h2 class="blue-color">Select Your City</h2>
      <div class="row">
      <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $citi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="col-lg-6">
            if(auth->user)
            <a href=<?php echo e(url('select-your-school/'.$citi->city_id.'/')); ?>>
                <div class="select-your-city__column hover">
                  <img src="images/select-school-image.png" class="img-fluid">
                  <div class="select-your-city__button">
                      <div class="btn-white"><?php echo e($citi->city_name); ?></div>                
                  </div>
                </div>
            </a>
            else

            <a href="javascript:;" class="">
                <div class="select-your-city__column hover">
                  <img src="images/select-school-image.png" class="img-fluid">
                  <div class="select-your-city__button">
                      <div class="btn-white"><?php echo e($citi->city_name); ?></div>                
                  </div>
                </div>
            </a>

          </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          
      </div>
    </div>
</section>
<!-- Select Your City Section End -->

<?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/front/select-your-city.blade.php ENDPATH**/ ?>