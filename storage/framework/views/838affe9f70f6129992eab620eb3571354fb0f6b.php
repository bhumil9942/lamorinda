
<?php $__env->startSection('title', 'Dashboard'); ?>
<?php $__env->startSection('content'); ?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua small-box-primary">
            <div class="inner">
                <h3><?php echo e($winner_count); ?></h3>

                <h4>Total Winners</h4>
            </div>
            <div class="icon">
            <i class="ion ion-person-add"></i> 
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow small-box-success">
            <div class="inner">
                <h3><?php echo e($tourny_count); ?></h3>

                <h4>Number Of Tournament</h4>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i> 
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red small-box-danger">
            <div class="inner">
                <h3>$150</h3>

                <h4>Total Commision</h4>
            </div>
            <div class="icon">
                
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>
<!-- /.row -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.mnre', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\wof\resources\views/mnre/dashboard.blade.php ENDPATH**/ ?>