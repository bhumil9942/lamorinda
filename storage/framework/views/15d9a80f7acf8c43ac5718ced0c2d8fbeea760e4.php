
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Add School'); ?>
<?php
    $editable = empty($editable) ? '' : $editable;
?>
<?php $__env->startPush('css'); ?>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<?php $__env->stopPush(); ?>
<div class="row">
    <div class="col-md-12">
        <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="box box-primary">
            <form action="<?php echo e(URL::to('/'.$prefix.'/create-school')); ?>" id="awardRequestForm" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="box-header with-border">
                    <a href="<?php echo e(URL::to($prefix.'/school')); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="school_code"><?php echo e(__('School Code')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="school_code" <?php echo e($editable ?? ''); ?> value="<?php echo e($school['school_code'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="school_name"><?php echo e(__('School Name')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="school_name" <?php echo e($editable ?? ''); ?> value="<?php echo e($school['school_name'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="school_image"><?php echo e(__('School Image')); ?> <span class="error">*</span></label>  
                                <?php if(isset($school->school_image)): ?>
                                    <input type="file" class="form-control" name="school_image" <?php echo e($editable ?? ''); ?> value="<?php echo e($school->school_image); ?>">
                                <?php else: ?>
                                    <input type="file" class="form-control required" name="school_image" <?php echo e($editable ?? ''); ?> value="">
                                <?php endif; ?>
                            </div>
                            <?php if(isset($school->school_image)): ?>
                            <div class="form-group">
                                <img src="<?php echo e(URL::to('/api/assets/user'). '/'.$school->school_image); ?>" height="100" width="200">
                            </div>
                            <?php endif; ?>
                            <br>
                            <div class="form-group">
                                <label for="city_id"><?php echo e(__('City')); ?> <span class="error">*</span></label>  
                                <select class="form-control required" name="city_id"  <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select City </option>
                                    <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($city->id); ?>"  <?php if(($school->city_id ?? '') == $city->id): ?> selected <?php endif; ?>><?php echo e($city->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="school_year"><?php echo e(__('School Year')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="school_year" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Year</option>
                                    <option value="1" <?php if(($school->school_year ?? '') == 1): ?> selected <?php endif; ?>>2022-2023</option>
                                    <option value="2" <?php if(($school->school_year ?? '') == 2): ?> selected <?php endif; ?>>2023-2024</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="route_type"><?php echo e(__('Route Type')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="route_type" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Route Type</option>
                                    <option value="1" <?php if(($school->route_type ?? '') == 1): ?> selected <?php endif; ?>>AM</option>
                                    <option value="2" <?php if(($school->route_type ?? '') == 2): ?> selected <?php endif; ?>>PM</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="route_id"><?php echo e(__('Routes')); ?> <span class="error">*</span></label>  
                                <select class="js-example-basic-single" multiple="multiple" name="route_id[]"  <?php echo e($editable ?? ''); ?>>
                                    <?php $__currentLoopData = $routes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $route): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($route->bus_route_id); ?>"  <?php if(in_array($route->bus_route_id,$rIds)): ?> selected <?php endif; ?>><?php echo e($route->bus_route_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alternate_route_id"><?php echo e(__('Alternate Routes')); ?> <span class="error">*</span></label>  
                                <select class="js-example-basic-single" multiple="multiple" name="alternate_route_id[]"  <?php echo e($editable ?? ''); ?>>
                                    <?php $__currentLoopData = $routes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $route): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($route->bus_route_id); ?>"  <?php if(in_array($route->bus_route_id,$aIds)): ?> selected <?php endif; ?>><?php echo e($route->bus_route_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(isset($school->school_id)): ?>
                    <input type="hidden" name="school_id" value="<?php echo e($school->school_id ?? ''); ?>">
                <?php endif; ?>
                <?php if(isset($school->school_image)): ?>
                    <input type="hidden" name="school_old_image" value="<?php echo e($school->school_image ?? ''); ?>">
                <?php endif; ?>
                <?php if($editable != 'disabled'): ?>
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(function () {
        validator = $('#awardRequestForm').validate();
    });
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
            // placeholder: "Select a Alternate Route",
            // allowClear: true,
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/admin/editSchool.blade.php ENDPATH**/ ?>