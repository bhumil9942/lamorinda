
<?php $__env->startSection('title', 'Notification'); ?>
<?php $__env->startSection('content'); ?>
<?php
    $editable = empty($editable) ? '' : $editable;
?>
<div class="row">
    <div class="col-md-12">
        <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="box box-primary">
            <form action="<?php echo e(URL::to('/'.$prefix.'/send-notification')); ?>" id="notificationRequestForm" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="box-header with-border">
                    <a href="<?php echo e(URL::to($prefix.'/notifications')); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="notification_title"><?php echo e(__('Notification Title')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="notification_title" <?php echo e($editable ?? ''); ?> value="<?php echo e($notification->notification_title ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="description"><?php echo e(__('Description')); ?> <span class="error">*</span></label>
                                <textarea rows="6" class="form-control required" name="description" <?php echo e($editable ?? ''); ?>><?php echo e($notification->description ?? ''); ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(isset($notification->notification_id)): ?>
                    <input type="hidden" name="notification_id" value="<?php echo e($notification->notification_id ?? ''); ?>">
                <?php endif; ?>
                <?php if($editable != 'disabled'): ?>
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(function () {
        validator = $('#notificationRequestForm').validate();
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/createNotification.blade.php ENDPATH**/ ?>