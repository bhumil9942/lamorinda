
<?php $__env->startSection('button'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Manage Tournaments'); ?>
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Start Date & Time</th>
                            <th>End Date & Time</th>
                            <th>Location</th>
                            <th>Host Name</th>
                            <th>Minimum Entry</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $tournaments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tournament): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($tournament->tournament_id); ?></td>
                            <td><img src="<?php echo e(URL::to('/api/assets/user'). '/'.$tournament->tourny_image); ?>" width="120px" height="70px"></td>
                            <td><?php echo e($tournament->tourny_name); ?></td>
                            <td><?php echo e($tournament->start_date." ".$tournament->start_time); ?></td>
                            <td><?php echo e($tournament->end_date." ".$tournament->end_time); ?></td>
                            <td><?php echo e($tournament->tourny_location); ?></td>
                            <td><?php echo e($tournament->tourny_host); ?></td>
                            <td><?php echo e($tournament->tourny_min_entry); ?></td>
                            <td>
                            <a class="btn btn-xs btn-warning" href="<?php echo e(url('/'.$prefix.'/tournament-detail/'.base64_encode($tournament->tournament_id))); ?>">View</a>
                            <a class="btn btn-xs btn-primary" style="display: table; margin: 5px 0;" href="<?php echo e(url('/'.$prefix.'/getattendeeslist/'.base64_encode($tournament['tournament_id']))); ?>">View Contestent</a>
                            
                                <?php if($tournament['result'] == '0'): ?>
                                    <a class="btn btn-warning btn-xs disabled" href="<?php echo e(url('/'.$prefix.'/getresults/'.base64_encode($tournament['tournament_id']))); ?>">View Results</a>                                                             
                                <?php else: ?>
                                    <a class="btn btn-xs btn-success" href="<?php echo e(url('/'.$prefix.'/getresults/'.base64_encode($tournament['tournament_id']))); ?>">View Results</a>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\wof\resources\views/common/tournamentList.blade.php ENDPATH**/ ?>