<!-- Extends template page-->


<!-- Specify content -->
<?php $__env->startSection('content'); ?>

<h3>Cities List</h3>

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <!-- Alert message (start) -->
      <?php if(Session::has('message')): ?>
      <div class="alert <?php echo e(Session::get('alert-class')); ?>">
         <?php echo e(Session::get('message')); ?>

      </div>
      <?php endif; ?>
      <!-- Alert message (end) -->

      <div class='actionbutton'>

         <a class='btn btn-info float-right' href="<?php echo e(URL::to('/'.$prefix.'/city/create/')); ?>">Add</a>

      </div>
      <table class="table" >
        <thead>
          <tr>
            <th width='20%'>City Id</th>
            <th width='40%'>City Name</th>
            <th width='20%'>City Status</th>
            <th width='40%'>Actions</th>
          </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $subjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subject): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           <tr>
              <td><?php echo e($subject->city_id); ?></td>
              <td><?php echo e($subject->city_name); ?></td>
              <td><?php echo e($subject->city_status); ?></td>
              <td>
                 <!-- Edit -->
                 <a href="<?php echo e(URL::to('/'.$prefix.'/city/edit/'.$subject->city_id)); ?>" class="btn btn-sm btn-info">Edit</a>
                 <!-- Delete -->
                 <a href="<?php echo e(URL::to('/'.$prefix.'/city/delete/'.$subject->city_id)); ?>" class="btn btn-sm btn-danger">Delete</a>
              </td>
           </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
     </table>

   </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/city/index.blade.php ENDPATH**/ ?>