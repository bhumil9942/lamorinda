<header class="main-header">
  <!-- Logo -->
  <a href="javascript:void(0)" class="logo">
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg fs22" id="logo_txt">
      <img class="dash-logo" src="<?php echo e(URL::asset('images/logo-inner.png')); ?>">  
    </span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-state="1" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu MobileView">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="">Welcome  <b>WOF</b></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <p>
                <span><b>WOF</b></span><br>
                <span></span><span id="datetime" style="font-size: 12px;line-height: 30px;"></span>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="p-5-10">
                <a href="<?php echo e(url('wof/changepassword')); ?>" class="">Change Password</a>
              </div>
              <div class="p-5-10">
                <a title="Logout" class="req" href="<?php echo e(url('wof/logout')); ?>">Logout  <i class="fa fa-sign-out"></i></a>
              </div>
              <div>
                <span style="float: right !important;font-size: 10px;">
                  <a style="font-size: 10px!important;color: #404040!important;" href="#">Privacy Policy</a>
                   | 
                  <a style="font-size: 10px!important;color: #404040!important;" href="#">Terms of use</a>
                </span>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header><?php /**PATH C:\xampp\htdocs\wof\resources\views/elements/MNREElements/_header.blade.php ENDPATH**/ ?>