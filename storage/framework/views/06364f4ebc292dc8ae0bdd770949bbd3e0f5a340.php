
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Add Bus Route'); ?>
<?php
    $editable = empty($editable) ? '' : $editable;
?>
<div class="row">
    <div class="col-md-12">
        <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="box box-primary">
            <form action="<?php echo e(URL::to('/'.$prefix.'/create-route')); ?>" id="awardRequestForm" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="box-header with-border">
                    <a href="<?php echo e(URL::to($prefix.'/route')); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bus_route_name"><?php echo e(__('Route Name')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="bus_route_name" <?php echo e($editable ?? ''); ?> value="<?php echo e($routes['bus_route_name'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="bus_route_status"><?php echo e(__('Route Status')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="bus_route_status" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Status</option>
                                    <option value="1" <?php if(($routes->bus_route_status ?? '') == 1): ?> selected <?php endif; ?>>Active</option>
                                    <option value="2" <?php if(($routes->bus_route_status ?? '') == 2): ?> selected <?php endif; ?>>In-Active</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="available_pass"><?php echo e(__('Available Pass')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="available_pass" <?php echo e($editable ?? ''); ?> value="<?php echo e($routes['available_pass'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="current_pass"><?php echo e(__('Current Pass')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="current_pass" <?php echo e($editable ?? ''); ?> value="<?php echo e($routes['current_pass'] ?? ''); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(isset($routes->bus_route_id)): ?>
                    <input type="hidden" name="bus_route_id" value="<?php echo e($routes->bus_route_id ?? ''); ?>">
                <?php endif; ?>
                <?php if($editable != 'disabled'): ?>
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(function () {
        validator = $('#awardRequestForm').validate();
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/createBusRoute.blade.php ENDPATH**/ ?>