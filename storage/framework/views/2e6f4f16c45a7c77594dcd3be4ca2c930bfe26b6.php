<!DOCTYPE html>
<html lang="en">

<body>
<?php echo $__env->make('partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <!-- Hero Section Start-->
  <section class="hero hero--small" style="background-image: url(images/Safety-Information.png);">
    <h1 class="hero__heading"> Safety Information </h1>
  </section>
  <!-- Hero Section End -->

 <!-- safty information section -->

<section class="safty-information">
  <div class="container">
    <div class="row">
        <div class="col-lg-7">
          <div class="safty-information__block">
            <div class="heading-patten">
              <h1>About us</h1>
              <h2>Welcome to Lamorinda School Bus</h2>
            </div>
          </div>
          <div class="safty-information__descreption">
            <p>The U.S. federal government recognizes yellow school buses as THE safest 
            mode of ground transportation in America. School buses are eight times 
            safer riding in a school than a car. </p>
            
            <p> This figure jumps even higher when a teenager is the driver.
            According 
            to the National Highway Transportation Safety Administration, 
            58% of child fatalities during normal school travel hours are in cars driven by 
            teenagers. 23% of such fatalities are in cars driven by an adult. Students 
            riding on school buses account for less than 1% of such fatalities. </p>
            
            <p> Simply put, school buses are THE SAFEST mode of transportation for 
            students traveling to and from school. See what the American School 
            Bus Council has to say about school bus safety!</p></div>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-4">
          <div class="safty-information__image">
            <div class="safty-information__img">
              <img src="images/safty-image.png" class="img-fluid">
            </div>
            <div class="yellow-square"></div>
          </div>
        </div>
    </div>
  </div>
</section>

<!-- End safty information section -->

<?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>

</html><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/front/safety-information.blade.php ENDPATH**/ ?>