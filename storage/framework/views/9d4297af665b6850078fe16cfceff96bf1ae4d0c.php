
<?php $__env->startSection('button'); ?>
<?php if(Auth::getDefaultDriver() == "mnre"): ?>
<a href="<?php echo e(URL::to('/'.$prefix.'/send-notification/')); ?>" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE NOTIFICATION
</a>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Manage Notifications'); ?>
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Notification ID</th>
                            <th>Notification Title</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($notes->notification_id); ?></td>
                            <td><?php echo e($notes->notification_title); ?></td>
                            <td><?php echo e($notes->description); ?></td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/'.$prefix.'/edit-notification/'.base64_encode($notes->notification_id))); ?>">Edit</a>
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/'.$prefix.'/delete-notification/'.base64_encode($notes->notification_id))); ?>">Delete</a>
                                <a class="btn btn-xs btn-success" href="<?php echo e(url('/'.$prefix.'/send-all-notification/'.base64_encode($notes->notification_id))); ?>">Send</a>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/notificationList.blade.php ENDPATH**/ ?>