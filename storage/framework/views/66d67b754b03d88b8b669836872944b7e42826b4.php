<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="<?php echo e(request()->is('admin/dashboard') ? 'active' : ''); ?>"><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="<?php echo e(request()->is('admin/city') ? 'active' : ''); ?>"><a href="<?php echo e(url('admin/city')); ?>"><i class="fa fa-building"></i> <span>City</span></a></li>
            <li class="<?php echo e(request()->is('admin/route') ? 'active' : ''); ?>"><a href="<?php echo e(url('admin/route')); ?>"><i class="fa fa-bus"></i> <span>Bus Routes</span></a></li>
            <li class="<?php echo e(request()->is('admin/school') ? 'active' : ''); ?>"><a href="<?php echo e(url('admin/school')); ?>"><i class="fa fa-university"></i> <span>Schools</span></a></li>
            <li class="<?php echo e(request()->is('admin/pass_type') ? 'active' : ''); ?>"><a href="<?php echo e(url('admin/pass_type')); ?>"><i class="fa fa-ticket"></i> <span>Passes</span></a></li>
            <li class="<?php echo e(request()->is('admin/students') ? 'active' : ''); ?>"><a href="<?php echo e(url('admin/students')); ?>"><i class="fa fa-user"></i> <span>Students</span></a></li>
            <li class="treeview"><a href="<?php echo e(url('admin/bookings')); ?>"><i class="fa fa-book"></i> <span>Bookings</span></a>
            <?php 
                $cities = DB::table('cities')->get();
            ?>
                <ul class="treeview-menu">
                    
                <?php foreach($cities as $city){?>
                    <li>
                        <a href="<?php echo e(URL::to('/'.$prefix.'/getStudentsByCityId/'.$city->id)); ?>"><span><?php echo $city->name ?></span></a>
                    </li>
                <?php }?>
                </ul>
            </li>
            <li class="<?php echo e(request()->is('admin/payments') ? 'active' : ''); ?>"><a href="<?php echo e(url('admin/payments')); ?>"><i class="fa fa-cc-stripe"></i> <span>Payments</span></a></li>
        </ul>
    </section>
</aside>
<?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/elements/MNREElements/_side.blade.php ENDPATH**/ ?>