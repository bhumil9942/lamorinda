
<?php $__env->startSection('button'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Wallets'); ?>
<style>
    .not-active {
        cursor: not-allowed;
    }
</style>
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Requested Amount</th>                    
                            <th>To Be Paid Amount</th> 
                            <th>Status</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $wallets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($result->fullname); ?></td>
                            <td><?php echo e("$".$result->requested_amount); ?></td> 
                            <td><?php echo e("$".$result->to_be_paid_amount); ?></td> 
                            <td>
                            <?php if($result->status == '0'): ?>
                                <a class="btn btn-xs btn-success" href="<?php echo e(url('/'.$prefix.'/updateresultstatus/'.base64_encode($result->request_payment_id).'/'.base64_encode($result->user_id))); ?>">PAY</a>                                                               
                            <?php else: ?>
                                <a class="btn btn-xs btn-danger not-active">PAID</a>
                            <?php endif; ?>

                            <a class="btn btn-xs btn-primary" href="<?php echo e(url('/'.$prefix.'/bank-details/'.base64_encode($result->user_id))); ?>">Bank Details</a>

                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\wof\resources\views/common/paymentrequest.blade.php ENDPATH**/ ?>