
<?php $__env->startSection('button'); ?>
<?php if(Auth::getDefaultDriver() == "mnre"): ?>
<a href="<?php echo e(URL::to('/'.$prefix.'/create-city/')); ?>" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE CITY
</a>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Manage Cities'); ?>
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>City ID</th>
                            <th>City Name</th>
                            <th>City Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $citi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($citi->city_id); ?></td>
                            <td><?php echo e($citi->city_name); ?></td>
                            <td>
                                <?php if($citi->city_status == 1): ?> 
                                    Active
                                <?php elseif($citi->city_status == 2): ?>
                                    In-Active
                                <?php endif; ?>
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/'.$prefix.'/edit-city/'.$citi->city_id)); ?>">Edit</a>
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/'.$prefix.'/delete-city/'.$citi->city_id)); ?>">Delete</a>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/cityList.blade.php ENDPATH**/ ?>