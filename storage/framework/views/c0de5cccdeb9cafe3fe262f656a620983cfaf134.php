<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="<?php echo e(request()->is('wof/dashboard') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/dashboard')); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="<?php echo e(request()->is('wof/users') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/users')); ?>"><i class="fa fa-user"></i> <span>WOF USERS</span></a></li>
            <li class="<?php echo e(request()->is('wof/tournaments') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/tournaments')); ?>"><i class="fa fa-trophy"></i> <span>TOURNAMENT LIST</span></a></li>
            <li class="<?php echo e(request()->is('wof/awards') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/awards')); ?>"><i class="fa fa-delicious"></i> <span>AWARDS LIST</span></a></li>
            <li class="<?php echo e(request()->is('wof/notifications') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/notifications')); ?>"><i class="fa fa-bell"></i> <span>SEND NOTIFICATION</span></a></li>
            <li class="<?php echo e(request()->is('wof/spices') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/spices')); ?>"><i class="fa fa-delicious"></i> <span>SPECIES</span></a></li>
            <li class="<?php echo e(request()->is('wof/tournament-commision') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/tournament-commision')); ?>"><i class="fa fa-percent"></i> <span>WOF COMMISION</span></a></li>
            <!--<li class="<?php echo e(request()->is('wof/hoster-commision') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/hoster-commision')); ?>"><i class="fa fa-percent"></i> <span>HOSTER COMMISION</span></a></li>-->
            <li class="<?php echo e(request()->is('wof/contacts') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/contacts')); ?>"><i class="fa fa-address-book"></i> <span>CONTACTS</span></a></li>
            <li class="<?php echo e(request()->is('wof/paymentrequests') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/paymentrequests')); ?>"><i class="fa fa-address-book"></i> <span>WALLET</span></a></li>
            <li class="<?php echo e(request()->is('wof/payment') ? 'active' : ''); ?>"><a href="<?php echo e(url('wof/payment')); ?>"><i class="fa fa-address-book"></i> <span>PAYMENT</span></a></li>
        </ul>
    </section>
</aside>
<?php /**PATH C:\xampp\htdocs\wof\resources\views/elements/MNREElements/_side.blade.php ENDPATH**/ ?>