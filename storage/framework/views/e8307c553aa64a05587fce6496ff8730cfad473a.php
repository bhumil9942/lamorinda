
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="box box-primary">
            <form action="<?php echo e(URL::to('/'.$prefix.'/update-commision')); ?>" id="tournamentRequestForm" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="commission"><?php echo e(__('Tournament Commission (%) ')); ?> <span class="error">*</span></label>
                                <input type="number" class="form-control required" name="commission" 
                                value="<?php if(isset($commission->commission)): ?><?php echo e($commission->commission); ?><?php endif; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(isset($commission->commission_id)): ?>
                    <input type="hidden" name="commission_id" value="<?php echo e($commission->commission_id ?? ''); ?>">
                <?php endif; ?>
                <div class="box-footer">
                    <input type="submit" class="mt-1 btn btn-primary" value="Update">
                </div>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
    $(function () {
        validator = $('#tournamentRequestForm').validate();
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\wof\resources\views/common/tournamentCommission.blade.php ENDPATH**/ ?>