<nav id="homenav" class="navbar navbar-mnre navbar-fixed-top navbar-expand-lg navbar-light bg-light header">
  <div class="container-fluid pl-0">
    <div class="navbar-header col-md-4 col-sm-6 col-xs-6">
      <a class="navbar-brand" target="_blank" href="https://mnre.gov.in">
        <div class="home-logo-holder">
          <img src="<?php echo e(URL::asset('images/logo-inner.png')); ?>">
        </div>
      </a>
    </div>
    <ul class="nav navbar-nav">
      <li><a class="glyphicon glyphicon-align-right visible-xs-block" onclick='OpenMenu();'></a></li>
    </ul>
  </div>
</nav>
<div class="container-fluid mobile-menu">
  <div class="row">
      <div class="col-sm-12 pt-30">
        <button onclick='CloseMenu();'>X</button>
        <ul class="mobile-list">
          <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
          <li><a href="<?php echo e(url('about')); ?>">About</a></li>
          <li><a href="<?php echo e(url('help')); ?>">Help</a></li>
          <li><a href="<?php echo e(url('contact-us')); ?>">Contact Us</a></li>
          <li><a href="<?php echo e(url('login')); ?>">Login</a></li>
        </ul>
      </div>
  </div>
</div><?php /**PATH C:\xampp\htdocs\wof\resources\views/elements/HomeElements/_header.blade.php ENDPATH**/ ?>