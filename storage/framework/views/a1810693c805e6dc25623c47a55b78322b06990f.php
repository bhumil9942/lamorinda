
<!-- Specify content -->
<?php $__env->startSection('content'); ?>

<h3>Edit Subject</h3>

<div class="row">

   <div class="col-md-12 col-sm-12 col-xs-12">

     <!-- Alert message (start) -->
     <?php if(Session::has('message')): ?>
     <div class="alert <?php echo e(Session::get('alert-class')); ?>">
        <?php echo e(Session::get('message')); ?>

     </div>
     <?php endif; ?>
     <!-- Alert message (end) -->

     <div class="actionbutton">

        <a class='btn btn-info float-right' href="<?php echo e(URL::to('/'.$prefix.'/city')); ?>">List</a>

     </div>
     
     <form action="<?php echo e(URL::to('/'.$prefix.'/city/update'.$subject['city_id'])); ?>" method="post" >
<?php echo e(csrf_field()); ?>


       <div class="form-group">
         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Name <span class="required">*</span></label>
         <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="name" class="form-control col-md-12 col-xs-12" name="name" placeholder="Enter subject name" required="required" type="text" value="<?php echo e(old('name',$subject->name)); ?>">

            <?php if($errors->has('name')): ?>
               <span class="errormsg"><?php echo e($errors->first('name')); ?></span>
            <?php endif; ?>
         </div>
       </div>

       <div class="form-group">
         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description
</label>
         <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name='description' id='description' class='form-control' placeholder="Enter description"><?php echo e(old('description',$subject->description)); ?></textarea>

            <?php if($errors->has('description')): ?>
               <span class="errormsg"><?php echo e($errors->first('description')); ?></span>
            <?php endif; ?>
         </div>
       </div>

       <div class="form-group">
          <div class="col-md-6">
            <input type="submit" name="submit" value='Submit' class='btn btn-success'>
          </div>
       </div>

     </form>

   </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/city/edit.blade.php ENDPATH**/ ?>