
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Add School Bus Routes'); ?>
<?php $__env->startPush('css'); ?>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<?php $__env->stopPush(); ?>
<?php
    $editable = empty($editable) ? '' : $editable;
?>
<div class="row">
    <div class="col-md-12">
        <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="box box-primary">
            <form action="<?php echo e(URL::to('/'.$prefix.'/create-school_bus_route/'.$schoolId)); ?>" id="spiceRequestForm" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="box-header with-border">
                    <a href="<?php echo e(URL::to($prefix.'/school')); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <input type="hidden" name="school_id" value="<?php echo e($schoolId); ?>">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="school_id"><?php echo e(__('School Name')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" disabled name="school_id" <?php echo e($editable ?? ''); ?> value="<?php echo e($schools); ?>">
                            </div>
                            <div class="form-group">
                                <label for="route_type"><?php echo e(__('Route Type')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="route_type" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Route Type</option>
                                    <option value="1" <?php if(($schoolBusRoute->route_type ?? '') == 1): ?> selected <?php endif; ?>>AM</option>
                                    <option value="2" <?php if(($schoolBusRoute->route_type ?? '') == 2): ?> selected <?php endif; ?>>PM</option>
                                </select>
                            </div>
                            <!-- <div class="form-group">
                                <label for="route_id"><?php echo e(__('Routes')); ?> <span class="error">*</span></label>  
                                <select class="form-control required" name="route_id"  <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select Route </option>
                                    <?php $__currentLoopData = $routes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $route): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($route->bus_route_id); ?>"  <?php if(($schoolBusRoute->route_id ?? '') == $route->bus_route_id): ?> selected <?php endif; ?>><?php echo e($route->bus_route_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div> -->
                            <div class="form-group">
                                <label for="route_id"><?php echo e(__('Routes')); ?> <span class="error">*</span></label>  
                                <select class="js-example-basic-single" multiple="multiple" name="route_id[]"  <?php echo e($editable ?? ''); ?>>
                                    <?php $__currentLoopData = $routes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $route): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($route->bus_route_id); ?>"  <?php if(in_array($route->bus_route_id,$rIds)): ?> selected <?php endif; ?>><?php echo e($route->bus_route_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alternate_route_id"><?php echo e(__('Alternate Routes')); ?> <span class="error">*</span></label>  
                                <select class="js-example-basic-single" multiple="multiple" name="alternate_route_id[]"  <?php echo e($editable ?? ''); ?>>
                                    <?php $__currentLoopData = $routes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $route): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($route->bus_route_id); ?>"  <?php if(in_array($route->bus_route_id,$aIds)): ?> selected <?php endif; ?>><?php echo e($route->bus_route_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(isset($schoolBusRoute->school_bus_route_id)): ?>
                    <input type="hidden" name="school_bus_route_id" value="<?php echo e($schoolBusRoute->school_bus_route_id ?? ''); ?>">
                <?php endif; ?>
                <?php if($editable != 'disabled'): ?>
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(function () {
        validator = $('#spiceRequestForm').validate();
    });
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
            // placeholder: "Select a Alternate Route",
            // allowClear: true,
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/editSchoolBusRoute.blade.php ENDPATH**/ ?>