
<?php $__env->startSection('button'); ?>
<?php if(Auth::getDefaultDriver() == "mnre"): ?>
<a href="<?php echo e(URL::to('/'.$prefix.'/create-route/')); ?>" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE ROUTE
</a>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Manage Bus Routes'); ?>
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Route ID</th>
                            <th>Route Name</th>
                            <th>Available Pass</th>
                            <th>Current Pass</th>
                            <th>Route Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $routes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $route): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($route->bus_route_id); ?></td>
                            <td><?php echo e($route->bus_route_name); ?></td>
                            <td><?php echo e($route->available_pass); ?></td>
                            <td><?php echo e($route->current_pass); ?></td>
                            <td>
                                <?php if($route->bus_route_status == 1): ?> 
                                    Active
                                <?php elseif($route->bus_route_status == 2): ?>
                                    In-Active
                                <?php endif; ?>
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/'.$prefix.'/edit-route/'.$route->bus_route_id)); ?>">Edit</a>
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/'.$prefix.'/delete-route/'.$route->bus_route_id)); ?>">Delete</a>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/busRouteList.blade.php ENDPATH**/ ?>