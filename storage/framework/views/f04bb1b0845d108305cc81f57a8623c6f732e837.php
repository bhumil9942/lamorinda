<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Lamorinda | Home</title>
		<!--Has all the sylesheets attached already!-->
        <?php echo $__env->make('elements.CommonElements._head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <link rel="stylesheet" href="<?php echo e(URL::asset('css/style.css')); ?>">  
		<!--Custom CSS or CSS Files for particular page-->
		<?php echo $__env->yieldContent('styles'); ?>
	</head>
	<body id="home-body">
		<div>
			<!--Application Header-->
			<?php echo $__env->make('elements.HomeElements._header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<!--Page Content Main-->
			<section id="content">
                <div id="loader" class="overlay">
                    <div class="overlay__inner">
                        <div class="overlay__content">
							<span class="spinner"></span>
							<div class="clearfix mb-15"></div>
							<span class="colorWhite">Processing, Please wait</span>
						</div>
                    </div>
                </div>
            <?php echo $__env->yieldContent('content'); ?>
			</section>
			<?php if(Session::has('msg')): ?>
				<div class="clearfix"></div>
				<?php echo $__env->make('modals.msgModal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<?php endif; ?>
			<div class="clearfix"></div>
			<!--Footer of Application--> 
			<?php echo $__env->make('elements.HomeElements._footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>    
		</div>
		<!--Has all the scripts already attached-->  
		<?php echo $__env->make('elements.CommonElements._base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
		<!--Custom scripts for particular pages-->
		<?php echo $__env->yieldContent('scripts'); ?> 
  </body>
</html><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/layouts/home.blade.php ENDPATH**/ ?>