
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Add City'); ?>
<?php
    $editable = empty($editable) ? '' : $editable;
?>
<div class="row">
    <div class="col-md-12">
        <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="box box-primary">
            <form action="<?php echo e(URL::to('/'.$prefix.'/create-city')); ?>" id="awardRequestForm" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="box-header with-border">
                    <a href="<?php echo e(URL::to($prefix.'/city')); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="city_name"><?php echo e(__('City Name')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="city_name" <?php echo e($editable ?? ''); ?> value="<?php echo e($city['city_name'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="city_status"><?php echo e(__('City Status')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="city_status" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Status</option>
                                    <option value="1" <?php if(($city->city_status ?? '') == 1): ?> selected <?php endif; ?>>Active</option>
                                    <option value="2" <?php if(($city->city_status ?? '') == 2): ?> selected <?php endif; ?>>In-Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(isset($city->city_id)): ?>
                    <input type="hidden" name="city_id" value="<?php echo e($city->city_id ?? ''); ?>">
                <?php endif; ?>
                <?php if($editable != 'disabled'): ?>
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(function () {
        validator = $('#awardRequestForm').validate();
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/admin/createCity.blade.php ENDPATH**/ ?>