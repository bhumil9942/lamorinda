
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Student Information'); ?>
<div class="box box-primary">
<div class="box-header with-border">
<?php $backurl = $prefix.'/tournaments'; ?>
    <a href="<?php echo e(URL::to($backurl)); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
</div>
<div class="row">
    <div class="col-md-12">
        <ul id="installations-tabs" class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#system"><b>Student Details</b></a></li>
            </ul>
            <div class="tab-content">
                <div id="system" class="tab-pane fade in active">
                    <div class="box box-primary">
                        <!-- <div class="box-header with-border">
                        <?php $backurl = $prefix.'/activetournament'; ?>
                            <a href="<?php echo e(URL::to($backurl)); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                        </div> -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament ID <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament->tournament_id); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>User ID <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament->user_id); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Tournament Details</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Name <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament->tourny_name); ?></p>
                                        <label>Location <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament->tourny_location); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Host Name <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament['fullname']); ?></p>
                                        <label>Geo tag <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament->tourny_lat.", ".$tournament->tourny_long); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Image <span class="error">*</span></label>
                                        <p class="mb-25"><img src="<?php echo e(URL::to('/api/assets/user').'/'.$tournament->tourny_image); ?>" height="100" width="200" /></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Start Date & Time <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament->start_date." ".$tournament->start_time); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>End Date & Time <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament->end_date." ".$tournament->end_time); ?></p>
                                    </div>
                                </div>
                                <!-- <div class="col-md-3">
                                    <div class="form-group">
                                       
                                       
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Geo tag <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament->tourny_lat.", ".$tournament->tourny_long); ?></p>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="box-header with-border">
                            <h3 class="box-title">Tournament Specifications</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Minimum Entry <span class="error">*</span></label>
                                        <p class="mb-25">$<?php echo e($tournament->tourny_min_entry); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Type <span class="error">*</span></label>
                                        <?php if($tournament->tourny_type == 1): ?>
                                            <p class="mb-25">Weigh-In</p>
                                        <?php elseif($tournament->tourny_type == 2): ?>
                                            <p class="mb-25">Voting Only</p>
                                        <?php elseif($tournament->tourny_type == 3): ?>
                                            <p class="mb-25">Measurement</p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Spices Name <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament['spice_name']); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Description <span class="error">*</span></label>
                                        <p class="mb-25"><?php echo e($tournament->tourny_desc); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/admin/studentDetails.blade.php ENDPATH**/ ?>