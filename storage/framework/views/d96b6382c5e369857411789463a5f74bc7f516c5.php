
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Add Student'); ?>
<?php
    $editable = empty($editable) ? '' : $editable;
?>
<div class="row">
    <div class="col-md-12">
        <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="box box-primary">
            <form action="<?php echo e(URL::to('/'.$prefix.'/create-student')); ?>" id="awardRequestForm" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="box-header with-border">
                    <a href="<?php echo e(URL::to($prefix.'/students')); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="school_year"><?php echo e(__('School Year')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="school_year" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Year</option>
                                    <option value="1" <?php if(($students->school_year ?? '') == 1): ?> selected <?php endif; ?>>2022-2023</option>
                                    <option value="2" <?php if(($students->school_year ?? '') == 2): ?> selected <?php endif; ?>>2023-2024</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="date_downloaded"><?php echo e(__('Date Downloaded')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="date_downloaded[]" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select Dates </option>
                                    <?php $__currentLoopData = $allDates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $date): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($date); ?>"  <?php if(($dateD ?? '') == $date): ?> selected <?php endif; ?>><?php echo e($date); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="fname"><?php echo e(__('Student First Name')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="fname" <?php echo e($editable ?? ''); ?> value="<?php echo e($students['fname'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="lname"><?php echo e(__('Student Last Name')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="lname" <?php echo e($editable ?? ''); ?> value="<?php echo e($students['lname'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="address"><?php echo e(__('Mailing Address')); ?> <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="address" <?php echo e($editable ?? ''); ?> value="<?php echo e($students['address'] ?? ''); ?>">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="city"><?php echo e(__('City')); ?> <span class="error">*</span></label>  
                                <select class="form-control required" name="city"  <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select City </option>
                                    <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($city->city_id); ?>"  <?php if(($students->city ?? '') == $city->city_id): ?> selected <?php endif; ?>><?php echo e($city->city_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="state"><?php echo e(__('State')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="state" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select State</option>
                                    <option value="1" <?php if(($students->state ?? '') == 1): ?> selected <?php endif; ?>>CA</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="zipcode"><?php echo e(__('Zip Code')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="zipcode[]" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select Zip Code </option>
                                    <?php $__currentLoopData = $zipcodes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($code); ?>"  <?php if(($students->zipcode ?? '') == $code): ?> selected <?php endif; ?>><?php echo e($code); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="grade"><?php echo e(__('Grade(Fall)')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="grade[]" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select Grade </option>
                                    <?php $__currentLoopData = $grades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gra): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($gra); ?>"  <?php if(($students->grade ?? '') == $gra): ?> selected <?php endif; ?>><?php echo e($gra); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="school"><?php echo e(__('School')); ?> <span class="error">*</span></label>  
                                <select class="form-control required" name="school"  <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select School </option>
                                    <?php $__currentLoopData = $schools; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($sch->school_id); ?>"  <?php if(($students->school ?? '') == $sch->school_id): ?> selected <?php endif; ?>><?php echo e($sch->school_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="school_code"><?php echo e(__('School ID')); ?> <span class="error">*</span></label>  
                                <select class="form-control required" name="school_code"  <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select School Code</option>
                                    <?php $__currentLoopData = $schools; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($sch->school_code); ?>"  <?php if(($students->school_code ?? '') == $sch->school_code): ?> selected <?php endif; ?>><?php echo e($sch->school_code); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="rte"><?php echo e(__('Rte #')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="rte[]" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select Rte </option>
                                    <?php $__currentLoopData = $grades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gra): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($gra); ?>"  <?php if(($students->rte ?? '') == $gra): ?> selected <?php endif; ?>><?php echo e($gra); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pass_type"><?php echo e(__('Pass Type (1,2,3)')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="pass_type" <?php echo e($editable ?? ''); ?> value="<?php echo e($students['pass_type'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="service_requested"><?php echo e(__('Service Requested')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="service_requested[]" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select Service Requested </option>
                                    <?php $__currentLoopData = $pass_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($type->pass_id); ?>"  <?php if(($students->service_requested ?? '') == $type->pass_id): ?> selected <?php endif; ?>><?php echo e($type->pass_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="waitlist"><?php echo e(__('Waitlist')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="waitlist" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Waitlist</option>
                                    <option value="1" <?php if(($students->waitlist ?? '') == 1): ?> selected <?php endif; ?>>1</option>
                                    <option value="2" <?php if(($students->waitlist ?? '') == 2): ?> selected <?php endif; ?>>2</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="am_bus_stop"><?php echo e(__('AM Bus Stop')); ?> <span class="error">*</span></label>  
                                <select class="form-control required" name="am_bus_stop"  <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select AM Bus Stop</option>
                                    <?php $__currentLoopData = $routes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $route): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($route->bus_route_id); ?>"  <?php if(($student->am_bus_stop ?? '') == $route->bus_route_id): ?> selected <?php endif; ?>><?php echo e($route->bus_route_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pm_bus_stop"><?php echo e(__('PM Bus Stop')); ?> <span class="error">*</span></label>  
                                <select class="form-control required" name="pm_bus_stop"  <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select PM Bus Stop</option>
                                    <?php $__currentLoopData = $routes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $route): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($route->bus_route_id); ?>"  <?php if(($student->pm_bus_stop ?? '') == $route->bus_route_id): ?> selected <?php endif; ?>><?php echo e($route->bus_route_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="p1_fname"><?php echo e(__('Parent 1 Full Name')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="p1_fname" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['p1_fname'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="p2_fname"><?php echo e(__('Parent 2 Full Name')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="p2_fname" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['p2_fname'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="email"><?php echo e(__('Email Address')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="email" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['email'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="phone"><?php echo e(__('Home Phone')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="phone" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['phone'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="cell_phone"><?php echo e(__('Cell Phone')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="cell_phone" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['cell_phone'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="alt_phone"><?php echo e(__('Alternate Cell Number')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="alt_phone" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['alt_phone'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="authorize_1"><?php echo e(__('Authorized 1')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="authorize_1" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['authorize_1'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="authorize_2"><?php echo e(__('Authorized 2')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="authorize_2" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['authorize_2'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="authorize_3"><?php echo e(__('Authorized 3')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="authorize_3" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['authorize_3'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="disability"><?php echo e(__('Disability')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="disability" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Disability</option>
                                    <option value="1" <?php if(($student->disability ?? '') == 1): ?> selected <?php endif; ?>>Yes</option>
                                    <option value="2" <?php if(($student->disability ?? '') == 2): ?> selected <?php endif; ?>>No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="explain"><?php echo e(__('Explain')); ?> <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="explain" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['explain'] ?? ''); ?>">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="comments"><?php echo e(__('Comments or Special Requests')); ?> <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="comments" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['comments'] ?? ''); ?>">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="initials"><?php echo e(__('Initials')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="initials" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Initials</option>
                                    <option value="1" <?php if(($student->initials ?? '') == 1): ?> selected <?php endif; ?>>1</option>
                                    <option value="2" <?php if(($student->initials ?? '') == 2): ?> selected <?php endif; ?>>2</option>
                                    <option value="3" <?php if(($student->initials ?? '') == 3): ?> selected <?php endif; ?>>3</option>
                                    <option value="4" <?php if(($student->initials ?? '') == 4): ?> selected <?php endif; ?>>4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="monthly_payments"><?php echo e(__('Monthly Payments')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="monthly_payments" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['monthly_payments'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="discount"><?php echo e(__('Discount')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="discount" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['discount'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="free_transportation"><?php echo e(__('Free Transportation')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="free_transportation" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['free_transportation'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="f_payment_type"><?php echo e(__('1st Payment Type')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="f_payment_type" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select 1st Payment Type</option>
                                    <option value="1" <?php if(($student->f_payment_type ?? '') == 1): ?> selected <?php endif; ?>>Credit Card</option>
                                    <option value="2" <?php if(($student->f_payment_type ?? '') == 2): ?> selected <?php endif; ?>>Cheque</option>
                                    <option value="3" <?php if(($student->f_payment_type ?? '') == 3): ?> selected <?php endif; ?>>Cash</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="f_cc"><?php echo e(__('1st CC/Check#')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="f_cc" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['f_cc'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="f_exp_date"><?php echo e(__('Exp Date')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="f_exp_date" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['f_exp_date'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="f_amt_paid"><?php echo e(__('1st Amount Paid')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="f_amt_paid" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select 1st Amount Paid</option>
                                    <option value="1" <?php if(($student->f_amt_paid ?? '') == 1): ?> selected <?php endif; ?>>$225</option>
                                    <option value="2" <?php if(($student->f_amt_paid ?? '') == 2): ?> selected <?php endif; ?>>$325</option>
                                    <option value="3" <?php if(($student->f_amt_paid ?? '') == 3): ?> selected <?php endif; ?>>$125</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="f_date_payment_processed"><?php echo e(__('1st Date Payment Processed')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="f_date_payment_processed" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select 1st Date Payment Processed</option>
                                    <option value="1" <?php if(($student->f_date_payment_processed ?? '') == 1): ?> selected <?php endif; ?>>02/02/2022</option>
                                    <option value="2" <?php if(($student->f_date_payment_processed ?? '') == 2): ?> selected <?php endif; ?>>02/04/2022</option>
                                    <option value="3" <?php if(($student->f_date_payment_processed ?? '') == 3): ?> selected <?php endif; ?>>02/06/2022</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="s_payment_type"><?php echo e(__('2nd Payment Type')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="s_payment_type" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select 2nd Payment Type</option>
                                    <option value="1" <?php if(($student->s_payment_type ?? '') == 1): ?> selected <?php endif; ?>>Credit Card</option>
                                    <option value="2" <?php if(($student->s_payment_type ?? '') == 2): ?> selected <?php endif; ?>>Cheque</option>
                                    <option value="3" <?php if(($student->s_payment_type ?? '') == 3): ?> selected <?php endif; ?>>Cash</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="s_cc"><?php echo e(__('2nd CC/Check#')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="s_cc" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['s_cc'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="s_exp_date"><?php echo e(__('Exp Date 2')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="s_exp_date" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['s_exp_date'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="s_amt_paid"><?php echo e(__('2nd Amount Paid')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="s_amt_paid" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select 2nd Amount Paid</option>
                                    <option value="1" <?php if(($student->s_amt_paid ?? '') == 1): ?> selected <?php endif; ?>>$225</option>
                                    <option value="2" <?php if(($student->s_amt_paid ?? '') == 2): ?> selected <?php endif; ?>>$325</option>
                                    <option value="3" <?php if(($student->s_amt_paid ?? '') == 3): ?> selected <?php endif; ?>>$125</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="s_date_payment_processed"><?php echo e(__('2nd Date Payment Processed')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="s_date_payment_processed" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select 2nd Date Payment Processed</option>
                                    <option value="1" <?php if(($student->s_date_payment_processed ?? '') == 1): ?> selected <?php endif; ?>>02/02/2022</option>
                                    <option value="2" <?php if(($student->s_date_payment_processed ?? '') == 2): ?> selected <?php endif; ?>>02/04/2022</option>
                                    <option value="3" <?php if(($student->s_date_payment_processed ?? '') == 3): ?> selected <?php endif; ?>>02/06/2022</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="t_payment_type"><?php echo e(__('3rd Payment Type')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="t_payment_type" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select 3rd Payment Type</option>
                                    <option value="1" <?php if(($student->t_payment_type ?? '') == 1): ?> selected <?php endif; ?>>Credit Card</option>
                                    <option value="2" <?php if(($student->t_payment_type ?? '') == 2): ?> selected <?php endif; ?>>Cheque</option>
                                    <option value="3" <?php if(($student->t_payment_type ?? '') == 3): ?> selected <?php endif; ?>>Cash</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="t_cc"><?php echo e(__('3rd CC/Check#')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="t_cc" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['t_cc'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="t_exp_date"><?php echo e(__('Exp Date 3')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="t_exp_date" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['t_exp_date'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="t_amt_paid"><?php echo e(__('3rd Amount Paid')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="t_amt_paid" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select 3rd Amount Paid</option>
                                    <option value="1" <?php if(($student->t_amt_paid ?? '') == 1): ?> selected <?php endif; ?>>$225</option>
                                    <option value="2" <?php if(($student->t_amt_paid ?? '') == 2): ?> selected <?php endif; ?>>$325</option>
                                    <option value="3" <?php if(($student->t_amt_paid ?? '') == 3): ?> selected <?php endif; ?>>$125</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="t_date_payment_processed"><?php echo e(__('3rd Date Payment Processed')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="t_date_payment_processed" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select 3rd Date Payment Processed</option>
                                    <option value="1" <?php if(($student->t_date_payment_processed ?? '') == 1): ?> selected <?php endif; ?>>02/02/2022</option>
                                    <option value="2" <?php if(($student->t_date_payment_processed ?? '') == 2): ?> selected <?php endif; ?>>02/04/2022</option>
                                    <option value="3" <?php if(($student->t_date_payment_processed ?? '') == 3): ?> selected <?php endif; ?>>02/06/2022</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="received_by"><?php echo e(__('Received By')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="received_by" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Received By</option>
                                    <option value="1" <?php if(($student->received_by ?? '') == 1): ?> selected <?php endif; ?>>WH</option>
                                    <option value="2" <?php if(($student->received_by ?? '') == 2): ?> selected <?php endif; ?>>DS</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="refund_requested"><?php echo e(__('Refund Requested')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="refund_requested" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['refund_requested'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="refund_issued"><?php echo e(__('Refund Issued')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="refund_issued" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['refund_issued'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="amt_refunded"><?php echo e(__('Amount Refunded')); ?> <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="amt_refunded" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['amt_refunded'] ?? ''); ?>">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="notes"><?php echo e(__('Notes')); ?> <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="notes" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['notes'] ?? ''); ?>">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="release_for_required"><?php echo e(__('Release Form Required')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="release_for_required" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['release_for_required'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="release_for_received"><?php echo e(__('Release Form Received')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="release_for_received" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['release_for_received'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="ride_bus_last_yr"><?php echo e(__('Did this student ride the bus last year?')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="ride_bus_last_yr" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['ride_bus_last_yr'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="date_printed"><?php echo e(__('Date Printed')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="date_printed[]" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected> Select Date Printed </option>
                                    <?php $__currentLoopData = $allDates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $date): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option  value="<?php echo e($date); ?>"  <?php if(($student->date_printed ?? '') == $date): ?> selected <?php endif; ?>><?php echo e($date); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sub_id"><?php echo e(__('Subscription ID')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="sub_id" <?php echo e($editable ?? ''); ?> value="<?php echo e($student['sub_id'] ?? ''); ?>">
                            </div>
                        </div>
                    </div>
                        
                </div>
                <?php if(isset($student->student_id)): ?>
                    <input type="hidden" name="student_id" value="<?php echo e($student->student_id ?? ''); ?>">
                <?php endif; ?>
                <?php if($editable != 'disabled'): ?>
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(function () {
        validator = $('#awardRequestForm').validate();
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/admin/createStudent.blade.php ENDPATH**/ ?>