<?php if(Session::has('success')): ?>
<div id="successmsg" class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  	<b>Success: </b><?php echo e(Session::get('success')); ?>

</div>
<?php endif; ?>

<?php if(Session::has('fail')): ?>
<div id="failmsg" class="alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  	<b>Failure: </b><?php echo e(Session::get('fail')); ?>

</div>
<?php endif; ?>
<script>
	window.setTimeout(function() {
	    $("#successmsg").fadeTo(500, 0).slideUp(500, function(){
	        $(this).remove(); 
	    });
	}, 2000);
	window.setTimeout(function() {
	    $("#failmsg").fadeTo(500, 0).slideUp(500, function(){
	        $(this).remove(); 
	    });
	}, 2000);
</script><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/elements/CommonElements/_flash.blade.php ENDPATH**/ ?>