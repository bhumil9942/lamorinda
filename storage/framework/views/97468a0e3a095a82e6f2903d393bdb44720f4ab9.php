
<?php $__env->startSection('button'); ?>
<?php if(Auth::getDefaultDriver() == "mnre"): ?>
<a href="<?php echo e(URL::to('/'.$prefix.'/create-school/')); ?>" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE SCHOOL
</a>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Manage Schools'); ?>
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>School ID</th>
                            <th>School Code</th>
                            <th>School Name</th>
                            <th>School Image</th>
                            <th>City Name</th>
                            <th>School Year</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $schools; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $school): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($school->school_id); ?></td>
                            <td><?php echo e($school->school_code); ?></td>
                            <td><?php echo e($school->school_name); ?></td>
                            <td><img src="<?php echo e(URL::to('/api/assets/user'). '/'.$school->school_image); ?>" width="120px" height="70px"></td>
                            <td><?php echo e($school->city_name); ?></td>
                            <td>
                                <?php if($school->school_year == 1): ?> 
                                    2022-2023
                                <?php elseif($school->school_year == 2): ?>
                                    2023-2024
                                <?php endif; ?>
                            </td>
                            <!-- <td><?php echo e($school->school_year); ?></td> -->
                            <td>
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/'.$prefix.'/edit-school/'.$school->school_id)); ?>">Edit</a>
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/'.$prefix.'/delete-school/'.$school->school_id)); ?>">Delete</a>
                                <br>
                                <a style="margin-top:4%" class="btn btn-xs btn-success" href="<?php echo e(url('/'.$prefix.'/view-school_bus_route/'.$school->school_id)); ?>">View Routes</a>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/schoolList.blade.php ENDPATH**/ ?>