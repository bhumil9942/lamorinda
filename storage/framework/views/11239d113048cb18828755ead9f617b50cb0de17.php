
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('title', 'Add Pass Types'); ?>
<?php
    $editable = empty($editable) ? '' : $editable;
?>
<div class="row">
    <div class="col-md-12">
        <?php echo $__env->make('elements.CommonElements._flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="box box-primary">
            <form action="<?php echo e(URL::to('/'.$prefix.'/create-pass_type')); ?>" id="spiceRequestForm" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="box-header with-border">
                    <a href="<?php echo e(URL::to($prefix.'/pass_type')); ?>" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="pass_name"><?php echo e(__('Pass Name')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="pass_name" <?php echo e($editable ?? ''); ?> value="<?php echo e($pass_type['pass_name'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="pass_monthly"><?php echo e(__('Price')); ?> <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="pass_monthly" <?php echo e($editable ?? ''); ?> value="<?php echo e($pass_type['pass_monthly'] ?? ''); ?>">
                            </div>
                            <div class="form-group">
                                <label for="pass_status"><?php echo e(__('Status')); ?> <span class="error">*</span></label>
                                <select class="form-control required" name="pass_status" <?php echo e($editable ?? ''); ?>>
                                    <option value="" selected>Select Status</option>
                                    <option value="1" <?php if(($pass_type->pass_status ?? '') == 1): ?> selected <?php endif; ?>>Active</option>
                                    <option value="2" <?php if(($pass_type->pass_status ?? '') == 2): ?> selected <?php endif; ?>>In-Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(isset($pass_type->pass_id)): ?>
                    <input type="hidden" name="pass_id" value="<?php echo e($pass_type->pass_id ?? ''); ?>">
                <?php endif; ?>
                <?php if($editable != 'disabled'): ?>
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(function () {
        validator = $('#spiceRequestForm').validate();
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.'.$layout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/common/createPassType.blade.php ENDPATH**/ ?>