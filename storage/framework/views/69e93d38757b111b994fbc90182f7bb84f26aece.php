<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Lamorinda | <?php echo $__env->yieldContent('title'); ?></title>
		<!--Has all the sylesheets attached already!-->
		<?php echo $__env->make('elements/CommonElements/_head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<link rel="stylesheet" href="<?php echo e(URL::asset('css/adminstyle.css')); ?>">
		<!--Custom CSS or CSS Files for particular page-->
		<?php echo $__env->yieldContent('styles'); ?>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" onload="startTime()">
		<div class="wrapper">
			<!--Application Header-->
			<?php echo $__env->make('elements.MNREElements._header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<!--Application Sidebar-->
			<?php echo $__env->make('elements.MNREElements._side', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<!--Page Content Main-->
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				  <h1>
				    <?php echo $__env->yieldContent('title'); ?><?php echo $__env->yieldContent('button'); ?>
				    <small></small>
				  </h1>
				</section>

				<!-- Main content -->
				<section class="content">
					<div id="loader" class="overlay">
						<div class="overlay__inner">
							<div class="overlay__content">
								<span class="spinner"></span>
								<div class="clearfix mb-15"></div>
								<span class="colorWhite">Processing, Please wait</span>
							</div>
						</div>
					</div>
					<?php echo $__env->yieldContent('content'); ?>
				</section>
			</div>
			<?php if(Session::has('msg')): ?>
				<div class="clearfix"></div>
				<?php echo $__env->make('modals.msgModal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			<?php endif; ?>
		</div>
		<!--Has all the scripts already attached-->
		<?php echo $__env->make('elements/CommonElements/_base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<!--Custom scripts for particular pages-->
		<?php echo $__env->yieldContent('scripts'); ?>
  </body>
</html>
<?php /**PATH C:\xampp\htdocs\lamorinda\resources\views/layouts/mnre.blade.php ENDPATH**/ ?>