$(function () {
    $(".dropdown-toggle").dropdown();
    $(".sidebar-toggle").click(function () {
        if ($(this).data("state") == "1") {
            $("#footer").css("margin-left", "50px");
            $(this).data("state", "0");
        } else {
            $("#footer").css("margin-left", "230px");
            $(this).data("state", "1");
        }
    });
});
/////////////////////////////////////////////////////////////////////////////////////////////////////
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    var d = today.getDate();
    var M = today.getMonth() + 1;
    var y = today.getFullYear();
    document.getElementById("datetime").innerHTML =
        checkTime(d) +
        "-" +
        checkTime(M) +
        "-" +
        y +
        "   " +
        checkTime(h) +
        ":" +
        checkTime(m) +
        ":" +
        checkTime(s);
    var t = setTimeout(startTime, 500);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    } // add zero in front of numbers < 10
    return i;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
function OpenMenu() {
    $(".mobile-menu").css({ display: "block" });
}

function CloseMenu() {
    $(".mobile-menu").css({ display: "none" });
}

$(function () {
    $(".mobileMenuStatus").click(function () {
        $(".mobile-menu").css({ display: "none" });
    });
});
////////////////////////////////////////////////////////////////////////////////////////////////////
function redirectOnLogout() {
    if (localStorage.getItem("active-session") == "Y") {
        $.ajax({
            type: "GET",
            url: baseUrl + "ajax/checksession",
            success: (resp) => {
                if (!resp.success) {
                    location.reload();
                }
            },
        });
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
function makeid(length) {
    var result = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(
            Math.floor(Math.random() * charactersLength)
        );
    }
    return result;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
function mysql_date_format(date) {
    date = date.split("-");
    return date[2] + "-" + date[1] + "-" + date[0];
}
////////////////////////////////////////////////////////////////////////////////////////////////////
function fetchStates(stateSelectBox) {
    ajaxcall("GET", {}, baseUrl + "ajax/fetchStates").then((resp) => {
        setDropdownHtml(resp, stateSelectBox);
    });
}

let setState = (stateSelectBox, state) => {
    ajaxcall("GET", {}, baseUrl + "ajax/fetchStates").then((resp) => {
        setDropdownHtml(resp, stateSelectBox);
        $("#" + stateSelectBox).val(state);
    });
};

function fetchCities(stateSelectBox, districtSelectBox) {
    let state = stateSelectBox.value;
    ajaxcall("GET", {}, baseUrl + "ajax/fetchCities/" + state).then((resp) => {
        setDropdownHtml(resp, districtSelectBox);
    });
}

let setDistrict = (state, districtSelectBox, district) => {
    ajaxcall("GET", {}, baseUrl + "ajax/fetchCities/" + state).then((resp) => {
        setDropdownHtml(resp, districtSelectBox);
        $("#" + districtSelectBox).val(district);
    });
};

function fetchSubDistricts(districtSelectBox, subDistrictSelectBox) {
    let district = districtSelectBox.value;
    ajaxcall("GET", {}, baseUrl + "ajax/fetchSubDistricts/" + district).then(
        (resp) => {
            setDropdownHtml(resp, subDistrictSelectBox);
        }
    );
}

let setSubDistrict = (district, subDistrictSelectBox, subDistrict) => {
    ajaxcall("GET", {}, baseUrl + "ajax/fetchSubDistricts/" + district).then(
        (resp) => {
            setDropdownHtml(resp, subDistrictSelectBox);
            $("#" + subDistrictSelectBox).val(subDistrict);
        }
    );
};

function fetchBlocks(districtSelectBox, blockBox) {
    let district = districtSelectBox.value;
    ajaxcall("GET", {}, baseUrl + "ajax/fetchBlocks/" + district).then(
        (resp) => {
            setDropdownHtml(resp, blockBox);
        }
    );
}

let setBlock = (district, blockBox, block) => {
    ajaxcall("GET", {}, baseUrl + "ajax/fetchBlocks/" + district).then(
        (resp) => {
            setDropdownHtml(resp, blockBox);
            $("#" + blockBox).val(block);
        }
    );
};

function fetchVillages(subDistrictSelectBox, villageBox) {
    let subDistrict = subDistrictSelectBox.value;
    ajaxcall("GET", {}, baseUrl + "ajax/fetchVillages/" + subDistrict).then(
        (resp) => {
            setDropdownHtml(resp, villageBox);
        }
    );
}

let setVillage = (subDistrict, villageBox, village) => {
    ajaxcall("GET", {}, baseUrl + "ajax/fetchVillages/" + subDistrict).then(
        (resp) => {
            setDropdownHtml(resp, villageBox);
            $("#" + villageBox).val(village);
        }
    );
};
////////////////////////////////////////////////////////////////////////////////////////////////////
function setDropdownHtml(dataArray, selectBox) {
    let html = "<option disabled selected>Select</option>";
    $.each(dataArray, (index, node) => {
        html += '<option value="' + node.code + '">' + node.name + "</option>";
    });
    $("#" + selectBox).html(html);
}
////////////////////////////////////////////////////////////////////////////////////////////////////
function ajaxcall(method, params, url) {
    $("#loader").css({ display: "block" });
    return new Promise((resolve, reject) => {
        $.ajax({
            type: method,
            data: params,
            url: url,
            success: (resp) => {
                $("#loader").css({ display: "none" });
                resolve(resp);
            },
            error: (resp) => {
                reject(resp);
            },
        });
    });
}
////////////////////////////////////////////////////////////////////////////////////////////////////
let isNumber = (evt) => {
    evt = evt ? evt : window.event;
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
};
////////////////////////////////////////////////////////////////////////////////////////////////////
let isPercentage = (element) => {
    if (parseInt(element.value) > 100) element.value = 100;
};
////////////////////////////////////////////////////////////////////////////////////////////////////
let makeDocMandatory = (file, label) => {
    $("#" + file).addClass("required");
    $("#" + label).html("*");
};
////////////////////////////////////////////////////////////////////////////////////////////////////
let makeDocNonMandatory = (file, label) => {
    $("#" + file)
        .removeClass("required")
        .removeClass("error");
    $("#" + label).html("");
    $("#" + file + "-error").remove();
};

passwordStrengthValidity = false;
$("#password, #new_password").passwordValidation(
    {},
    function (element, valid, match, failedCases) {
        let html =
            '<span>Password must contain at least:</span><p style="list-style:none;">';
        $.each(failedCases, (indx, node) => {
            html +=
                '<span class="req"><i class="fa fa-caret-right" aria-hidden="true"></i>   ' +
                node +
                "</span><br>";
        });
        html += "</p>";
        if (failedCases.length) {
            passwordStrengthValidity = false;
            $("#errors").html(html);
        } else {
            $('[data-toggle="popover"]').popover("hide");
            passwordStrengthValidity = true;
        }
    }
);
$.validator.addMethod(
    "passwordStrength",
    function (value, element) {
        return passwordStrengthValidity;
    },
    "The password isn't strong enough"
);
