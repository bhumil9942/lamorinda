$("#sanctionSelectBox").change(function () {
    let sanctionId = $(this).val();
    if (sanctionId === "all") {
        location.reload();
    }
    fetchSanctionProjects(sanctionId);
    $("#districtSelectBox").select2("destroy").val("all").select2();
    $("#villageSelectBox").val(null).empty().select2("destroy");
    $("#villageSelectBox")
        .append('<option value="all">All Villages</option>')
        .select2();
});

$("#districtSelectBox").change(function () {
    let districtId = $(this).val();
    fetchDistrictProjects(districtId);
});

$("#villageSelectBox").change(function () {
    let villageId = $(this).val();
    fetchVillageProjects(villageId);
});

function fetchSanctionProjects(sanctionId) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchSanctionProjects/" + btoa(sanctionId)
    ).then((resp) => {
        createProjectsTable(resp);
    });
}

function fetchDistrictProjects(districtId) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchDistrictProjects/" + btoa(districtId)
    ).then((resp) => {
        createProjectsTable(resp);
        fetchVillagesByDistrict(districtId, "villageSelectBox");
    });
}

function fetchVillagesByDistrict(district, villageBox) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchVillagesByDistrict/" + district
    ).then((resp) => {
        let html = '<option value="all">All Villages</option>';
        $.each(resp, (index, node) => {
            html +=
                '<option value="' + node.code + '">' + node.name + "</option>";
        });
        $("#villageSelectBox").html(html);
    });
}

function fetchVillageProjects(villageId) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchVillageProjects/" + btoa(villageId)
    ).then((resp) => {
        createProjectsTable(resp);
    });
}

function createProjectsTable(projects) {
    var html = "";
    $(".listtable").DataTable().clear().destroy();
    $.each(projects, (projectIndex, projectValue) => {
        html += "<tr>";
        html += "<td>" + projectValue.project_code + "</td>";
        html += "<td>" + projectValue.district + "</td>";
        html += "<td>" + projectValue.village + "</td>";
        html += "<td>" + projectValue.installer + "</td>";
        html += "<td>" + projectValue.allocated_ssl + "</td>";
        html += "<td>" + projectValue.installed_ssl + "</td>";
        html += "<td>" + projectValue.status + "</td>";
        html += "<td>";
        html +=
            '<a class="btn btn-xs btn-success" href="' +
            baseUrl +
            authUserType +
            "/project-detail/" +
            btoa(projectValue.id) +
            '">View</a>';
        if (authUserType == "state-agency") {
            if (projectValue.statusNo != 17)
                html +=
                    '<a class="btn btn-xs btn-primary ml-2" href="' +
                    baseUrl +
                    authUserType +
                    "/edit-project/" +
                    btoa(projectValue.id) +
                    '">Edit</a>';
        }
        html += "</td>";
    });

    $("#projectTable").html(html);
    $(".listtable").DataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: false,
        pageLength: 50,
    });
}
