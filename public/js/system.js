$("#sanctionSelectBox").change(function () {
    let sanctionId = $(this).val();
    if (sanctionId === "all") {
        location.reload();
    }
    if (authUserType == "state-agency") {
        fetchSystemsBySanction(sanctionId);
        fetchProjects(sanctionId);
    } else {
        fetchSystemsBySanction(sanctionId);
    }

    $("#stateSelectBox").select2("destroy").val("all").select2();
    $("#districtSelectBox").select2("destroy").val("all").select2();
    $("#villageSelectBox").val(null).empty().select2("destroy");
    $("#villageSelectBox")
        .append('<option value="all">All Villages</option>')
        .select2();
});

$("#projectSelectBox").change(function () {
    let projectId = $(this).val();
    fetchSystemsByProject(projectId);
});

$("#stateSelectBox").change(function () {
    let stateId = $(this).val();
    fetchSystemsByState(stateId);
    ajaxcall("GET", {}, baseUrl + "ajax/fetchCities/" + stateId).then(
        (resp) => {
            let html = '<option value="all">All District</option>';
            $.each(resp, (index, node) => {
                html +=
                    '<option value="' +
                    node.code +
                    '">' +
                    node.name +
                    "</option>";
            });
            $("#districtSelectBox").html(html);
            $("#villageSelectBox").val(null);
            $("#villageSelectBox").select2("destroy").val("all").select2();
        }
    );
});

$("#districtSelectBox").change(function () {
    let districtId = $(this).val();
    fetchSystemsByDistrict(districtId);
});

$("#villageSelectBox").change(function () {
    let villageId = $(this).val();
    fetchSystemsByVillage(villageId);
});

function fetchProjects(sanctionId) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchSanctionProjects/" + btoa(sanctionId)
    ).then((resp) => {
        html = "";
        if (resp.length) {
            html += '<option value="all">All Project</option>';
            $.each(resp, (index, node) => {
                html +=
                    '<option value="' +
                    node.id +
                    '">' +
                    node.project_code +
                    "</option>";
            });
        } else {
            html +=
                '<option value="" selected disabled>Select Project</option>';
        }
        $("#projectSelectBox").html(html);
    });
}

function fetchSystemsBySanction(sanctionId) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchSystemsBySanction/" + btoa(sanctionId)
    ).then((resp) => {
        createSystemsTable(resp);
    });
}

function fetchSystemsByProject(projectId) {
    if (projectId === "all") {
        sanctionId = $("#sanctionSelectBox option:selected").val();
        fetchSystemsBySanction(sanctionId);
    } else {
        ajaxcall(
            "GET",
            {},
            baseUrl + "ajax/fetchSystemsByProject/" + btoa(projectId)
        ).then((resp) => {
            createSystemsTable(resp);
        });
    }
}

function fetchSystemsByState(stateId) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchSystemsByState/" + btoa(stateId)
    ).then((resp) => {
        createSystemsTable(resp);
    });
}

function fetchSystemsByDistrict(districtId) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchSystemsByDistrict/" + btoa(districtId)
    ).then((resp) => {
        createSystemsTable(resp);
        fetchVillagesByDistrict(districtId, "villageSelectBox");
    });
}

function fetchSystemsByVillage(villageId) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchSystemsByVillage/" + btoa(villageId)
    ).then((resp) => {
        createSystemsTable(resp);
    });
}

function createSystemsTable(systems) {
    var html = "";
    $(".listtable").DataTable().clear().destroy();
    $.each(systems, (systemIndex, systemValue) => {
        html += "<tr>";
        html += "<td>" + systemValue.system_code + "</td>";
        if (authUserType == "mnre") {
            html += "<td>" + systemValue.state + "</td>";
        } else if (authUserType == "state-agency") {
            html += "<td>" + systemValue.state + "</td>";
        }
        html += "<td>" + systemValue.district + "</td>";
        html += "<td>" + systemValue.block + "</td>";
        html += "<td>" + systemValue.village + "</td>";
        if (systemValue.approval_date !== null) {
            html += "<td>" + systemValue.approval_date + "</td>";
        } else {
            html += "<td>---</td>";
        }
        html += "<td>" + systemValue.address_landmark + "</td>";
        html += "<td>" + systemValue.pin_code + "</td>";
        if (systemValue.latitude !== null) {
            var lat = systemValue.latitude;
        } else {
            var lat = "N/A";
        }
        if (systemValue.longitude !== null) {
            var lon = systemValue.longitude;
        } else {
            var lon = "N/A";
        }
        html += "<td>" + lat + "," + lon + "</td>";
        html += "<td>" + systemValue.inspector_name + "</td>";
        html +=
            "<td>" +
            (systemValue.status == 1 ? "Installed" : "Pending") +
            "</td>";
        html += "<td>";
        html +=
            '<a class="btn btn-xs btn-success" href="' +
            baseUrl +
            authUserType +
            "/system-detail/" +
            btoa(systemValue.id) +
            '">View</a>';
        html += "</td>";
    });

    $("#systemTable").html(html);
    $(".listtable").DataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: false,
        pageLength: 50,
    });
}

function fetchVillagesByDistrict(district, villageBox) {
    ajaxcall(
        "GET",
        {},
        baseUrl + "ajax/fetchVillagesByDistrict/" + district
    ).then((resp) => {
        let html = '<option value="all">All Villages</option>';
        $.each(resp, (index, node) => {
            html +=
                '<option value="' + node.code + '">' + node.name + "</option>";
        });
        $("#villageSelectBox").html(html);
    });
}
