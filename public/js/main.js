

// Slider-js start//

$(document).ready(function(){
jQuery('.slider__block').slick({
  arrows: false,
  centerMode: true, 
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1400,
      settings: {
        arrows: false,
        centerMode: true,        
        slidesToShow: 3
      }
    },
    {
      breakpoint: 1200,
      settings: {
        arrows: false,
        centerMode: true,        
        slidesToShow: 2
      }
    },
    {
      breakpoint: 991,
      settings: {
        arrows: false,
        centerMode: true,        
        slidesToShow: 1
      }
    }
  ]
});
});
// Slider-js end//


//login pop up js start//

$('.loginButton').click(function(e) {
  e.stopPropagation();
  $('#customer-login-from').fadeIn('slow');
  $('body').append('<div class="pageOverlay"></div>')
});

$('.closeButton').on('click', function() {
  $('#customer-login-from').fadeOut();
  $('.pageOverlay').fadeOut();
});

$(document).ready(function() {
$("#register-btn").click(function() {
  $(".login").hide();
  $("#forget-password").hide();
  $(".register").show();
});


$("#login-btn1").click(function() {
  $(".register").hide();
  $(".login").show();
});
});

//Login pop up js end//


$('.btn-1').click(function(e) {
  e.stopPropagation();
  $('#student-pass-popup').fadeIn('slow');
  $('body').append('<div class="pageOverlay"></div>')
});

$('.closeButton').on('click', function() {
  $('#student-pass-popup').fadeOut();
  $('.pageOverlay').fadeOut();
});