<?php



namespace App\Providers;



use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\View;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Schema;



class AppServiceProvider extends ServiceProvider

{

    /**

     * Register any application services.

     *

     * @return void

     */

    public function register()

    {

        //

    }



    /**

     * Bootstrap any application services.

     *

     * @return void

     */

    public function boot()

    {

        View::composer('*', function ($view) {

            if(Auth::guard('mnre')->check()){

                $layout = 'mnre';

                $prefix = 'admin';

            }else{

                $layout = null;

                $prefix = null;

            }

            $view->with(['layout' => $layout, 'prefix' => $prefix]);

        });



        if(env('APP_DEBUG')) {

                DB::listen(function($query) {

                    File::append(

                        storage_path('/logs/query.log'),

                        $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL

                );

            });

        }



        Schema::defaultStringLength(191);

    }

}

