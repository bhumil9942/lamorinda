<?php

namespace App\Providers;

use App\Models\System;
use App\Models\Project;
use App\Models\Sanction;
use App\Models\Inspector;
use App\Models\Installer;
use App\Policies\SystemAccessPolicy;
use Illuminate\Support\Facades\Gate;
use App\Policies\ProjectAccessPolicy;
use App\Policies\SanctionAccessPolicy;
use App\Policies\InspectorAccessPolicy;
use App\Policies\InstallerAccessPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Installer::class => InstallerAccessPolicy::class,
        Inspector::class => InspectorAccessPolicy::class,
        Sanction::class => SanctionAccessPolicy::class,
        Project::class => ProjectAccessPolicy::class,
        System::class => SystemAccessPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
    }
}
