<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SanctionRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $sanction;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sanction)
    {
        $this->sanction = $sanction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $locationsPdf = json_decode($this->sanction->locations, true)['name'];
        if($this->sanction->toMail === 'state-agency'){
            return $this->subject('Sanction Request Receipt Confirmation')
            ->attachFromStorageDisk('filestore', '\\sanctions\\'.$this->sanction->id.'\\'.$locationsPdf, 'locations.pdf')
            ->markdown('emails.sanctionRequest');
        }else{
            return $this->subject('Receipt of Sanction Request')
            ->attachFromStorageDisk('filestore', '\\sanctions\\'.$this->sanction->id.'\\'.$locationsPdf, 'locations.pdf')
            ->markdown('emails.sanctionRequest');
        }
    }
}
