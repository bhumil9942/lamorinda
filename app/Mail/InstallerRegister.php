<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InstallerRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $installer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($installer)
    {
        $this->installer = $installer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Registration Confirmation for MNRE Solar Street Light Monitoring Web Portal')->markdown('emails.installerRegister');
    }
}
