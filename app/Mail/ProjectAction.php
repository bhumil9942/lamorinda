<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProjectAction extends Mailable
{
    use Queueable, SerializesModels;

    public $project, $logs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project, $logs)
    {
        $this->project = $project;
        $this->logs = $logs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->project->action == 'approve'){
            return $this->subject('Project Approval Confirmation from State Nodal Agency ('.$this->project->project_code.')')->markdown('emails.projectAction');
        }elseif($this->project->action == 'reject'){
            return $this->subject('Project Rejection Confirmation from State Nodal Agency ('.$this->project->project_code.')')->markdown('emails.projectAction');
        }elseif($this->project->action == 'feedback'){
            return $this->subject('Modification Feedback Received from State Nodal Agency ('.$this->project->project_code.')')->markdown('emails.projectAction');
        }
    }
}
