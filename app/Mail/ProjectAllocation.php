<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProjectAllocation extends Mailable
{
    use Queueable, SerializesModels;

    public $project;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project)
    {
        $this->project = $project;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $allocationLetterPdf = json_decode($this->project->allocation_letter, true)['name'];
        return $this->subject('Confirmation of Project Allocation')
                    ->attachFromStorageDisk('filestore', 'sanctions\\'.$this->project->sanction_id.'\\projects\\'.$this->project->project_code.'\\'.$allocationLetterPdf, 'allocation-letter.pdf')
                    ->markdown('emails.projectAllocation');
    }
}
