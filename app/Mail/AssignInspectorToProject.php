<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssignInspectorToProject extends Mailable
{
    use Queueable, SerializesModels;

    public $project;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project)
    {
        $this->project = $project;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->project->toInstaller == 'true'){
            return $this->subject('Confirmtion of Inspector Allocation')->markdown('emails.assignInspectorToProject');
        }else{
            return $this->subject('New Project Allocated for Inspection ')->markdown('emails.assignInspectorToProject');
        }
    }
}
