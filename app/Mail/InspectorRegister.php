<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InspectorRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $inspector;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inspector)
    {
        $this->inspector = $inspector;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Confirmation of Inspector Registration')->markdown('emails.inspectorRegister');
    }
}
