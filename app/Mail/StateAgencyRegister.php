<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StateAgencyRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $stateAgency;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($stateAgency)
    {
        $this->stateAgency = $stateAgency;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Registration Confirmation for MNRE Solar Street Light Monitoring Web Portal')->markdown('emails.stateAgencyRegister');
    }
}
