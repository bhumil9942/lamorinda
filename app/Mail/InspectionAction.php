<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InspectionAction extends Mailable
{
    use Queueable, SerializesModels;

    public $project, $logs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project, $logs)
    {
        $this->project = $project;
        $this->logs = $logs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Status of Inspected Project')->markdown('emails.inspectionAction');
    }
}
