<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SanctionApproval extends Mailable
{
    use Queueable, SerializesModels;

    public $sanction;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sanction)
    {
        $this->sanction = $sanction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $locationsPdf = json_decode($this->sanction->locations, true)['name'];
        $sanctionLetterPdf = json_decode($this->sanction->sanction_letter, true)['name'];
        return $this->subject('Confirmation of Sanction Approval')
                    ->attachFromStorageDisk('filestore', '\\sanctions\\'.$this->sanction->id.'\\'.$locationsPdf, 'locations.pdf')
                    ->attachFromStorageDisk('filestore', '\\sanctions\\'.$this->sanction->id.'\\'.$sanctionLetterPdf, 'sanction-letter.pdf')
                    ->markdown('emails.sanctionApproval');
    }
}
