<?php

namespace App\Utils;

use App\Traits\General;
use Validator;

class FormValidator
{
    use General;
    public function __construct(){}

    public function validateSystemForm($request, $Steps, $required = NULL)
    {
        if($required) $required = 'required|';
        else $required = '';

        $Validations = [
            'address_landmark' => 'required',
            'pincode' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'survey_date' => 'required',
            'site_before_installation.*' => $required.'image'
        ];

        $stepTwoValidations = [
            'date_material_supply' => 'required',
            'date_construction_start' => 'required',
            'date_completion' => 'required',
            'pv_module_capacity' => 'required',
            'pv_module_technology' => 'required',
            'battery_type' => 'required',
            'depth_of_discharge' => 'required',
            'autonomy' => 'required',
            'battery_capacity' => 'required'
        ];

        $stepThreeValidations = [
            'light_source_type' => 'required',
            'light_source_capacity' => 'required',
            'light_output' => 'required',
            'charge_controller_rating' => 'required',
            'electronics_efficiency' => 'required',
            'duty_cycle_hours' => 'required',
            'pole_material' => 'required',
            'foundation_type' => 'required',
            'person_trained' => 'required',
            'site_after_installation.*' => $required.'image',
            'installer_picture' => $required.'image'
        ];

        switch($Steps){
            case 2:
                $Validations = array_merge($Validations, $stepTwoValidations);
                $Validations['site_before_installation.*'] = 'image';
            break;
            case 3:
                $Validations = array_merge($Validations, $stepTwoValidations, $stepThreeValidations);
                $Validations['site_before_installation.*'] = 'image';
            break;
        }

        $validation = Validator::make($request->all(), $Validations);
        if($validation->fails())
            return FALSE;

        return TRUE;
    }

    public function validateSystemInspectionForm($request, $required = NULL)
    {
        if($required) $required = 'required|';
        else $required = '';

        $validationArray = [
            'date' => 'required',
            'beneficiary_availability' => 'required',
            'ssl_with_beneficiary.*' => $required.'image',
            'pic_inspector' => $required.'image',
            'approve' => 'required'
        ];

        $validation = Validator::make($request->all(), $validationArray);
        if($validation->fails())
            return FALSE;

        return TRUE;
    }
}