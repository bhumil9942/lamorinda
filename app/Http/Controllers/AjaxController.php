<?php

namespace App\Http\Controllers;

use App\Traits\General;
use App\Models\MnreUser;
use Session, DB, Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AjaxController extends Controller
{
    use General;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(Session::token() !== $request->header('X-CSRF-TOKEN'))
                abort(419);
            return $next($request);
        });
    }

    public function passwordcheck()
    {
        $password = base64_decode(request()->password);
        if(request()->type == 'mnre'){
            $userPassword = MnreUser::where('id', Auth::guard('mnre')->user()->id)->value('password');
            if(Hash::check($password, $userPassword))
                return ['success' => true];
            else
                return ['success' => false];
        }
    }
}
