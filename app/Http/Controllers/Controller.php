<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use DateTimeZone;
use \Carbon\Carbon;
use Datetime;
use DB;
use Config;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function timezoneByLocation($latitude, $longitude, $key = 'AIzaSyCT-rBkorUKCTKLG0RfWkT76HAMlzk6S9k'){
        $time = time();
        $url = 'https://maps.googleapis.com/maps/api/timezone/json?location='.$latitude.'%2C'.$longitude.'&timestamp='.$time.'&key='.$key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $responseJson = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($responseJson);
        return $response->timeZoneId;
    }

    function utcTime($timezone, $date, $time){
        $dateTime = date('Y-m-d H:i:s', strtotime($date . $time));
        $newDateTime = new DateTime($dateTime, new DateTimeZone($timezone));
        $newDateTime->setTimezone(new DateTimeZone("UTC")); 
        $dateTimeUTC = $newDateTime->format("Y-m-d H:i:s");
        return $dateTimeUTC;
    }

    function utcTimeToCurrent($timezone, $date, $time){
        $dateTime = date('Y-m-d H:i:s', strtotime($date . $time));
        $newDateTime = new DateTime($dateTime, new DateTimeZone('UTC'));
        $newDateTime->setTimezone(new DateTimeZone($timezone)); 
        $dateTimeUTC = $newDateTime->format("Y-m-d H:i:s");
        return $dateTimeUTC;
    }

    function utcTimeToCurrentDateTime($timezone, $datetime){
        $dateTime = date('Y-m-d H:i:s', strtotime($datetime));
        $newDateTime = new DateTime($dateTime, new DateTimeZone('UTC'));
        $newDateTime->setTimezone(new DateTimeZone($timezone)); 
        $dateTimeUTC = $newDateTime->format("Y-m-d H:i:s");
        return $dateTimeUTC;
    }

    function utcTimeByDatetime($timezone, $finaldatetime){
        $dateTime = date('Y-m-d H:i:s', strtotime($finaldatetime));
        $newDateTime = new DateTime($dateTime, new DateTimeZone($timezone));
        $newDateTime->setTimezone(new DateTimeZone("UTC")); 
        $dateTimeUTC = $newDateTime->format("Y-m-d H:i:s");
        return $dateTimeUTC;
    }

    function verifyTimezone(){
        $array = [
            'America/Adak',
            'America/Anchorage',
            'America/Anguilla',
            'America/Antigua',
            'America/Araguaina',
            'America/Argentina/Buenos_Aires',
            'America/Argentina/Catamarca',
            'America/Argentina/Cordoba',
            'America/Argentina/Jujuy',
            'America/Argentina/La_Rioja',
            'America/Argentina/Mendoza',
            'America/Argentina/Rio_Gallegos',
            'America/Argentina/Salta',
            'America/Argentina/San_Juan',
            'America/Argentina/San_Luis',
            'America/Argentina/Tucuman',
            'America/Argentina/Ushuaia',
            'America/Aruba',
            'America/Asuncion',
            'America/Atikokan',
            'America/Bahia',
            'America/Bahia_Banderas',
            'America/Barbados',
            'America/Belem',
            'America/Belize',
            'America/Blanc-Sablon',
            'America/Boa_Vista',
            'America/Bogota',
            'America/Boise',
            'America/Cambridge_Bay',
            'America/Campo_Grande',
            'America/Cancun',
            'America/Caracas',
            'America/Cayenne',
            'America/Cayman',
            'America/Chicago',
            'America/Chihuahua',
            'America/Costa_Rica',
            'America/Creston',
            'America/Cuiaba',
            'America/Curacao',
            'America/Danmarkshavn',
            'America/Dawson',
            'America/Dawson_Creek',
            'America/Denver',
            'America/Detroit',
            'America/Dominica',
            'America/Edmonton',
            'America/Eirunepe',
            'America/El_Salvador',
            'America/Fort_Nelson',
            'America/Fortaleza',
            'America/Glace_Bay',
            'America/Goose_Bay',
            'America/Grand_Turk',
            'America/Grenada',
            'America/Guadeloupe',
            'America/Guatemala',
            'America/Guayaquil',
            'America/Guyana',
            'America/Halifax',
            'America/Havana',
            'America/Hermosillo',
            'America/Indiana/Indianapolis',
            'America/Indiana/Knox',
            'America/Indiana/Marengo',
            'America/Indiana/Petersburg',
            'America/Indiana/Tell_City',
            'America/Indiana/Vevay',
            'America/Indiana/Vincennes',
            'America/Indiana/Winamac',
            'America/Inuvik',
            'America/Iqaluit',
            'America/Jamaica',
            'America/Juneau',
            'America/Kentucky/Louisville',
            'America/Kentucky/Monticello',
            'America/Kralendijk',
            'America/La_Paz',
            'America/Lima',
            'America/Los_Angeles',
            'America/Lower_Princes',
            'America/Maceio',
            'America/Managua',
            'America/Manaus',
            'America/Marigot',
            'America/Martinique',
            'America/Matamoros',
            'America/Mazatlan',
            'America/Menominee',
            'America/Merida',
            'America/Metlakatla',
            'America/Mexico_City',
            'America/Miquelon',
            'America/Moncton',
            'America/Monterrey',
            'America/Montevideo',
            'America/Montserrat',
            'America/Nassau',
            'America/New_York',
            'America/Nipigon',
            'America/Nome',
            'America/Noronha',
            'America/North_Dakota/Beulah',
            'America/North_Dakota/Center',
            'America/North_Dakota/New_Salem',
            'America/Nuuk',
            'America/Ojinaga',
            'America/Panama',
            'America/Pangnirtung',
            'America/Paramaribo',
            'America/Phoenix',
            'America/Port-au-Prince',
            'America/Port_of_Spain',
            'America/Porto_Velho',
            'America/Puerto_Rico',
            'America/Punta_Arenas',
            'America/Rainy_River',
            'America/Rankin_Inlet',
            'America/Recife',
            'America/Regina',
            'America/Resolute',
            'America/Rio_Branco',
            'America/Santarem',
            'America/Santiago',
            'America/Santo_Domingo',
            'America/Sao_Paulo',
            'America/Scoresbysund',
            'America/Sitka',
            'America/St_Barthelemy',
            'America/St_Johns',
            'America/St_Kitts',
            'America/St_Lucia',
            'America/St_Thomas',
            'America/St_Vincent',
            'America/Swift_Current',
            'America/Tegucigalpa',
            'America/Thule',
            'America/Thunder_Bay',
            'America/Tijuana',
            'America/Toronto',
            'America/Tortola',
            'America/Vancouver',
            'America/Whitehorse',
            'America/Winnipeg',
            'America/Yakutat',
            'America/Yellowknife',
        ];
        return $array;
    }

    function getUserLocation($userId){
        $location = [];
        $user = DB::table('wof_users')->where(['user_id'=>$userId])->first();
        if($user){
            $location['lat'] = $user->lat;
            $location['lon'] = $user->lon;
        }else{
            $location['lat'] = '';
            $location['lon'] = '';
        }
        return $location;
    }

    function getTournamentLocation($tournyId){
        $info = [];
        $tourny = DB::table('tournaments')->where(['tournament_id'=>$tournyId])->first();
        if($tourny){
            $info['lat'] = $tourny->tourny_lat;
            $info['lon'] = $tourny->tourny_long;
            $info['timezone'] = $tourny->timezone;
        }else{
            $info['lat'] = '';
            $info['lon'] = '';
            $info['timezone'] = "";
        }
        return $info;
    }

    function getServerDateTime($timezone){
        $date = date('Y-m-d H:i:s');
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        $date->setTimezone(new DateTimeZone($timezone));
        $formatted_date = $date->format('Y-m-d H:i:s');
        return date('Y-m-d H:i:s',strtotime($formatted_date));
    }

    function getTournyTimezone($tournydate, $tournytimezone, $usertimezone)
    {
        $tournydate = date('Y-m-d H:i:s',strtotime($tournydate));
        $date = new DateTime($tournydate, new DateTimeZone($tournytimezone));
        $date->setTimezone(new DateTimeZone($usertimezone));
        $final_date = $date->format('Y-m-d H:i:s');
        return $final_date;
    }

    function dateTimeByTimezone($date, $timezone){
        $servertimezone = Config::get('app.timezone');
        $date = new DateTime($date, new DateTimeZone($timezone));
        $date->setTimezone(new DateTimeZone($servertimezone));
        return $date->format('Y-m-d H:i:s');
    }

    function covertDateFormat($date){
        return date('m/d/y', strtotime($date));
    }

    function covertTimeFormat($time){
        return date('g:i a', strtotime($time));
    }

    function get_nearest_timezone($cur_lat, $cur_long, $country_code = '') {
        $timezone_ids = ($country_code) ? DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country_code)
                                        : DateTimeZone::listIdentifiers();

        if($timezone_ids && is_array($timezone_ids) && isset($timezone_ids[0])) {

            $time_zone = '';
            $tz_distance = 0;

            //only one identifier?
            if (count($timezone_ids) == 1) {
                $time_zone = $timezone_ids[0];
            } else {
                foreach($timezone_ids as $timezone_id) {
                    $timezone = new DateTimeZone($timezone_id);
                    $location = $timezone->getLocation();
                    $tz_lat   = $location['latitude'];
                    $tz_long  = $location['longitude'];

                    $theta    = $cur_long - $tz_long;
                    $distance = (sin(deg2rad($cur_lat)) * sin(deg2rad($tz_lat))) 
                    + (cos(deg2rad($cur_lat)) * cos(deg2rad($tz_lat)) * cos(deg2rad($theta)));
                    $distance = acos($distance);
                    $distance = abs(rad2deg($distance));
                    // echo '<br />'.$timezone_id.' '.$distance; 
                    if (!$time_zone || $tz_distance > $distance) {
                        $time_zone   = $timezone_id;
                        $tz_distance = $distance;
                    } 

                }
            }
            return  $time_zone;
        }
        return 'unknown';
    }
}
