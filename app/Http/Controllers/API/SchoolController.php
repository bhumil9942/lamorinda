<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use App\Models\School;
use App\Models\City;
use App\Models\BusRoute;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    public function getSchoolsByCityId($city_id)
    {  
        $schools = School::where(['school_city_id'=>$city_id])->get();
        foreach ($schools as $school) {
            $cityName = City::where(['city_id'=>$city_id])->first();
            $school->city_name = $cityName->city_name;
        }
        return view('common.front.select-your-school', compact('schools'));
    }
}