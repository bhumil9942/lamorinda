<?php

namespace App\Http\Controllers\API;

use App\Models\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function cityList()
    {  
        $city = City::getCities();
        return view('common.front.select-your-city', compact('city'));
    }
}
