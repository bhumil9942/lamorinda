<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\PassType;
use Illuminate\Http\Request;

class PassTypeController extends Controller
{
    public function getPassTypeBySchoolId($school_city_id)
    {  
        $pass_type = PassType::getPassType();
        return view('common.front.choose-bus-pases', compact('pass_type'));
    }
}