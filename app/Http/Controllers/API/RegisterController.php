<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Models\Students;
use Illuminate\Support\Facades\Auth;
class RegisterController extends Controller
{
    // public function index()
    // {
    //     return view('auth.login');
    // } 
   
      
    public function signIn(Request $request)
    {
        // print($request);
        // exit;
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        // $this->validate($request, ['email' => 'required|email', 'password' => 'required']);
        // $user = $request->only('email', 'password');
        // if(Auth::attempt($user)){
        // return response()->json(['message'=>'Login Successfully','status'=>'success','data'=>[]]);
        // }
        // $credentials = $request->only('email', 'password');
 
        // if (Auth::attempt($credentials)) {
        //     // Authentication passed...
        //     return response()->json(['message'=>'Login Successfully','status'=>'success','data'=>[]]);
        // }
    //     $this->validate($request, ['email' => 'required|email', 'password' => 'required']);
    //     $user = $request->all();
    //     if (Auth::attempt($user))
    //    {
    //     return response()->json(['message'=>'Login Successfully','status'=>'success','data'=>[]]);
    //    }
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // return redirect()->intended('dashboard')
            //             ->withSuccess('Signed in');
            return response()->json(['message'=>'Login Successfully','status'=>'success','data'=>[]]);
        }
        // if (Auth::attempt(['email', 'password'])) {
      
        //     // Authentication passed...
        //     return response()->json(['message'=>'Login Successfully','status'=>'success','data'=>[]]);
        //  }
  
        return response()->json(['message'=>'Login Unsuccessfull','status'=>'fail','data'=>[]]);
    }

    // public function registration()
    // {
    //     return view('auth.registration');
    // }
      
    public function storeUser(Request $request)
    {  
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        // print($request);
        // exit;
           
        $data = $request->all();
        $check = $this->create($data);

        // $students = new Students;
        // $students -> fname = $request->fname;
        // $students -> lname = $request->lname;
        // $students -> email = $request->email;
        // $students -> password = $request->password;
        // $students -> save();
         
        return redirect("/homepage")->withSuccess('You have signed-in');
        //return response()->json(['message'=>'Registered Successfully','status'=>'success','data'=>[]]);
    }

    public function create(array $data)
    {
      return Students::create([
        'fname' => $data['fname'],
        'lname' => $data['lname'],
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }    
    
    public function dashboard()
    {
        if(Auth::check()){
            return view('/homepage');
        }
  
        return response()->json(['message'=>'You are not allowed to access']);
        //return redirect("login")->withSuccess('You are not allowed to access');
    }
    
    // public function signOut() {
    //     Session::flush();
    //     Auth::logout();
  
    //     return Redirect('login');
    // }
}