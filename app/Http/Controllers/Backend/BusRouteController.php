<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BusRoute;

class BusRouteController extends Controller
{
    public function busRouteList()
    {  
        $routes = BusRoute::getBusRoutes();
        return view('common.admin.busRouteList', compact('routes'));
    }

    public function createRoute(Request $request)
    {  
        try {
            if($request->isMethod('get')){
                return view('common.admin.createBusRoute');
            }
            if(empty($request->bus_route_id)){
                $routes = BusRoute::createBusRoute($request->all());
                if(!empty($routes->bus_route_id)){
                    return redirect('/admin/route')->with('success', 'Bus Route added successfully !');
                }
                else{
                    return back()->with('fail', 'Server error !');
                }
            }
            else{
                // Update Award
                // echo "test1";exit;
                // echo '<pre>';print_r($request->all());exit;
                $routes = BusRoute::updateBusRoute($request->all());
                if(!empty($routes)){
                    return redirect('/admin/route')->with('success', 'Bus Route updated successfully !');
                }
                else{
                    return back()->with('fail', 'Server error test !');
                }
            }
        } catch (\Throwable $th) {
            return back()->with('fail', 'Server error !');
        }
    }

    public function editRoute($id)
    {
        // $id = base64_decode($id);
        $routes = BusRoute::getBusRouteById($id);
        return view('common.admin.createBusRoute', compact('routes'));
    }

    public function deleteRoute($id)
    {
        // $id = base64_decode($id);
        $routes = BusRoute::getBusRouteById($id);
        BusRoute::removeBusRoute($id);
        return redirect('/admin/route')->with('success', 'Bus Route Deleted successfully !');
    }
}
