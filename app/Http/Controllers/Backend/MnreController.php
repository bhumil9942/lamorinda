<?php

namespace App\Http\Controllers\Backend;

use App\Traits\General;
use App\Utils\Dictionary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\APIModels\Tournament;
use Hash;
use DB;


class MnreController extends Controller
{
    use General;
    public function __construct()
    {}

    public function dashboard()
    {
        // echo "test";exit;
        $routes = DB::table('routes')->count();

        // echo '<pre>';print_r($routes);exit;
        $cities = DB::table('city')->count();
        $schools = DB::table('schools')->count();
        return view('mnre.dashboard',compact('routes','cities','schools'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function changePassword(Request $request)
    {
        try {
            if($request->isMethod('get')){
                return view('common.admin.changePassword');
            }
            elseif($request->isMethod('post')){
                if(!$this->passwordPolicyTest($request->newpass)){
                    Session::flash('msg', ['status' => 'Failed', 'msg' => "Failed strong password policy!"]);
                    return redirect('/admin/changepassword');
                }
                $user = Auth::user();
                $user->password = Hash::make($request->newpass);
                $user->save();
                Session::flash('msg', ['status' => 'Success', 'msg' => "Password changed successfully!"]);
                return redirect('/admin/changepassword');
            }
        }
        catch (\Exception $e) {
            return redirect()->back();
        }
    }
}
