<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class BookingsController extends Controller
{
    public function citiesList()
    {
        $cities = DB::table('cities')->get();
        return view('common.admin.citiesBookingList',compact('cities'));
    }

    public function getStudentsByCityId($cityId)
    {
        $name = DB::table('cities')->where(['id'=>$cityId])->value('name');
        $bookings = DB::table('bookings')->where(['city_id'=>$cityId])->get();
        // echo '<pre>';print_r($bookings);exit;
        return view('common.admin.studentsList',compact('bookings','name'));
    }

    public function studentDetails($studentId)
    {
        // echo '<pre>';print_r($studentId);exit;
        $students = DB::table('bookings')->where(['student_id'=>$studentId])->first();
        if($students->disability == '1'){
            $students->disability = 'Yes';
        }else{
            $students->disability = 'No';
        }
        // echo '<pre>';print_r($students);exit;
        return view('common.admin.studentDetails',compact('students'));
    }

    public function payments()
    {
        $payments = DB::table('payments')->get();
        foreach ($payments as $value) {
            $students = DB::table('students')->where(['id'=>$value->student_id])->first();
            $value->student_name = $students->first_name.' '.$students->last_name;
        }
        // echo '<pre>';print_r($bookings);exit;
        return view('common.admin.payment',compact('payments'));
    }
}
