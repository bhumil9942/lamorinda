<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use App\Models\School;
use App\Models\City;
use App\Models\BusRoute;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    public function schoolsList()
    {  
        $schools = School::getSchools();
        foreach ($schools as $school) {
            $cityName = City::where(['id'=>$school->city_id])->first();
            $school->city_name = $cityName->city_name;
        }
        return view('common.admin.schoolList', compact('schools'));
    }

    public function createSchool(Request $request)
    {  
        try {
            if($request->isMethod('get')){
                $cities = DB::table('cities')->where(['status'=>"1"])->get();
                $routes = DB::table('routes')->where(['bus_route_status'=>"1"])->get();
                return view('common.admin.createSchool',compact('cities','routes'));
            }
            if(empty($request->school_id)){
                if($request->hasfile('school_image'))
                {
                    //Upload Image In DestinationPath
                    $file = $request->file('school_image');
                    $filename = uniqid().$file->getClientOriginalName(); 
                    $destinationPath = public_path('/api/assets/user');
                    $file->move($destinationPath, $filename);
                    $request->request->add(['schoolimage_name' => $filename]);
                }
                $school = School::createSchool($request->all());
                if(!empty($school->school_id)){
                    return redirect('/admin/school')->with('success', 'School added successfully !');
                }
                else{
                    return back()->with('fail', 'Server error !');
                }
            }
            else{
                if($request->hasfile('school_image'))
                {
                    //Delete Old Image Of School If Updated
                    unlink(public_path()."/api/assets/user/".$request->school_old_image);

                    //Upload Image In DestinationPath
                    $file = $request->file('school_image');
                    $filename = uniqid().$file->getClientOriginalName(); 
                    $destinationPath = public_path('/api/assets/user');
                    $file->move($destinationPath, $filename);
                    $request->request->add(['schoolimage_name' => $filename]);
                }
                // Update Award
                $school = School::updateSchool($request->all());
                if(!empty($school)){
                    return redirect('/admin/school')->with('success', 'School updated successfully !');
                }
                else{
                    return back()->with('fail', 'Server error test !');
                }
            }
        } catch (\Throwable $th) {
            return back()->with('fail', 'Server error !');
        }
    }

    public function editSchool($id)
    {
        $school = School::getSchoolById($id);
        $cities = DB::table('cities')->where(['status'=>"1"])->get();
        $routes = DB::table('routes')->where(['bus_route_status'=>"1"])->get();
        $aIds = json_decode($school['alternate_route_id']);
        $rIds = json_decode($school['route_id']);
        // echo '<pre>';print_r($school->toArray());exit;
        return view('common.admin.editSchool', compact('school','cities','routes','aIds','rIds'));
    }

    public function deleteSchool($id)
    {
        // $id = base64_decode($id);
        $city = School::getSchoolById($id);
        School::removeSchool($id);
        return redirect('/admin/school')->with('success', 'School Deleted successfully !');
    }

    public function schoolRoutesList($id)
    {
        $schools = School::getSchoolById($id);
        // echo '<pre>';print_r($schools->toArray());exit;
        $alternateRouteIds = json_decode($schools['alternate_route_id']);
        $routeIds = json_decode($schools['route_id']);
        $schoolName = School::where(['school_id'=>$id])->first();
        foreach ($alternateRouteIds as $ids) {
            $alternateRouteName = BusRoute::where(['bus_route_id'=>$ids])->pluck('bus_route_name');
            $listArray = json_decode(json_encode($alternateRouteName), true);
            $implodeAlternate[] = implode(',',$listArray);
        }
        foreach ($routeIds as $ids1) {
            $routeName = BusRoute::where(['bus_route_id'=>$ids1])->pluck('bus_route_name');
            $listArray1 = json_decode(json_encode($routeName), true);
            $implodeRoute[] = implode(',',$listArray1);
        }
        $aName = implode(',',$implodeAlternate);
        $rName = implode(',',$implodeRoute);
        return view('common.admin.schoolBusRouteList', compact('schools','aName','rName'));
    }
}
