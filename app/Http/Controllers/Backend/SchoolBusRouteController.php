<?php

namespace App\Http\Controllers\Backend;
use App\Models\SchoolBusRoute;
use App\Models\School;
use App\Models\BusRoute;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class SchoolBusRouteController extends Controller
{
    public function schoolBusRouteList($id)
    {  
        // echo $id;exit;
        $schoolBusRoute = SchoolBusRoute::where(['school_id'=>$id])->get();
        // echo '<pre>';print_r($schoolBusRoute->toArray());exit;
        $getFinalData = [];
        $i=0;    
        foreach ($schoolBusRoute as $school) {
            $alternateRouteIds = json_decode($school->alternate_route_id);
            $routeIds = json_decode($school->route_id);
            $schoolName = School::where(['school_id'=>$id])->first();
            // echo '<pre>';print_r($routeIds);
            foreach ($alternateRouteIds as $ids) {
                $alternateRouteName = BusRoute::where(['bus_route_id'=>$ids])->pluck('bus_route_name');
                $listArray = json_decode(json_encode($alternateRouteName), true);
                $implodeAlternate[] = implode(',',$listArray);
            }
            foreach ($routeIds as $ids1) {
                $routeName = BusRoute::where(['bus_route_id'=>$ids1])->pluck('bus_route_name');
                $listArray1 = json_decode(json_encode($routeName), true);
                $implodeRoute[] = implode(',',$listArray1);
            }
            $alternateRouteNameFinal = implode(',',$implodeAlternate);
            $routeNameFinal = implode(',',$implodeRoute);
            $getFinalData[$i]['school_bus_route_id']    = $school->school_bus_route_id;        
            $getFinalData[$i]['school_id']              = $id;         
            $getFinalData[$i]['school_name']            = $schoolName->school_name;         
            $getFinalData[$i]['route_type']             = $school->route_type;         
            $getFinalData[$i]['route_name']             = $routeNameFinal;         
            $getFinalData[$i]['alternate_route_name']   = $alternateRouteNameFinal;         
            // $i++;
        }
        // exit;   

        // echo '<pre>';print_r($getFinalData);exit;
        return view('common.admin.schoolBusRouteList', compact('getFinalData'));
    }

    public function viewSchoolBusRoute($id){
        $schoolId = $id;
        $schools = DB::table('schools')->where(['school_id'=>$id])->value('school_name');
        $routes = DB::table('routes')->get();
        return view('common.admin.createSchoolBusRoute',compact('schools','routes','schoolId'));
    }

    public function createSchoolBusRoute(Request $request,$id)
    {  
        // echo "test";exit;
        // try {
            if($request->isMethod('get')){
                $schoolId = $id;
                $schools = DB::table('schools')->where(['school_id'=>$id])->value('school_name');
                $routes = DB::table('routes')->get();
                return view('common.admin.createSchoolBusRoute',compact('routes','schools','schoolId'));
            }
            if(empty($id)){
                // echo "test";exit;
                $schoolBusRoute = SchoolBusRoute::createSchoolBusRoute($request->all(),$id);
                if(!empty($schoolBusRoute->school_bus_route_id)){
                    return redirect('/admin/school')->with('success', 'School Bus Route added successfully !');
                }
                else{
                    return back()->with('fail', 'Server error !');
                }
            }
            else{
                echo '<pre>';print_r($request->all());exit;
                $schoolBusRoute = SchoolBusRoute::updateSchoolBusRoute($request->all());
                if(!empty($schoolBusRoute)){
                    return redirect('/admin/school_bus_route/'.$id)->with('success', 'School Bus Route updated successfully !');
                }
                else{
                    return back()->with('fail', 'Server error test !');
                }
            }
        // } catch (\Throwable $th) {
        //     return back()->with('fail', 'Server error !');
        // }
    }

    public function editSchoolBusRoute($school_id,$id)
    {
        // $id = base64_decode($id);
        $schoolId = $school_id;
        $schools = DB::table('schools')->where(['school_id'=>$school_id])->value('school_name');
        $routes = DB::table('routes')->get();
        $schoolBusRoute = SchoolBusRoute::getSchoolBusRouteById($id);
        $aIds = json_decode($schoolBusRoute['alternate_route_id']);
        $rIds = json_decode($schoolBusRoute['route_id']);
        // echo '<pre>';print_r($aIds);exit;
        return view('common.admin.editSchoolBusRoute', compact('schoolBusRoute','schools','routes','schoolId','aIds','rIds'));
    }

    public function deleteSchoolBusRoute($id)
    {
        // $id = base64_decode($id);
        $city = SchoolBusRoute::getSchoolBusRouteById($id);
        SchoolBusRoute::removeSchoolBusRoute($id);
        return redirect('/admin/school_bus_route')->with('success', 'School Bus Route Deleted successfully !');
    }
}
