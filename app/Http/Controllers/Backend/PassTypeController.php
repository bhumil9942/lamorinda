<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PassType;
use Illuminate\Http\Request;

class PassTypeController extends Controller
{
    public function passTypeList()
    {  
        $pass_type = PassType::getPassType();
        return view('common.admin.passTypeList', compact('pass_type'));
    }

    public function createPassType(Request $request)
    {  
        try {
            if($request->isMethod('get')){
                return view('common.admin.createPassType');
            }
            if(empty($request->pass_id)){
                // echo '<pre>';print_r($request->all());exit;
                $pass_type = PassType::createPassType($request->all());
                if(!empty($pass_type->pass_id)){
                    return redirect('/admin/pass_type')->with('success', 'Pass Type added successfully !');
                }
                else{
                    return back()->with('fail', 'Server error !');
                }
            }
            else{
                $pass_type = PassType::updatePassType($request->all());
                if(!empty($pass_type)){
                    return redirect('/admin/pass_type')->with('success', 'Pass Type updated successfully !');
                }
                else{
                    return back()->with('fail', 'Server error test !');
                }
            }
        } catch (\Throwable $th) {
            return back()->with('fail', 'Server error !');
        }
    }

    public function editPassType($id)
    {
        // $id = base64_decode($id);
        $pass_type = PassType::getPassTypeById($id);
        return view('common.admin.createPassType', compact('pass_type'));
    }

    public function deletePassType($id)
    {
        // $id = base64_decode($id);
        $pass_type = PassType::getPassTypeById($id);
        PassType::removePassType($id);
        return redirect('/admin/pass_type')->with('success', 'Pass Type Deleted successfully !');
    }
}
