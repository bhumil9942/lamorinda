<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use App\Models\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function cityList()
    {  
        $city = City::getCities();
        return view('common.admin.cityList', compact('city'));
    }

    public function createCity(Request $request)
    {  
        try {
            if($request->isMethod('get')){
                return view('common.admin.createCity');
            }
            if(empty($request->id)){
                $city = City::createCity($request->all());
                if(!empty($city->id)){
                    return redirect('/admin/city')->with('success', 'City added successfully !');
                }
                else{
                    return back()->with('fail', 'Server error !');
                }
            }
            else{
                // Update Award
                // echo "test1";exit;
                // echo '<pre>';print_r($request->all());exit;
                $city = City::updateCity($request->all());
                if(!empty($city)){
                    return redirect('/admin/city')->with('success', 'City updated successfully !');
                }
                else{
                    return back()->with('fail', 'Server error test !');
                }
            }
        } catch (\Throwable $th) {
            return back()->with('fail', 'Server error !');
        }
    }

    public function editCity($id)
    {
        // $id = base64_decode($id);
        $city = City::getCityById($id);
        return view('common.admin.createCity', compact('city'));
    }

    public function deleteCity($id)
    {
        // $id = base64_decode($id);
        $city = City::getCityById($id);
        City::removeCity($id);
        return redirect('/admin/city')->with('success', 'City Deleted successfully !');
    }
}
