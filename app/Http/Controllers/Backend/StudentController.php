<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Students;
use Carbon\Carbon;
use DB;

class StudentController extends Controller
{
    public function studentsList()
    {  
        $students = Students::getStudents();
        return view('common.admin.studentsList', compact('students'));
    }

    public function createStudent(Request $request)
    {  
        // try {
            if($request->isMethod('get')){
                $cities = DB::table('city')->where(['city_status'=>"1"])->get();
                $routes = DB::table('routes')->where(['bus_route_status'=>"1"])->get();
                $pass_types = DB::table('pass_types')->where(['pass_status'=>"1"])->get();
                $schools = DB::table('schools')->get();
                $allDates = $this->listAllDates();
                $zipcodes = $this->listZipcodes();
                $grades = $this->listGrades();
                $amRoutes = DB::table('schools')->where(['route_type'=>"1"])->get();
                return view('common.admin.createStudent',compact('cities','routes','schools','allDates','zipcodes',
                            'grades','pass_types','amRoutes'));
            }
            if(empty($request->student_id)){
                $students = Students::createStudent($request->all());
                if(!empty($students->student_id)){
                    return redirect('/admin/students')->with('success', 'Student added successfully !');
                }
                else{
                    return back()->with('fail', 'Server error !');
                }
            }
            else{
                // Update Award
                // echo "test1";exit;
                // echo '<pre>';print_r($request->all());exit;
                $students = Students::updateStudent($request->all());
                if(!empty($students)){
                    return redirect('/admin/students')->with('success', 'Student updated successfully !');
                }
                else{
                    return back()->with('fail', 'Server error test !');
                }
            }
        // } catch (\Throwable $th) {
        //     return back()->with('fail', 'Server error !');
        // }
    }

    public function editStudent($id)
    {
        // $id = base64_decode($id);
        $students = Students::getStudentById($id);
        $cities = DB::table('city')->where(['city_status'=>"1"])->get();
        $routes = DB::table('routes')->where(['bus_route_status'=>"1"])->get();
        $pass_types = DB::table('pass_types')->where(['pass_status'=>"1"])->get();
        $schools = DB::table('schools')->get();
        $allDates = $this->listAllDates();
        $dateDown = Students::where(['student_id'=>$id])->first();
        $dateD = json_decode($dateDown['date_downloaded'])[0];
        $zipcodes = $this->listZipcodes();
        $grades = $this->listGrades();
        $amRoutes = DB::table('schools')->where(['route_type'=>"1"])->get();
        return view('common.admin.createStudent',compact('cities','routes','schools','allDates','zipcodes',
                    'grades','pass_types','amRoutes','students','dateD'));
        // return view('common.admin.createStudent', compact('students'));
    }

    public function deleteStudent($id)
    {
        // $id = base64_decode($id);
        $students = Students::getStudentById($id);
        Students::removeStudent($id);
        return redirect('/admin/students')->with('success', 'Student Deleted successfully !');
    }

    public static function listAllDates(){
        $startDate = new Carbon('2022-01-01');
        $endDate = new Carbon('2024-12-31');
        $all_dates = array();
        while ($startDate->lte($endDate)){
            $all_dates[] = $startDate->toDateString();
            $startDate->addDay();
        }
        return $all_dates;
    }

    public static function listZipcodes(){
        $zipcode = ['94563',
            '94563-1317',
            '94463',
            '94563-1215',
            '94563-3610',
            '94563-1703',
            '94563-2805',
            '94563-1925',
            '94803',
            '94563-1408',
            '94563-2329',
            '94563-3227',
            '94582',
            '94519',
            '94563-3550',
            '94563-2028',
            '94563-2819',
            '94563-1407',
        ];
        return $zipcode;
    }

    public static function listGrades(){
        $grades = ['K','1','2','3','4','5','6','7','8','9','10','11','12'];
        return $grades;
    }
}
