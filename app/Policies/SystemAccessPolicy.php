<?php

namespace App\Policies;

use App\Models\User;
use App\Models\System;
use App\Models\Project;
use App\Models\Sanction;
use App\Models\StateAgency;
use Illuminate\Auth\Access\HandlesAuthorization;

class SystemAccessPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function systemDetail(StateAgency $stateAgency, System $system)
    {
        $sanctionId = Project::where('id', $system->project_id)->value('sanction_id');
        $stateAgencyId = Sanction::where('id', $sanctionId)->value('state_agency_id');
        return $stateAgency->id === $stateAgencyId;
    }
}
