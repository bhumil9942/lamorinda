<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Installer;
use App\Models\StateAgency;
use App\Models\StateAgencyInstaller;
use Illuminate\Auth\Access\HandlesAuthorization;

class InstallerAccessPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function installerEditOrDetail(StateAgency $stateAgency, Installer $installer)
    {
        $checkExist = StateAgencyInstaller::where(['state_agency_id' => $stateAgency->id, 'installer_id' => $installer->id])->first();
        return $checkExist ? true : false;
    }
}
