<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Inspector;
use App\Models\StateAgency;
use Illuminate\Auth\Access\HandlesAuthorization;

class InspectorAccessPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function inspectorEditOrDetail(StateAgency $stateAgency, Inspector $inspector)
    {
        return $stateAgency->id === $inspector->state_agency_id;
    }
}
