<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Sanction;
use App\Models\StateAgency;
use Illuminate\Auth\Access\HandlesAuthorization;

class SanctionAccessPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function sanctionEditOrDetail(StateAgency $stateAgency, Sanction $sanction)
    {
        return $stateAgency->id === $sanction->state_agency_id;
    }

    public function editWhenNotActive(StateAgency $stateAgency, Sanction $sanction)
    {
        return $sanction->status === 1 ? false : true;
    }
}
