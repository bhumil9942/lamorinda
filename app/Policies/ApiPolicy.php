<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\System;
use App\Models\Inspector;
use App\Models\Installer;
use App\Models\Inspection;
use App\Models\SystemInspection;

class ApiPolicy
{
    public function __construct()
    {}

    public function verifyInstallerProject($installerId, $projectId)
    {
        if(Project::where([['id', $projectId],['installer_id', $installerId]])->first()) return FALSE;
        else return TRUE;
    }

    public function verifyInstallerSystem($installerId, $systemId)
    {
        if(Project::join('systems','systems.project_id','projects.id')->where([['systems.id', $systemId],['projects.installer_id', $installerId]])->first()) return FALSE;
        else return TRUE;
    }

    public function verifyInspectorSystem($inspectorId, $systemId)
    {
        if(Project::join('systems','systems.project_id','projects.id')->where([['systems.id', $systemId],['projects.inspector_id', $inspectorId]])->first()) return FALSE;
        else return TRUE;
    }

    public function verifyInspectorProjectBySystem($inspectorId, $systemId)
    {
        if(Project::join('systems','systems.project_id','projects.id')
                    ->where([['systems.id', $systemId],['projects.inspector_id', $inspectorId]])
                    ->first()
        ) 
            return FALSE;
        else return TRUE;
    }

    public function verifyInspectorProject($inspectorId, $projectId)
    {
        if(Project::where([['id', $projectId],['inspector_id', $inspectorId]])->first()) return FALSE;
        else return TRUE;
    }

    public function verifyInspectorOfInspection($inspectorId, $systemInspectionId)
    {
        if(SystemInspection::join('inspections','inspections.id','system_inspections.inspection_id')
                            ->where([['system_inspections.id', $systemInspectionId],['inspections.inspector_id', $inspectorId]])
                            ->first()
        )
            return FALSE;
        else return TRUE;
    }
}
