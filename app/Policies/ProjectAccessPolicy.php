<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Project;
use App\Models\Sanction;
use App\Models\StateAgency;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectAccessPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function projectEditOrDetail(StateAgency $stateAgency, Project $project)
    {
        $stateAgencyId = Sanction::where('id', $project->sanction_id)->value('state_agency_id');
        return $stateAgency->id === $stateAgencyId;
    }
}
