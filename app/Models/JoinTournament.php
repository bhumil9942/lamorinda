<?php

namespace App\Models\APIModels;
use Illuminate\Database\Eloquent\Model;
use App\Models\APIModels\WOFAuth;
use DB;

class JoinTournament extends Model
{
    protected $table = 'jointournament';

    protected $primaryKey = 'jointourny_id';

    protected $fillable = [
        'tournament_id', 'user_id', 'created_at',
    ];

    protected $hidden = [
        'updated_at',
    ];

    public static function fillTournament($data)
    {
        return self::create([
            'tournament_id' => $data['tourny_id'],
            'user_id'   => $data['user_id'],
        ]);
    }

    public static function joinedUser($tournamentId)
    {
        return self::select('jointournament.*','wof_users.username','wof_users.image','wof_users.fullname','tournaments.tourny_location')
                ->join('wof_users','wof_users.user_id','=','jointournament.user_id')
                ->join('tournaments','tournaments.tournament_id','=','jointournament.tournament_id')
                ->where('jointournament.tournament_id', $tournamentId)
                ->get()->toArray();
    }

    public static function joinTourny($tournamentId,$userId)
    {
        $tournaments = self::select()
            ->where(['user_id'=>$userId])
            ->where(['tournament_id'=>$tournamentId])
            ->get()->toArray();
        if(!empty($tournaments))
        {
            $status='1';
        }
        else
        {
            $status='0';
        }
        return $status;
    }

    public static function joinedUserCounts($userId)
    {
        return DB::table('jointournament')->where(['user_id'=>$userId])->count(); 
    }

    public static function joinUserCounts($tournamentId)
    {
        return DB::table('jointournament')->where(['tournament_id'=>$tournamentId])->count(); 
    }    

    public static function contestantList($tournamentId)
    {
        $contestants = self::select('jointournament.*')
                ->where('jointournament.tournament_id', $tournamentId)
                ->get()->toArray();
        $user = array();
        $i=0;
        foreach($contestants as $attendee)
        {
            $user[$i]['jointourny_id'] = (string)$attendee['jointourny_id'];
            $user[$i]['tournament_id'] = $attendee['tournament_id'];
            $user[$i]['user_id'] = $attendee['user_id'];
            $user[$i]['user_image'] = WOFAuth::fetchUserImage($attendee['user_id']);
            $user[$i]['username'] = WOFAuth::fetchUserName($attendee['user_id']);
            $user[$i]['fish_length'] = Tournament::getFishLength($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['fish_weight'] = Tournament::getFishWeight($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['is_fish_submitted'] = Tournament::getIsFishSubmitted($attendee['user_id'],$attendee['tournament_id']);
            $i++;
        }
        return $user;
    }

    public static function addVote($fish_id)
    {
        $voting = self::select('jointournament.*')
        ->where('jointournament.tournament_id', $tournamentId)
        ->get()->toArray();
        $user = array();
        $i=0;
        foreach($voting as $vote)
        {
            $user[$i]['tournament_id'] = $vote['tournament_id'];
            $user[$i]['username'] = Tournament::TournamentById($vote['tourny_name']);
            $user[$i]['user_id'] = $vote['user_id'];
            $user[$i]['username'] = WOFAuth::fetchUserName($vote['user_id']);
            $user[$i]['fish_length'] = Tournament::getFishLength($vote['user_id'],$vote['tournament_id']);
            $user[$i]['fish_weight'] = Tournament::getFishWeight($vote['user_id'],$vote['tournament_id']);
            $i++;
        }
        return $user;
    }
}