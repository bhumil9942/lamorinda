<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PassType extends Model
{
    protected $table = 'pass_types';

    protected $primaryKey = 'pass_id';

    protected $fillable = [
        'pass_name', 'pass_status', 'pass_monthly', 'created_at', 'updated_at',
    ];

    public static function getPassType()
    {
        $query = self::select();
        return $query->get();
    }

    public static function createPassType($data)
    {
        return self::create([
            'pass_name' => $data['pass_name'],
            'pass_status' => $data['pass_status'],
            'pass_monthly' => $data['pass_monthly'],
        ]);
    }

    public static function getPassTypeById($passId)
    {
        $query = self::select()->where('pass_types.pass_id', $passId);
        return $query->first();
    }

    public static function updatePassType($data)
    {
        $isUpdated = self::where('pass_id', $data['pass_id'])->update([
            'pass_name' => $data['pass_name'],
            'pass_status' => $data['pass_status'],
            'pass_monthly' => $data['pass_monthly'],
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        
        if($isUpdated){
            return $data['pass_id'];
        }else{
            return null;
        }
    }

    public static function removePassType($passId)
    {
        return self::where('pass_id', $passId)->delete();
    }
}
