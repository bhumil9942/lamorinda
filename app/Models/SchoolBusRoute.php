<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use stdClass;
use DB;

class SchoolBusRoute extends Model
{
    protected $table = 'school_bus_route';

    protected $primaryKey = 'school_bus_route_id';

    protected $fillable = [
        'school_id', 'route_type', 'route_id', 'alternate_route_id', 'created_at', 'updated_at',
    ];

    public static function getSchoolBusRoute()
    {
        $query = self::select();
        return $query->get();
    }

    public static function createSchoolBusRoute($data,$id)
    {
        // echo '<pre>';print_r($alternateIds);
        // exit;
        return self::create([
            'school_id'             => $id,
            'route_type'            => $data['route_type'],
            'route_id'              => json_encode($data['route_id'],TRUE),
            'alternate_route_id'    => json_encode($data['alternate_route_id'],TRUE),
        ]);
    }

    public static function getSchoolBusRouteById($schoolBusRouteId)
    {
        $query = self::select()->where('school_bus_route.school_bus_route_id', $schoolBusRouteId);
        return $query->first();
    }

    public static function updateSchoolBusRoute($data)
    {
        $isUpdated = self::where('school_bus_route_id', $data['school_bus_route_id'])->update([
            'school_id'             => $data['school_id'],
            'route_type'            => $data['route_type'],
            'route_id'              => $data['route_id'],
            'alternate_route_id'    => $data['alternate_route_id'],
            'updated_at'            => date('Y-m-d h:i:s'),
        ]);
        
        if($isUpdated){
            return $data['school_bus_route_id'];
        }else{
            return null;
        }
    }

    public static function removeSchoolBusRoute($schoolBusRouteId)
    {
        return self::where('school_bus_route_id', $schoolBusRouteId)->delete();
    }
}
