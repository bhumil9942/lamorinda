<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HosterCommision extends Model
{
    protected $table = 'hoster_commission';

    protected $primaryKey = 'commission_id';

    protected $fillable = [
        'commission',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function getCommisionById()
    {
        $query = self::select()->orderBy('commission_id', 'desc');
        return $query->first();
    }

    public static function insertTournamentCommision($data)
    {
        $isCreated = self::create([
            'commission'=> $data['commission'],
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        if($isCreated){
            return $isCreated['commission_id'];
        }else{
            return null;
        }
    }

    public static function updateTournamentCommision($data)
    {
        $isUpdated = self::where('commission_id', $data['commission_id'])->update([
            'commission' => $data['commission'],
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        
        if($isUpdated){
            return $data['commission_id'];
        }else{
            return null;
        }
    }
}
