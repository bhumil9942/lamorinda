<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $guard = 'lamorinda';

    protected $table = 'students';

    public $timestamps=false;

    protected $primaryKey = 'student_id';

    protected $fillable = [
        'school_year', 'date_downloaded', 'fname', 'lname', 'address', 
        'city','state','zipcode','grade', 'school',
        'school_code', 'rte', 'pass_type', 'service_requested', 'waitlist', 
        'am_bus_stop','pm_bus_stop','p1_fname','p2_fname', 'email',
        'phone', 'alt_phone', 'authorize_1', 'authorize_2', 'authorize_3', 
        'disability','comments','monthly_payments','discount', 'free_transportation',
        'f_payment_type', 'f_cc', 'f_exp_date', 'f_amt_paid', 'f_date_payment_processed', 
        's_payment_type','s_cc','s_exp_date','s_amt_paid', 's_date_payment_processed',
        't_payment_type', 't_cc', 't_exp_date', 't_amt_paid', 't_date_payment_processed', 
        'received_by','refund_requested','refund_issued','amt_refunded', 'notes',
        'release_for_required', 'release_for_received', 'ride_bus_last_yr', 'date_printed', 'sub_id', 
        'created_at', 'updated_at', 'password',
    ];

    public static function getStudents()
    {
        $query = self::select();
        return $query->get();
    }

    public static function createStudent($data)
    {
        // echo '<pre>';print_r($data);exit;
        return self::create([
            'school_year' => $data['school_year'],
            'date_downloaded' => json_encode($data['date_downloaded'],TRUE),
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'address' => $data['address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'zipcode' => json_encode($data['zipcode'],TRUE),
            'grade' => json_encode($data['grade'],TRUE),
            'school' => $data['school'],
            'school_code' => $data['school_code'],
            'rte' => json_encode($data['rte'],TRUE),
            'pass_type' => $data['pass_type'],
            'service_requested' => json_encode($data['service_requested'],TRUE),
            'waitlist' => $data['waitlist'],
            'am_bus_stop' => $data['am_bus_stop'],
            'pm_bus_stop' => $data['pm_bus_stop'],
            'p1_fname' => $data['p1_fname'],
            'p2_fname' => $data['p2_fname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'alt_phone' => $data['alt_phone'],
            'authorize_1' => $data['authorize_1'],
            'authorize_2' => $data['authorize_2'],
            'authorize_3' => $data['authorize_3'],
            'disability' => $data['disability'],
            'comments' => $data['comments'],
            'monthly_payments' => $data['monthly_payments'],
            'discount' => $data['discount'],
            'free_transportation' => $data['free_transportation'],
            'f_payment_type' => $data['f_payment_type'],
            'f_cc' => $data['f_cc'],
            'f_exp_date' => $data['f_exp_date'],
            'f_amt_paid' => $data['f_amt_paid'],
            'f_date_payment_processed' => $data['f_date_payment_processed'],
            's_payment_type' => $data['s_payment_type'],
            's_cc' => $data['s_cc'],
            's_exp_date' => $data['s_exp_date'],
            's_amt_paid' => $data['s_amt_paid'],
            's_date_payment_processed' => $data['s_date_payment_processed'],
            't_payment_type' => $data['t_payment_type'],
            't_cc' => $data['t_cc'],
            't_exp_date' => $data['t_exp_date'],
            't_amt_paid' => $data['t_amt_paid'],
            't_date_payment_processed' => $data['t_date_payment_processed'],
            'received_by' => $data['received_by'],
            'refund_requested' => $data['refund_requested'],
            'refund_issued' => $data['refund_issued'],
            'amt_refunded' => $data['amt_refunded'],
            'notes' => $data['notes'],
            'release_for_required' => $data['release_for_required'],
            'release_for_received' => $data['release_for_received'],
            'ride_bus_last_yr' => $data['ride_bus_last_yr'],
            'date_printed' => json_encode($data['date_printed'],TRUE),
            'sub_id' => $data['sub_id'],
            'password' => $data['password'],
        ]);
    }

    public static function getStudentById($studentId)
    {
        $query = self::select()->where('students.student_id', $studentId);
        return $query->first();
    }

    public static function updateStudent($data)
    {
        $isUpdated = self::where('student_id', $data['student_id'])->update([
            'school_year' => $data['school_year'],
            'date_downloaded' => json_encode($data['date_downloaded'],TRUE),
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'address' => $data['address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'zipcode' => json_encode($data['zipcode'],TRUE),
            'grade' => json_encode($data['grade'],TRUE),
            'school' => $data['school'],
            'school_code' => $data['school_code'],
            'rte' => json_encode($data['rte'],TRUE),
            'pass_type' => $data['pass_type'],
            'service_requested' => json_encode($data['service_requested'],TRUE),
            'waitlist' => $data['waitlist'],
            'am_bus_stop' => $data['am_bus_stop'],
            'pm_bus_stop' => $data['pm_bus_stop'],
            'p1_fname' => $data['p1_fname'],
            'p2_fname' => $data['p2_fname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'alt_phone' => $data['alt_phone'],
            'authorize_1' => $data['authorize_1'],
            'authorize_2' => $data['authorize_2'],
            'authorize_3' => $data['authorize_3'],
            'disability' => $data['disability'],
            'comments' => $data['comments'],
            'monthly_payments' => $data['monthly_payments'],
            'discount' => $data['discount'],
            'free_transportation' => $data['free_transportation'],
            'f_payment_type' => $data['f_payment_type'],
            'f_cc' => $data['f_cc'],
            'f_exp_date' => $data['f_exp_date'],
            'f_amt_paid' => $data['f_amt_paid'],
            'f_date_payment_processed' => $data['f_date_payment_processed'],
            's_payment_type' => $data['s_payment_type'],
            's_cc' => $data['s_cc'],
            's_exp_date' => $data['s_exp_date'],
            's_amt_paid' => $data['s_amt_paid'],
            's_date_payment_processed' => $data['s_date_payment_processed'],
            't_payment_type' => $data['t_payment_type'],
            't_cc' => $data['t_cc'],
            't_exp_date' => $data['t_exp_date'],
            't_amt_paid' => $data['t_amt_paid'],
            't_date_payment_processed' => $data['t_date_payment_processed'],
            'received_by' => $data['received_by'],
            'refund_requested' => $data['refund_requested'],
            'refund_issued' => $data['refund_issued'],
            'amt_refunded' => $data['amt_refunded'],
            'notes' => $data['notes'],
            'release_for_required' => $data['release_for_required'],
            'release_for_received' => $data['release_for_received'],
            'ride_bus_last_yr' => $data['ride_bus_last_yr'],
            'date_printed' => json_encode($data['date_printed'],TRUE),
            'sub_id' => $data['sub_id'],
            'updated_at' => date('Y-m-d h:i:s'),
            'password' => $data['password'],
        ]);
        
        if($isUpdated){
            return $data['student_id'];
        }else{
            return null;
        }
    }

    public static function removeStudent($studentId)
    {
        return self::where('student_id', $studentId)->delete();
    }
}
