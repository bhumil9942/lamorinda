<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    protected $table = 'awards';

    protected $primaryKey = 'award_id';

    protected $fillable = [
        'award_name', 'award_image', 'created_at', 'updated_at',
    ];

    public static function getAwards()
    {
        $query = self::select();
        return $query->get();
    }

    public static function createAward($data)
    {
        return self::create([
            'award_name' => $data['award_name'],
            'award_image' => $data['awardimage_name'],
        ]);
    }

    public static function getAwardById($awardId)
    {
        $query = self::select()->where('awards.award_id', $awardId);
        return $query->first();
    }

    public static function updateAward($data)
    {
        if (array_key_exists("awardimage_name",$data)){
            $awardimage_name = $data['awardimage_name'];
        }else{
            $awardimage_name = $data['award_old_image'];
        }
        
        $isUpdated = self::where('award_id', $data['id'])->update([
            'award_name' => $data['award_name'],
            'award_image' => $awardimage_name,
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        
        if($isUpdated){
            return $data['id'];
        }else{
            return null;
        }
    }

    public static function removeAward($awardId)
    {
        return self::where('award_id', $awardId)->delete();
    }
}
