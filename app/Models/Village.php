<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    public static function getVillageBySubDistrict($sub_district_code)
	{
		return self::select('code','name')->where('sub_district_id', $sub_district_code)->orderby('name')->get();
    }

    public static function getVillageByDistrict($district_code)
	{
		return self::select('code','name')->where('district_id', $district_code)->orderby('name')->get();
	}
}
