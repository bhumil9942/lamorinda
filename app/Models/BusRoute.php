<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusRoute extends Model
{
    protected $table = 'routes';

    protected $primaryKey = 'bus_route_id';

    protected $fillable = [
        'bus_route_name', 'bus_route_status', 'available_pass', 'current_pass', 'created_at', 'updated_at',
    ];

    public static function getBusRoutes()
    {
        $query = self::select();
        return $query->get();
    }

    public static function createBusRoute($data)
    {
        return self::create([
            'bus_route_name' => $data['bus_route_name'],
            'bus_route_status' => $data['bus_route_status'],
            'available_pass' => $data['available_pass'],
            'current_pass' => $data['current_pass'],
        ]);
    }

    public static function getBusRouteById($bus_route_id)
    {
        $query = self::select()->where('routes.bus_route_id', $bus_route_id);
        return $query->first();
    }

    public static function updateBusRoute($data)
    {
        $isUpdated = self::where('bus_route_id', $data['bus_route_id'])->update([
            'bus_route_name' => $data['bus_route_name'],
            'bus_route_status' => $data['bus_route_status'],
            'available_pass' => $data['available_pass'],
            'current_pass' => $data['current_pass'],
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        
        if($isUpdated){
            return $data['bus_route_id'];
        }else{
            return null;
        }
    }

    public static function removeBusRoute($bus_route_id)
    {
        return self::where('bus_route_id', $bus_route_id)->delete();
    }

    public static function getRouteNameById($bus_route_id){
        return self::where('bus_route_id',$bus_route_id)->value('bus_route_name');
    }
}
