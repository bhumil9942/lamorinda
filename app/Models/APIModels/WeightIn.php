<?php

namespace App\Models\APIModels;

use Illuminate\Database\Eloquent\Model;
use Log;
use DB;

class WeightIn extends Model
{
    protected $table = 'weightin_description';

    protected $primaryKey = 'weightin_id';

    protected $fillable = [
        'user_id',
        'w_date', 
        'w_time', 
        'description',
        'weightin_lat',
        'weightin_lng',
        'tourny_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function insertWeightIn($data)
    {
        return self::create($data);
    }

    public static function getWeightIn($tourny_id)
    {   
        $weightData = [
            'weightin_where' => '',
            'weightin_date' => '',
            'weightin_time' => '',
            'weightin_lat' => '',
            'weightin_lng' => '',
        ];

        $weight =  DB::table('weightin_description')->where(['tourny_id'=>$tourny_id])->first();

        if(!empty($weight)){
            $finalWeight = date('m/d/y', strtotime($weight->w_date));
            $weightData['weightin_where'] = $weight->description;
            $weightData['weightin_date'] = $finalWeight;
            $weightData['weightin_time'] = $weight->w_time;
            $weightData['weightin_lat'] = $weight->weightin_lat;
            $weightData['weightin_lng'] = $weight->weightin_lng;
        }
        return $weightData;
    }
}
