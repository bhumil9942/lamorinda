<?php

namespace App\Models\APIModels;

use Illuminate\Database\Eloquent\Model;
use DB;

class Chat extends Model
{
    protected $table = 'chats';

    protected $primaryKey = 'chat_id';

    protected $fillable = [
        'sender_id', 'recipient_id', 'conversation_id', 'message', 'type', 'read_count'
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
    
    public static function storeChat($data)
    {
        return self::create([
            'sender_id'        => $data['sender_id'],
            'recipient_id'     => $data['recipient_id'],
            'conversation_id'  => $data['sender_id']."_".$data['recipient_id'],
            'message'          => $data['message'],
            'type'             => $data['type'],
            'read_count'       => "1",
        ]);
    }

    public static function chatList($userId)
    {
        // return DB::select('SELECT * , (
        //     r.sender_id + r.recipient_id
        //     ) AS dist
        //     FROM (
        //     SELECT * 
        //     FROM  `chats` t
        //     WHERE (
        //     t.sender_id ='.$userId.'
        //     OR t.recipient_id ='.$userId.'
        //     )
        //     ORDER BY t.created_at DESC
        //     )r
        //     GROUP BY dist
        //     ORDER BY r.created_at DESC
        //     ');

        return DB::select('SELECT * 
        FROM  `chats`  `t` 
        WHERE chat_id IN (SELECT MAX(s.chat_id) FROM chats s WHERE s.`sender_id` ='.$userId.'
        OR  s.`recipient_id` ='.$userId.' GROUP BY (IF(s.`sender_id`='.$userId.', s.`recipient_id`, s.`sender_id`)))
        ORDER BY created_at DESC');
    }

    public static function unreadCount($sender_id, $recipient_id)
    {
        return self::where(['sender_id'=>$sender_id,'recipient_id'=>$recipient_id, 'read_count'=>1])->count(); 
    }

    public static function chatDetails($sender_id, $recipient_id)
    {
        //Update Unread Count
        self::Where(function ($query) use ($sender_id, $recipient_id) {
            $query->where('sender_id', '=', $recipient_id)
                ->where('recipient_id', '=', $sender_id);
        })->update(array('read_count' => 0));

        //Fetch Conversation Between Two Users
        return self::where(function ($query) use ($sender_id, $recipient_id) {
            $query->where('sender_id', '=', $sender_id)
                ->where('recipient_id', '=', $recipient_id);
        })->orWhere(function ($query) use ($sender_id, $recipient_id) {
            $query->where('sender_id', '=', $recipient_id)
                ->where('recipient_id', '=', $sender_id);
        })->orderBy('created_at', 'DESC')->get();
    }
}
