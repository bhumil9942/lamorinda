<?php

namespace App\Models\APIModels;

use Illuminate\Database\Eloquent\Model;
use App\Models\Spice;
use App\Models\APIModels\WOFAuth;
use DB;

class Follower extends Model
{
    protected $table = 'followers';

    protected $primaryKey = 'follower_id';

    protected $fillable = [
        'user_id', 'followTo_id', 'isFollow',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function saveFollower($data)
    {
        return self::create([
            'user_id'     => $data['user_id'],
            'followTo_id' => $data['follower_id'],
            'isFollow'    => 1,
        ]);
    }

    public static function ExistsFollower($userId, $followerId)
    {
        return self::where(function ($query) use ($userId, $followerId) {
            $query->where('user_id', '=', $userId)
                ->where('followTo_id', '=', $followerId);
        })->orWhere(function ($query) use ($userId, $followerId) {
            $query->where('user_id', '=', $followerId)
                ->where('followTo_id', '=', $userId);
        })->first();
    }

    public static function FollowsYou($data)
    {
        $where = [
            'user_id'     => $follower_id,
            'followTo_id' => $user_id,
        ];
        return self::where($where)->first();
    }

    public static function deleteFollower($userId, $followerId)
    {
        return self::where(function ($query) use ($userId, $followerId) {
            $query->where('user_id', '=', $userId)
                ->where('followTo_id', '=', $followerId);
        })->orWhere(function ($query) use ($userId, $followerId) {
            $query->where('user_id', '=', $followerId)
                ->where('followTo_id', '=', $userId);
        })->delete();
    }

    public static function getFollowersById($userId)
    {
        $data = self::where(function ($query) use ($userId) {
            $query->where('user_id', '=', $userId)
                  ->where('isFollow', '=', '1');
        })->orWhere(function ($query) use ($userId) {
            $query->where('followTo_id', '=', $userId)
                   ->where('isFollow', '=', '1');
        })->get()->toArray();
        $final_array = array(); $i=0;
        foreach ($data as $key => $val) 
        {
            if($userId == $val['user_id'])
            {
                $final_array[$i]['follower_id'] = $val['followTo_id'];
            }
            else
            {
                $final_array[$i]['follower_id'] = $val['user_id'];
            }
            $final = WOFAuth::fetchUser($final_array[$i]['follower_id']);
            $final_array[$i]['follower_name'] = $final['username'];
            $final_array[$i]['fullname'] = $final['fullname'];
            $final_array[$i]['email'] = $final['email'];
            $final_array[$i]['mobile'] = $final['mobile'];
            $final_array[$i]['image'] = $final['image'];
            $final_array[$i]['dob'] = $final['dob'];
            $final_array[$i]['address'] = $final['address'];
            $final_array[$i]['city'] = $final['city'];
            $final_array[$i]['state'] = $final['state'];
            $final_array[$i]['boat_name'] = $final['boat_name'];
            $final_array[$i]['boat_type'] = $final['boat_type'];
            $final_array[$i]['zipcode'] = $final['zipcode'];
            $i++;
        }
        return $final_array;
    }

    public static function getFollowersByCount($userId)
    {
        $data = self::where(function ($query) use ($userId) {
            $query->where('user_id', '=', $userId)
                  ->where('isFollow', '=', '1');
        })->orWhere(function ($query) use ($userId) {
            $query->where('followTo_id', '=', $userId)
                   ->where('isFollow', '=', '1');
        })->get()->toArray();
        //$final_array = array(); $i=0;
        
        return  count($data);
    }

    public static function getFollowingOrNot($userId,$myId)
    {
        $data = self::where(function ($query) use ($userId,$myId) {
            $query->where('user_id', '=', $userId)
                   ->where('followTo_id', '=', $myId);
                  //->where('isFollow', '=', '1');
        })->orWhere(function ($query) use ($userId,$myId) {
            $query->where('followTo_id', '=', $userId)
                   ->where('user_id', '=', $myId);
                 //  ->where('isFollow', '=', '1');
        })->get()->toArray();
        if(count($data) >= 1){
            $following = '1';
        }else{
            $following = '0';
        } 
        return  $following;
    }

}
