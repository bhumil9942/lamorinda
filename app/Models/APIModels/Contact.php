<?php

namespace App\Models\APIModels;

use Illuminate\Database\Eloquent\Model;
use Mail;
use DB;

class Contact extends Model
{
    protected $table = 'contacts';

    protected $primaryKey = 'contact_id';

    protected $fillable = [
        'user_id', 'name', 'email', 'contact_no', 'contact_desc'
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function fillContact($data)
    {
        if(array_key_exists("desc",$data)){
            $contact_desc = $data['contact_desc'];
        }else{
            $contact_desc = NULL;
        }
        
        //Send Email To Admin
        Mail::send('emails.contactEmail', ['mail_data' => $data], function ($m) use($data){
            $email_address = env("MAIL_USERNAME", "admin123@gmail.com");
            $m->to($email_address, 'WOF Contact Form')->subject('Contact Form');
        });

        return self::create([
            'user_id'       => $data['user_id'],
            'name'          => $data['name'],
            'email'         => $data['email'],
            'contact_no'    => $data['contact_no'],
            'contact_desc'  => $contact_desc,
        ]);
    }

    public static function getContacts()
    {
        $query = self::select();
        return $query->get();
    }
}
