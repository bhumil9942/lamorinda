<?php

namespace App\Models\APIModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;
use DB;

class WOFAuth extends Authenticatable
{
    use Notifiable;

    protected $guard = 'wof';

    protected $table = 'wof_users';

    protected $primaryKey = 'user_id';

    protected $fillable = [
        'username', 'fullname', 'email', 'mobile', 'image', 'lat', 'lon'
    ];

    protected $hidden = [
        'password','created_at', 'updated_at',
    ];

    public static function userRegistration($data)
    {
        if(!array_key_exists("current_lat",$data)){
            $user_lat = '0.00';
        }else{
            $user_lat = $data['current_lat'];
        }

        if(!array_key_exists("current_lng",$data)){
            $user_lon = '0.00';
        }else{
            $user_lon = $data['current_lng'];
        }

        //Insert User Data
        $user = new WOFAuth();
        $user->username      = $data['username'];
        $user->fullname      = $data['fullname'];
        $user->mobile        = $data['mobile'];
        $user->email         = $data['email'];
        $user->password      = Hash::make($data['password']);
        $user->image         = $data['image'];
        $user->lat           = $user_lat;
        $user->lon           = $user_lon;
        $user->save();

        //Return User Data
        return $user;
    }

    public static function fetchUser($userId)
    {
        return self::where(['user_id'=>$userId])->first();
    }

    public static function fetchUserName($userId)
    {
        return self::where(['user_id'=>$userId])->value('username');
    }

    public static function fetchFullName($userId)
    {
        return self::where(['user_id'=>$userId])->value('fullname');
    }

    public static function fetchUserImage($userId)
    {
        return self::where(['user_id'=>$userId])->value('image');
    }

    public static function getwalletAmount($user_id)
    {
        $wallet = DB::table('wallet_amount')->where(['user_id' => $user_id])->first();
        return $wallet->wallet_amount;
    }

    public static function getWalletHistory($userId)
    {
        return DB::table('wallets')->where('user_id', '=', $userId)->orderBy('wallets_id', 'DESC')->get()->toArray();
    }

    public static function getrequestedpayment($data)
    {
        $commission = DB::table('tournament_commission')->where(['commission_id' => 1])->value('commission');
        $wallet_amount = DB::table('wallet_amount')->where(['user_id' => $data['user_id']])->value('wallet_amount');
        $data['requested_amount'] = $wallet_amount; 
            $to_be_paid_amount = $data['requested_amount'];
            return DB::table('request_payment')->insert([
                'user_id' => $data['user_id'],
                'requested_amount' => $data['requested_amount'],
                'commission' => $commission,
                'to_be_paid_amount' => $to_be_paid_amount,
                'status' => "0",
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s'),
            ]);
    }

    public static function getPaymentHistory($userId)
    {
        return DB::table('stripe_payment')->where('user_id', '=', $userId)->orderBy('stripe_payment_id', 'DESC')->get()->toArray();
    }
}
