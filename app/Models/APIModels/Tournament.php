<?php

namespace App\Models\APIModels;

use Illuminate\Database\Eloquent\Model;
use App\Models\Spice;
use App\Models\APIModels\JoinTournament;
use App\Models\APIModels\WOFAuth;
use DateTimeZone;
use \Carbon\Carbon;
use Datetime;
use Config;
use DB;
use Log;

class Tournament extends Model
{
    protected $table = 'tournaments';

    protected $primaryKey = 'tournament_id';

    protected $fillable = [
        'user_id', 
        'tourny_radius', 
        'tourny_name',
        'start_date',
        'start_time',
        'end_date',
        'end_time',
        'start_date_time',
        'end_date_time',
        'tourny_location',
        'tourny_lat',
        'tourny_long',
        'tourny_fish_species',
        'tourny_min_entry',
        'how_to_win',
        'first_winner',
        'second_winner',
        'third_winner',
        'hoster_percentage',
        'tourny_type',
        'tourny_image',
        'tourny_desc',
        'tourny_pot',
        'tourny_status',
        'tourny_host',        
        'length', 
        'weight', 
        'image', 
        'tourny_attendees_type', 
        'tourny_attendees',
        'np_organization_name',
        'tourny_join_after_start',
        'tourny_voting_days',
        'fish_name',
        'image',
        'first_winner_userid',
        'first_winner_amount',
        'second_winner_userid',
        'second_winner_amount',
        'third_winner_userid',
        'third_winner_amount',
        'tourny_count',
        'ranking_count',
        'distance',
        'timezone'
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function addTournament($data)
    {  
        
        $start_date_time = $data['start_date_time'];
        $end_date_time   = $data['end_date_time'];


        //convert date & time
        $start_date = date('Y-m-d', strtotime($data['start_date']));
        $end_date   = date('Y-m-d', strtotime($data['end_date']));
        $start_time = date('h:iA', strtotime($data['start_time']));
        $end_time   = date('h:iA', strtotime($data['end_time']));

        // $new_start_time = date("h:iA", strtotime('+1 hours', strtotime($start_time)));
        // $new_end_time   = date("h:iA", strtotime('+1 hours', strtotime($end_time)));

        if(!array_key_exists("tourny_desc",$data)){
            $data['tourny_desc'] = NULL;
        }
        //Add New Spice
        if(array_key_exists("tourny_fish_species",$data)){
            $spice = Spice::getSpiceById($data['tourny_fish_species']);
            if(!empty($spice)){
                $spiceId = $spice->id;
            }
            else{
                Spice::insert([
                    'spice_name' => $data['species_name'],
                    'active_status' => 1,
                ]);
                $last_inserted = Spice::orderBy('id', 'DESC')->first();
                if(!empty($last_inserted)){
                    $spiceId = $last_inserted->id;
                }
            }
        }else{
            $spiceId = NULL;
        }

        //Collection Tournament Specification Data
        if($data['how_to_win'] == 1){
            $first_winner  = $data['first_winner'];
            $second_winner = $data['second_winner'];
            $third_winner  = $data['third_winner'];
        }
        else if($data['how_to_win'] == 0){
            $first_winner  = 100;
            $second_winner = NULL;
            $third_winner  = NULL;
        }

        if($data['tourny_attendees_type'] == 3){
            $organization_name  = $data['np_organization_name'];
        }
        else{
            $organization_name = NULL;
        }

        if($data['tourny_image'] == NULL){
            $tourny_image = 'fish-default.jpg';
        }
        else{
            $tourny_image = $data['tourny_image'];
        }

        if(!array_key_exists("hoster_percentage",$data)){
            $data['hoster_percentage'] = 0;
        }
        return self::create([
            'user_id'                      => $data['user_id'],
            'tourny_radius'                => $data['tourny_radius'],
            'tourny_name'                  => $data['tourny_name'],
            'start_date'                   => $start_date,
            'start_time'                   => $start_time,
            'end_date'                     => $end_date,
            'end_time'                     => $end_time,
            'start_date_time'              => $start_date_time,
            'end_date_time'                => $end_date_time,
            'tourny_location'              => $data['tourny_location'],
            'tourny_lat'                   => $data['tourny_lat'],
            'tourny_long'                  => $data['tourny_lng'],
            'tourny_fish_species'          => $spiceId,
            'tourny_min_entry'             => $data['tourny_min_entry'],
            'how_to_win'                   => $data['how_to_win'],
            'first_winner'                 => $first_winner,
            'second_winner'                => $second_winner,
            'third_winner'                 => $third_winner,
            'hoster_percentage'            => $data['hoster_percentage'],
            'tourny_type'                  => $data['tourny_type'],
            'tourny_image'                 => $tourny_image,
            'tourny_desc'                  => $data['tourny_desc'],
            'tourny_attendees_type'        => $data['tourny_attendees_type'],
            'tourny_attendees'             => $data['tourny_attendees'],
            'np_organization_name'         => $organization_name,
            'tourny_join_after_start'      => $data['tourny_join_after_start'],
            'tourny_voting_days'           => $data['tourny_voting_days'],
            'timezone'                     => $data['timezone'],
        ]);
    }

    public static function editTournament($data)
    { 
        //convert date & time
        $start_date = date('Y-m-d', strtotime($data['start_date']));
        $end_date   = date('Y-m-d', strtotime($data['end_date']));
        $start_time = date('h:i A', strtotime($data['start_time']));
        $end_time   = date('h:i A', strtotime($data['end_time']));
        
        if(!array_key_exists("tourny_desc",$data)){
            $data['tourny_desc'] = NULL;
        }
        //Add New Spice
        if(array_key_exists("tourny_fish_species",$data)){
            $spice = Spice::getSpiceById($data['tourny_fish_species']);
            if(!empty($spice)){
                $spiceId = $spice->id;
            }
            else{
                Spice::insert([
                    'spice_name' => $data['species_name'],
                    'active_status' => 1,
                ]);
                $last_inserted = Spice::orderBy('id', 'DESC')->first();
                if(!empty($last_inserted)){
                    $spiceId = $last_inserted->id;
                }
            }
        }else{
            $spiceId = NULL;
        }

        //Collection Tournament Specification Data
        if($data['how_to_win'] == 1){
            $first_winner  = $data['first_winner'];
            $second_winner = $data['second_winner'];
            $third_winner  = $data['third_winner'];
        }
        else if($data['how_to_win'] == 0){
            $first_winner  = 100;
            $second_winner = NULL;
            $third_winner  = NULL;
        }

        if($data['tourny_attendees_type'] == 3){
            $organization_name  = $data['np_organization_name'];
        }
        else{
            $organization_name = NULL;
        }

        if(!array_key_exists("tourny_image",$data)){
            $tourny_image = 'fish-default.jpg';
        }else{
            $tourny_image = $data['tourny_image'];
        }
        
        if($data['tourny_attendees_type'] == 2){
            $tourny_attendees  = $data['tourny_attendees'];
        }
        else{
            $tourny_attendees = NULL;
        }

        $tourny_id = $data['tourny_id'];
        $data = array(
            'user_id'                      => $data['user_id'],
            'tourny_radius'                => $data['tourny_radius'],
            'tourny_name'                  => $data['tourny_name'],
            'start_date'                   => $start_date,
            'start_time'                   => $start_time,
            'end_date'                     => $end_date,
            'end_time'                     => $end_time,
            'tourny_location'              => $data['tourny_location'],
            'tourny_lat'                   => $data['tourny_lat'],
            'tourny_long'                  => $data['tourny_lng'],
            'tourny_fish_species'          => $spiceId,
            'tourny_min_entry'             => $data['tourny_min_entry'],
            'how_to_win'                   => $data['how_to_win'],
            'first_winner'                 => $first_winner,
            'second_winner'                => $second_winner,
            'third_winner'                 => $third_winner,
            'tourny_type'                  => $data['tourny_type'],
            'tourny_image'                 => $tourny_image,
            'tourny_desc'                  => $data['tourny_desc'],
            'tourny_attendees_type'        => $data['tourny_attendees_type'],
            'tourny_attendees'             => $tourny_attendees,
            'np_organization_name'         => $organization_name,
            'tourny_join_after_start'      => $data['tourny_join_after_start'],
            'tourny_voting_days'           => $data['tourny_voting_days']
        );
        $tourny_data = DB::table('tournaments')->where(['tournament_id'=>$tourny_id])->update($data);
        return $data;
    }

    public static function getAttendeeName($tourny_attendees_type,$tourny_attendees)
    {
        if($tourny_attendees_type == '2')
        {
            $data = explode(',',$tourny_attendees);
            $attendees_name = array();
            foreach($data as $name)
            {
                $attendees_name[] = WOFAuth::fetchUserName($name);
            }
            $names = implode(',', $attendees_name);
            return $names;            
        }else{
            $data = "";
            return $data; 
        }
    }

    public static function getTournamentById($tournamentId)
    {
        $query = self::select('tournaments.*', 'spices.spice_name', 'spices.active_status')
                    ->join('spices', 'spices.id', 'tournaments.tourny_fish_species')
                    ->where('tournaments.tournament_id', $tournamentId);
        return $query->first();
    }

    public static function getTournamentByUserId($userId)
    {
        return self::select('tournaments.*','spices.id as species_id','spices.spice_name as species_name')
                ->join('spices', 'spices.id', 'tournaments.tourny_fish_species')
                ->where('tournaments.user_id', $userId)
                ->get();
    }

    public static function getTournament()
    {
        return self::select()->orderby('created_at','DESC')->get();
    }

    public static function TournamentById($tournamentId)
    {
        return self::where(['tournament_id'=>$tournamentId])->first();
    }

    public static function getUpcomingTournament($covertedDateTime)
    {
       return self::select('tournaments.*','spices.id as species_id','spices.spice_name as species_name')
            ->join('spices', 'spices.id', 'tournaments.tourny_fish_species')
            ->where('tournaments.start_date_time', '>=' , $covertedDateTime)
            ->orderBy('tournaments.start_date_time', 'ASC')
            ->get();
    }

    public static function activeTournament($covertedDateTime)
    {
        return self::select('tournaments.*','spices.id as species_id','spices.spice_name as species_name')
            ->join('spices', 'spices.id', 'tournaments.tourny_fish_species')
            ->where('tournaments.start_date_time', '<=', $covertedDateTime)
            ->where('tournaments.end_date_time', '>=', $covertedDateTime)
            ->orderBy('tournaments.start_date_time', 'ASC')
            ->get();
    }

    public static function upcomingTournySpicesIds($covertedDateTime)
    {
        return self::where('tournaments.start_date_time', '>=' , $covertedDateTime)->orderBy('tournaments.start_date_time', 'ASC')->pluck('tourny_fish_species');
    }

    public static function getCompletedTournament()
    {
        $currenttime = date('h:iA');
        return self::select('tournaments.*','spices.id as species_id','spices.spice_name as species_name')
                ->join('spices', 'spices.id', 'tournaments.tourny_fish_species')
                ->whereDate('tournaments.start_date', '<', date("Y-m-d"))
                ->where('tournaments.start_time', '<', $currenttime)
                ->whereDate('tournaments.end_date', '<', date("Y-m-d"))
                ->where('tournaments.end_time', '<', $currenttime)
                ->get();
    }
    public static function getTournamentDetails()
    {
        return self::select('tournaments.*','tourny.id as tourny_id')
                ->whereDate('tournaments.start_date', '<=', date("D-m-y"))
                ->whereDate('tournaments.end_date', '>=', date("D-m-y"))
                ->get();
    }

    public static function getPotById($tournamentId)
    {
      $pot = (float)self::where(['tournament_id'=>$tournamentId])->value('tourny_min_entry');
      $count = (float)JoinTournament::where(['tournament_id'=>$tournamentId])->count();
      $tourny_pot = $pot*$count;
      return $tourny_pot;  
    }
    
    public static function getSpicesName($species_id){
        $species_data = DB::table('spices')->where(['id'=>$species_id])->first();
        return $species_data['spice_name'];
    }

    public static function getSpiceName($species_id){
        $species_data = DB::table('spices')->where(['id'=>$species_id])->value('spice_name');
        return $species_data;
    }

    public static function joinToWatchList($data)
    {       
        $values = [
            'tournament_id' => $data['tourny_id'],
            'user_id' => $data['user_id'], 
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ];
        return DB::table('watch_users')->insert($values);
    }

    public static function joinedWatchUser($tourny_id,$user_id)
    {
        $where = 
        [
            'user_id'         => $user_id,
            'tournament_id'   => $tourny_id,
        ];
        return DB::table('watch_users')->where($where)->first();
    }

    public static function getAttendeeById($tournamentId)
    {
      $watch_users = DB::table('watch_users')->where(['tournament_id'=>$tournamentId])->get();
      $joined_users = JoinTournament::where(['tournament_id'=>$tournamentId])->get();
      $watch_users = json_decode(json_encode($watch_users), true);
      $joined_users = json_decode(json_encode($joined_users), true);
      $combined_users = array_merge($watch_users,$joined_users);
      $final_array = array(); $i=0;
      foreach ($combined_users as $key => $val) {
        $final_array[$i]['user_id'] = $val['user_id'];
        $final_array[$i]['tournament_id'] = $val['tournament_id'];
        $i++;
      }
      $input = array_map("unserialize", array_unique(array_map("serialize", $final_array)));
      if(!empty($input)){
          $attendies_count = count($input);
      }else{
        $attendies_count = '0';
      }
      return $attendies_count;
    }

    public static function submitFish($data)
    {
        $values = array
                        (
                            'tournament_id' => $data['tourny_id'],
                            'user_id'       => $data['user_id'], 
                            'length'        => $data['length'],
                            'weight'        => $data['weight'], 
                            'image'         => $data['image'],                             
                            'created_at'    => date("Y-m-d"),
                            'updated_at'    => date("Y-m-d"));
        return DB::table('submit_your_fish')->insert($values);
    }

    // function distanceMiles($userId, $tourny_id, $user_lat, $user_lng, $tourny_lat, $tourny_lng) {

    //     $user_lat = 40.730610;
    //     $user_lng = -73.935242;
    //     $tourny_lat = DB::table('tournaments')->where(['user_id' => $userId, 'tournament_id ' => $tourny_id])->value('tourny_lat');
    //     $tourny_lng = DB::table('tournaments')->where(['user_id' => $userId, 'tournament_id ' => $tourny_id])->value('tourny_long');

    //     if (($user_lat == $tourny_lat) && ($user_lng == $tourny_lng)) {
	// 		return 0;
	// 	} else {

	// 		$theta = $user_lng - $tourny_lng;
	// 		$dist = sin(deg2rad($user_lat)) * sin(deg2rad($tourny_lat)) +  cos(deg2rad($user_lat)) * cos(deg2rad($tourny_lat)) * cos(deg2rad($theta));
	// 		$dist = acos($dist);
	// 		$dist = rad2deg($dist);
	// 		$miles = $dist * 60 * 1.1515;
	// 		return round($miles, 2);
	// 	}
    //   }

    public static function alreadySubmitted($tourny_id,$user_id)
    {
        $submitfish = DB::table('submit_your_fish')->where(['user_id'=>$user_id,'tournament_id'=>$tourny_id])->get()->toArray();
        if(!empty($submitfish))
        {
            $status='1';
        }
        else
        {
            $status='0';
        }
        return $status;
        // return DB::table('submit_your_fish')->where($where)->first();
    }

    public static function getTournamentStatus1($start_date, $end_date, $start_time, $end_time, $tourny_timezone, $usertimezone, $result)
    {
        // getServerDateTime
        $timezone = Config::get('app.timezone');
        $cdate = $start_date.' '.date('H:i:s', strtotime($start_time));
        $edate = $end_date.' '.date('H:i:s', strtotime($end_time));

        $cdate = Carbon::createFromFormat('Y-m-d H:i:s', $cdate);
        $cdate->setTimezone(new DateTimeZone($tourny_timezone));
        $formatted_start_date = $cdate->format('Y-m-d H:i:s');

        $edate = Carbon::createFromFormat('Y-m-d H:i:s', $edate);
        $edate->setTimezone(new DateTimeZone($tourny_timezone));
        $formatted_end_date = $edate->format('Y-m-d H:i:s');

        $severStartDate =  date('Y-m-d',strtotime($formatted_start_date));
        $severEndDate =  date('Y-m-d',strtotime($formatted_end_date));
        $severStartTime =  date('g:i a', strtotime($formatted_start_date));
        $severEndTime =  date('g:i a', strtotime($formatted_end_date));

        $start_date =  $severStartDate;
        $end_date =  $severEndDate;
        $start_time = $severStartTime;
        $end_time = $severEndTime;

        // $start_date =  date('Y-m-d',strtotime($start_date));
        // $end_date =  date('Y-m-d',strtotime($end_date));
        // $start_time =  date('g:i a', strtotime($start_time));
        // $end_time =  date('g:i a', strtotime($start_time));

        $current_date = date('Y-m-d'); 
        $current_time = date('h:iA');
        $current_time = date('g:i a', strtotime($current_time));
        // $start_time = date('g:i a', strtotime($start_time));
        // $end_time = date('g:i a', strtotime($end_time));
        
        $status = "";
        if($start_date == $current_date) {
            if($end_date == $current_date) {
                if($start_time > $current_time) {
                    $status = ($result == 'status') ? 'Upcoming' : "2";
                }else if($start_time <= $current_time) {
                    if($end_time > $current_time) {
                        $status = ($result == 'status') ? 'Started' : "1";
                    }else if($end_time <= $current_time) {
                        $status = ($result == 'status') ? 'Completed' : "3";
                    }
                }else if($end_time <= $current_time) {
                    $status = ($result == 'status') ? 'Completed' : "3";
                }
            }else if($end_date > $current_date) {
                if($start_time > $current_time) {
                    $status = ($result == 'status') ? 'Upcoming' : "2";
                }else if($start_time <= $current_time) {
                    $status = ($result == 'status') ? 'Started' : "1";
                }
            }
        }else if($start_date > $current_date) {
            if($end_date > $current_date) {
                $status = ($result == 'status') ? 'Upcoming' : "2";
            }else if($end_date < $current_date) {
                $status = ($result == 'status') ? 'Completed' : "3";
            }
        }else if($start_date < $current_date) {
            if($end_date < $current_date) {
                $status = ($result == 'status') ? 'Completed' : "3";
            }else if($end_date >= $current_date) {
                $status = ($result == 'status') ? 'Started' : "1";
            }
        }
        
        return $status;
    }

    public static function getTournamentStatus($activeTournament, $upcomingTournament, $tournyId, $status)
    {
        $result = "";
        if(array_search($tournyId, array_column($activeTournament, 'tournament_id')) !== false){
            if($status == 'status'){
                $result = 'Started';
            }else{
                $result = '1';
            }
        }else if(array_search($tournyId, array_column($upcomingTournament, 'tournament_id')) !== false){
            if($status == 'status'){
                $result = 'Upcoming';
            }else{
                $result = '2';
            }
        }else{
            if($status == 'status'){
                $result = 'Completed';
            }else{
                $result = '3';
            }
        }

        return $result;
    }

    function getServerDateTime($timezone){
        $cdate = date('Y-m-d H:i:s');
        $cdate = Carbon::createFromFormat('Y-m-d H:i:s', $cdate);
        $cdate->setTimezone(new DateTimeZone($timezone));
        $formatted_date = $cdate->format('Y-m-d H:i:s');
        return date('Y-m-d H:i:s',strtotime($formatted_date));
    }

    public static function getTournamentStatusCode($start_date, $end_date, $start_time, $end_time, $tourny_timezone, $usertimezone)
    {
        // getServerDateTime
        $timezone = Config::get('app.timezone');
        $cdate = $start_date.' '.date('H:i:s', strtotime($start_time));
        $edate = $end_date.' '.date('H:i:s', strtotime($end_time));

        $cdate = Carbon::createFromFormat('Y-m-d H:i:s', $cdate);
        $cdate->setTimezone(new DateTimeZone($tourny_timezone));
        $formatted_start_date = $cdate->format('Y-m-d H:i:s');

        $edate = Carbon::createFromFormat('Y-m-d H:i:s', $edate);
        $edate->setTimezone(new DateTimeZone($tourny_timezone));
        $formatted_end_date = $edate->format('Y-m-d H:i:s');

        $severStartDate =  date('Y-m-d',strtotime($formatted_start_date));
        $severEndDate =  date('Y-m-d',strtotime($formatted_end_date));
        $severStartTime =  date('g:i a', strtotime($formatted_start_date));
        $severEndTime =  date('g:i a', strtotime($formatted_end_date));

        $start_date =  $severStartDate;
        $end_date =  $severEndDate;
        $start_time = $severStartTime;
        $end_time = $severEndTime;

        // $start_date =  date('Y-m-d',strtotime($start_date));
        // $end_date =  date('Y-m-d',strtotime($end_date));
        // $start_time =  date('g:i a', strtotime($start_time));
        // $end_time =  date('g:i a', strtotime($start_time));

        $current_date = date('Y-m-d');
        $current_time = date('h:iA');
        $current_time = date('g:i a', strtotime($current_time));
        // $start_time = date('g:i a', strtotime($start_time));
        // $end_time = date('g:i a', strtotime($end_time));

        $status = "";
        if($start_date == $current_date) {
            if($end_date == $current_date) {
                if($start_time > $current_time) {
                    $status = "1";
                }else if($start_time <= $current_time && $end_time >= $current_time) {
                    if($end_time > $current_time) {
                        $status = "2";
                    }else if($end_time <= $current_time) {
                        $status = "3";
                    }
                }else if($end_time <= $current_time) {
                    $status = "3";
                }
            }else if($end_date > $current_date) {
                if($start_time > $current_time) {
                    $status = "1";
                }else if($start_time <= $current_time) {
                    $status = "2";
                }
            }
        }else if($start_date > $current_date) {
            if($end_date > $current_date) {
                $status = "2";
            }else if($end_date < $current_date) {
                $status = "3";
            }
        }else if($start_date < $current_date) {
            if($end_date < $current_date) {
                $status = "3";
            }else if($end_date >= $current_date) {
                $status = "2";
            }
        }
        return $status;
    } 

    public static function getAttendeeType($tourny_attendees_type)
    {
        if($tourny_attendees_type == '1')
        {
            $status = 'Public';
        }
        else if($tourny_attendees_type == '2')
        {
            $status = 'Private';
        }
        else if($tourny_attendees_type == '3')
        {
            $status = 'Charity';
        }
        return $status;
    }

    public static function IsInvited($tourny_id,$user_id,$tourny_attendees_type)
    {
        if($tourny_attendees_type == '2'){
            $check_is_private = '0';
            $data = self::TournamentById($tourny_id);
            if($user_id == $data['user_id'])
            {
                $check_is_private = '1';
            }else
            {
                $attendees_check = explode(',',$data['tourny_attendees']);
                if(in_array($user_id,$attendees_check)){
                    $check_is_private = '1';
                }
            }           
        }else{
            $check_is_private = '0';
        }
        return $check_is_private;
    }    
    
    public static function addRatings($data)
    {
       $data = array(
        'tournament_id' => $data['tourny_id'],
        'user_id'       => $data['user_id'], 
        'rating'        => $data['rating'], 
        'type'          =>'tournament',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s'),
       );
       return DB::table('ratings')->insert($data);
    }

    public static function existsRating($user_id, $tourny_id)
    {
        $where = [
            'user_id'         => $user_id,
            'tournament_id'   => $tourny_id,
        ];
        return DB::table('ratings')->where($where)->first();
    }

    public static function getRatings($tourny_id)
    {
        $average_rating = 0;
        $total_ratings = 0;
        $count_ratings =  DB::table('ratings')->where(['tournament_id'=>$tourny_id])->get();
        $countable = count($count_ratings);
        if($countable > 0){
            for ($i=0; $i < $countable; $i++) { 
                $total_ratings += $count_ratings[$i]->rating;
            }
            $average_rating = $total_ratings / $countable;
        }
        return round($average_rating,1);
    }

    public static function watchingTourny($tournamentId,$userId)
    {
        $tournaments = DB::table('watch_users')
            ->where(['user_id'=>$userId])
            ->where(['tournament_id'=>$tournamentId])
            ->get()->toArray();
        if(!empty($tournaments))
        {
            $status='1';
        }
        else
        {
            $status='0';
        }
        return $status;
    }

    public static function results($data)
    {
        $data = array(
            'tournament_id'        => $data['tourny_id'],
            'first_winner_userid'  => $data['first_winner_userid'], 
            'first_winner_amount'  => $data['first_winner_amount'],
            'second_winner_userid' => $data['second_winner_userid'], 
            'second_winner_amount' => $data['second_winner_amount'], 
            'third_winner_userid'  => $data['third_winner_userid'], 
            'third_winner_amount'  => $data['third_winner_amount'], 
            'created_at'           => date("Y-m-d"),
            'updated_at'           => date("Y-m-d"),
           );
        return DB::table('results')->insertGetId($data);
    }

    public static function getTournamentCounts($user_id)
    {
       $where = [
            'user_id' => $user_id
        ];
        return DB::table('tournaments')->where(['user_id'=>$user_id])->count();
    }

    public static function getResultsList($results_id)
    {
        return DB::table('results')->where(['results_id'=>$results_id])->first();
    }
    
    public static function getAwardsList($user_id)
    {
        $var = DB::table('result_awards')
                 ->join('awards', 'result_awards.award_id', '=', 'awards.award_id')
                 ->select('result_awards.award_id','awards.award_name','awards.award_image', DB::raw('count(*) as total'))
                 ->groupBy('result_awards.award_id')
                 ->where(['result_awards.user_id'=>$user_id])->get()->toArray();
        $awards = array();
        $i=0;
        foreach($var as $award)
        {
            $awards[$i]['award_id'] = (string)$award->award_id;
            $awards[$i]['award_name'] = (string)$award->award_name;
            $awards[$i]['award_image'] = (string)$award->award_image;
            $awards[$i]['total'] = (string)$award->total;
            $i++;
        }
        return $awards;
    }

    public static function isExistsRating($user_id, $tourny_id)
    {
        $where = [
            'user_id'         => $user_id,
            'tournament_id'   => $tourny_id,
        ];
        $is_exists = DB::table('ratings')->where($where)->first();
        if(!empty($is_exists))
        {
            $rating ="1";
        }else{
            $rating = "0";
        }
        return $rating;
    }

    public static function getFishLength($user_id,$tourny_id)
    {
        $submitfish = DB::table('submit_your_fish')->where(['user_id'=>$user_id,'tournament_id'=>$tourny_id])->first();
        if(!empty($submitfish))
        {
            $fish_length=$submitfish->length;
        }
        else
        {
            $fish_length='0';
        }
        return $fish_length;
    }

    public static function getFishWeight($user_id,$tourny_id)
    {
        $submitfish = DB::table('submit_your_fish')->where(['user_id'=>$user_id,'tournament_id'=>$tourny_id])->first();
        if(!empty($submitfish))
        {
            $fish_weight=$submitfish->weight;
        }
        else
        {
            $fish_weight='0';
        }
        return $fish_weight;
    }

    public static function getFishImages($user_id,$tourny_id, $fish_images="")
    {
        $submitfish = DB::table('submit_your_fish')->where(['user_id'=>$user_id,'tournament_id'=>$tourny_id])->first();
        if(!empty($submitfish))
        {
            $fish_images = $submitfish->image;
        }
        return $fish_images;
    }

    public static function getFishByID($user_id,$tourny_id)
    { 
        $submitfish = DB::table('submit_your_fish')->where(['user_id'=>$user_id,'tournament_id'=>$tourny_id])->first();
        if(!empty($submitfish))
        {
            $fish_id=$submitfish->fish_id;
        }
        else
        {
            $fish_id='0';
        }
        return $fish_id;
    }

    public static function getIsFishSubmitted($user_id,$tourny_id)
    {
        $submitfish = DB::table('submit_your_fish')->where(['user_id'=>$user_id,'tournament_id'=>$tourny_id])->first();
        if(!empty($submitfish))
        {
            $is_fish_submitted='1';
        }
        else
        {
            $is_fish_submitted='0';
        }
        return $is_fish_submitted;
    }

    public static function getVotingCounts($fish_id,$user_id,$tourny_id)
    {
       $where = [
            'fish_id' => $fish_id,
            'user_id' => $user_id,
            'tournament_id' => $tourny_id,
        ];
        return DB::table('voting')->where(['fish_id'=>$fish_id,'user_id'=>$user_id,'tournament_id'=>$tourny_id])->count();
    }

    public static function getResults($tourny_id)
    {
        return DB::table('results')->where(['tournament_id'=>$tourny_id])->first();
    }

    public static function getIsResultCreated($tourny_id)
    {
        $create_result = DB::table('results')->where(['tournament_id'=>$tourny_id])->first();
        if(!empty($create_result))
        {
            $is_result_created='1';
        }
        else
        {
            $is_result_created='0';
        }
        return $is_result_created;
    }

    public static function isVoted($user_id,$tourny_id)
    {
        $voted = DB::table('voting')->where(['voter_user_id'=>$user_id],['tournament_id'=>$tourny_id])->get()->toArray();
        if(!empty($voted))
        {
            $is_voted='1';
        }
        else
        {
            $is_voted='0';
        }
        return $is_voted;
    }

    // public static function getVoteCounts($user_id,$tourny_id)
    // {
    //    $where = [
    //         'user_id' => $user_id,
    //         'tournament_id' => $tourny_id,
    //     ];
    //     return DB::table('voting')->where(['user_id'=>$user_id,'tournament_id'=>$tourny_id])->count();
    // }

    public static function getVoteCounts($user_id,$tourny_id)
    {
       $where = [
            'user_id' => $user_id,
            'tournament_id' => $tourny_id,
        ];

        return DB::table('voting')->where(['user_id'=>$user_id,'tournament_id'=>$tourny_id])->count();
    }

    public static function myTourny($user_id)
    {
        return DB::table('jointournament')
                //->join('results','results.tournament_id','=','jointournament.tournament_id')
                ->join('tournaments','tournaments.tournament_id','=','jointournament.tournament_id')
                ->where('jointournament.user_id', '=' , $user_id)
                ->get()->toArray();    
       
    }

    public function searchForId($id, $array) {
       foreach ($array as $key => $val) {
           if ($val['user_id'] === $id) {
               return $key;
           }
       }
       return null;
    }

    public static function getRankings($user_id, $tourny_id, $tourny_type)
    {
        if($tourny_type == 1){
            $orderBY = 'weight';
        }
        else if($tourny_type == 3){
            $orderBY = 'length';
        }
        $submitedFish = DB::table('submit_your_fish')->where(['tournament_id'=>$tourny_id])->orderBy($orderBY, 'ASC')->get();
        $submitedFish = json_decode(json_encode((object) $submitedFish), TRUE);

        $found_key = "0";
        foreach ($submitedFish as $key => $val) {
           if ($val['user_id'] === $user_id) {
               $found_key = $key;
           }
        }
        return $found_key + 1;
    }

    public static function matchResults($user_id,$tournament_id,$tourny_type)
    {
        $data = DB::table('results')->where(['tournament_id'=>$tournament_id])->first();
        $winner = '0';
        if(!empty($data))
        {
            if($user_id == $data->first_winner_userid)
            {
                $winner = '1';
            }elseif($user_id == $data->second_winner_userid)
            {
                $winner = '2';
            }elseif($user_id == $data->third_winner_userid)
            {
                $winner = '3';
           }else{
                $getrank = JoinTournament::contestantList($tournament_id,$tourny_type);
                $key = array_search($user_id, array_column($getrank, 'user_id'));
                $winner = $key + 1;
            }
        }else{
            $getrank = JoinTournament::contestantList($tournament_id,$tourny_type);
            $key = array_search($user_id, array_column($getrank, 'user_id'));
            $winner = $key + 1;
            
        }       
        return $winner;
    }

    public static function getFishImageById($tournament_id)
    {
        return DB::table('submit_your_fish')->where(['tournament_id'=>$tournament_id])->value('image');       
    }    

    public static function fetchTournyType($user_id)
    {
        return DB::table('tournaments')->where(['user_id'=>$user_id])->value('tourny_type');       
    }    

    
    public static function matchResultattendee($user_id,$tournament_id,$tourny_type)
    {
        $data = DB::table('results')->where(['tournament_id'=>$tournament_id])->first();
        $winner = '0';
        if(!empty($data))
        {
            if($user_id == $data->first_winner_userid)
            {
                $winner = '1';
            }elseif($user_id == $data->second_winner_userid)
            {
                $winner = '2';
            }elseif($user_id == $data->third_winner_userid)
            {
                $winner = '3';
           }else{
                $getrank = JoinTournament::attendeeList($tournament_id,$tourny_type);
                $key = array_search($user_id, array_column($getrank, 'user_id'));
                $winner = $key + 1;
            }
        }else{
            $getrank = JoinTournament::attendeeList($tournament_id,$tourny_type);
            $key = array_search($user_id, array_column($getrank, 'user_id'));
            $winner = $key + 1;
            
        }       
        return $winner;
    }
    
    public static function getTournyCounts()
    {
        return DB::table('tournaments')->count();
    }

    public static function totalWinnerCounts()
    {
        return DB::table('results')->count();
    }

    public static function distance($lat1, $lon1, $lat2, $lon2) 
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            return round($miles, 2);
        }
    }

    public static function getToken($user_id)
    {
        return DB::table('notifications')->select('notifications.*','api_tokens.device_token','api_tokens.device_type',)
                ->join('api_tokens', 'api_tokens.user_id', 'notifications.user_id')
                ->where('notifications.user_id', $user_id)
                ->get();
    }

    public static function getTournyEntry()
    {
        return DB::table('tournaments')->select('tourny_min_entry')->get()->toArray();
    }

    public static function hosterCommission()
    {
        $hoster_commision = DB::table('hoster_commission')->where(['commission_id'=> 1])->value('commission');
        return isset($hoster_commision) ? $hoster_commision : '0';
    }
}
