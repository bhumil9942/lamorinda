<?php

namespace App\Models\APIModels;
use Illuminate\Database\Eloquent\Model;
use App\Models\APIModels\WOFAuth;
use DB;

class JoinTournament extends Model
{
    protected $table = 'jointournament';

    protected $primaryKey = 'jointourny_id';

    protected $fillable = [
        'tournament_id', 'user_id', 'created_at',
    ];

    protected $hidden = [
        'updated_at',
    ];

    public static function fillTournament($data)
    {
        return self::create([
            'tournament_id' => $data['tourny_id'],
            'user_id'   => $data['user_id'],
        ]);
    }

    public static function joinedUser($tournamentId)
    {
        return self::select('jointournament.*','wof_users.username','wof_users.image','wof_users.fullname','tournaments.tourny_location', 'tournaments.tourny_type')
                ->join('wof_users','wof_users.user_id','=','jointournament.user_id')
                ->join('tournaments','tournaments.tournament_id','=','jointournament.tournament_id')
                ->where('jointournament.tournament_id', $tournamentId)
                ->get()->toArray();
    }

    public static function joinTourny($tournamentId,$userId)
    {
        $tournaments = self::select()
            ->where(['user_id'=>$userId])
            ->where(['tournament_id'=>$tournamentId])
            ->get()->toArray();
        if(!empty($tournaments))
        {
            $status='1';
        }
        else
        {
            $status='0';
        }
        return $status;
    }

    public static function joinedUserCounts($userId)
    {
        return DB::table('jointournament')->where(['user_id'=>$userId])->count(); 
    }

    public static function joinUserCounts($tournamentId)
    {
        return DB::table('jointournament')->where(['tournament_id'=>$tournamentId])->count(); 
    }    

    public static function contestantList($tournamentId, $tourny_type)
    {
        $contestants = self::select('jointournament.*','wof_users.fullname')
                ->where('jointournament.tournament_id', $tournamentId)
                ->join('wof_users','wof_users.user_id','=','jointournament.user_id')
                ->get()->toArray();
        $user = array();
        $i=0;
        foreach($contestants as $attendee)
        {
            $user[$i]['jointourny_id'] = (string)$attendee['jointourny_id'];
            $user[$i]['tournament_id'] = $attendee['tournament_id'];
            $user[$i]['user_id'] = $attendee['user_id'];
            $host_name = Tournament::getTournamentById($attendee['tournament_id'])['user_id'];
            $user[$i]['tourny_host_name'] = WOFAuth::fetchUserName($host_name);
            $user[$i]['tourny_name'] = Tournament::TournamentById($attendee['tournament_id'])['tourny_name'];
            $user[$i]['user_image'] = WOFAuth::fetchUserImage($attendee['user_id']);
            $user[$i]['submitted_by'] = WOFAuth::fetchUserName($attendee['user_id']);
            $user[$i]['fish_length'] = Tournament::getFishLength($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['fish_weight'] = Tournament::getFishWeight($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['is_fish_submitted'] = Tournament::getIsFishSubmitted($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['fish_id'] = (string)Tournament::getFishByID($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['ranking_count'] = (string)Tournament::getVotingCounts($user[$i]['fish_id'],$attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['fish_images'] = Tournament::getFishImages($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['tourny_location'] = Tournament::getTournamentById($attendee['tournament_id'])['tourny_location'];
            $user[$i]['contestant_name'] = WOFAuth::fetchUserName($attendee['user_id']);
            $user[$i]['is_voted'] = Tournament::isVoted($attendee['user_id'],$attendee['tournament_id']);
            $spices_name = Tournament::getTournamentById($attendee['tournament_id'])['tourny_fish_species'];
            $user[$i]['fish_name'] = Tournament::getSpiceName($spices_name);
            $user[$i]['admin_commission'] = (string)DB::table('tournament_commission')->where(['commission_id' => 1])->value('commission');            
            $i++;
        }
        if($tourny_type == 1)
        {
            array_multisort(array_column($user, 'fish_weight'), SORT_DESC, $user);
        }elseif($tourny_type == 2){
            array_multisort(array_column($user, 'ranking_count'), SORT_DESC, $user);
        }elseif($tourny_type == 3){
            array_multisort(array_column($user, 'fish_length'), SORT_ASC, $user);
        }

        return $user;
    }

    public static function addVote($data)
    {
        $data = array(
            'fish_id' => $data['fish_id'],
            'user_id' => $data['user_id'], 
            'tournament_id' => $data['tourny_id'], 
            'voter_user_id' => $data['voter_user_id'], 
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),   
           );
           return DB::table('voting')->insert($data);
    }

    public static function existsVoting($fish_id, $user_id, $tourny_id)
    {
        $where = [
            'fish_id'         => $fish_id,
            'user_id'         => $user_id,
            'tournament_id'   => $tourny_id,
        ];
        return DB::table('voting')->where($where)->first();
    }

    public static function getJoinedTournamentByUserId($userId)
    {
        $tournamentId = self::select()->where(['user_id'=>$userId])->orderby('jointourny_id', 'DESC')->get()->toArray();
        $hostournycount = count($tournamentId);
        if($hostournycount > 0)
        {
            foreach($tournamentId as $tournyId){
                $joinedTournamnet[] = Tournament::getTournamentById($tournyId['tournament_id']);
            }
        }else{
            $joinedTournamnet = [];
        }        
        return $joinedTournamnet;
    }

    public static function getWatchedTournamentByUserId($userId)
    {
        $tournamentId = DB::table('watch_users')->select()->where(['user_id'=>$userId])->orderby('watch_users_id', 'DESC')->get()->toArray();
        $watchcount = count($tournamentId);
        if($watchcount > 0)
        {
            foreach($tournamentId as $tournyId){
                $watchedTournamnet[] = Tournament::getTournamentById($tournyId->tournament_id);
            }
        }else{
            $watchedTournamnet = [];
        } 
        
        return $watchedTournamnet;
    }

    public static function attendeeList($tournamentId, $tourny_type)
    {
        $contestants = self::select('jointournament.*','wof_users.fullname')
                ->where('jointournament.tournament_id', $tournamentId)
                ->join('wof_users','wof_users.user_id','=','jointournament.user_id')
                ->get()->toArray();
        $user = array();
        $i=0;
        foreach($contestants as $attendee)
        {
            $user[$i]['jointourny_id'] = (string)$attendee['jointourny_id'];
            $user[$i]['tournament_id'] = $attendee['tournament_id'];
            $user[$i]['user_id'] = $attendee['user_id'];
            $host_name = Tournament::getTournamentById($attendee['tournament_id'])['user_id'];
            $user[$i]['tourny_host_name'] = WOFAuth::fetchUserName($host_name);
            $user[$i]['tourny_name'] = Tournament::TournamentById($attendee['tournament_id'])['tourny_name'];
            $user[$i]['user_image'] = WOFAuth::fetchUserImage($attendee['user_id']);
            $user[$i]['submitted_by'] = WOFAuth::fetchUserName($attendee['user_id']);
            $user[$i]['fish_length'] = Tournament::getFishLength($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['fish_weight'] = Tournament::getFishWeight($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['is_fish_submitted'] = Tournament::getIsFishSubmitted($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['fish_id'] = (string)Tournament::getFishByID($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['ranking_count'] = (string)Tournament::getVotingCounts($user[$i]['fish_id'],$attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['fish_images'] = Tournament::getFishImages($attendee['user_id'],$attendee['tournament_id']);
            $user[$i]['tourny_location'] = Tournament::getTournamentById($attendee['tournament_id'])['tourny_location'];
            $user[$i]['contestant_name'] = WOFAuth::fetchUserName($attendee['user_id']);
            $user[$i]['is_voted'] = Tournament::isVoted($attendee['user_id'],$attendee['tournament_id']);
            $spices_name = Tournament::getTournamentById($attendee['tournament_id'])['tourny_fish_species'];
            $user[$i]['fish_name'] = Tournament::getSpiceName($spices_name);
            $user[$i]['admin_commission'] = (string)DB::table('tournament_commission')->where(['commission_id' => 1])->value('commission');   
            $i++;
        }
        if($tourny_type == 1)
        {
            array_multisort(array_column($user, 'fish_weight'), SORT_DESC, $user);
        }elseif($tourny_type == 2){
            array_multisort(array_column($user, 'ranking_count'), SORT_DESC, $user);
        }elseif($tourny_type == 3){

            echo "t3";exit;
            array_multisort(array_column($user, 'fish_length'), SORT_ASC, $user);
        }

        return $user;
    }
}