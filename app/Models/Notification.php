<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = "send_notifications";

    protected $primaryKey = 'notification_id';

    protected $fillable = [
        'notification_title', 'description',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function getNotification()
    {
        $query = self::select('send_notifications.notification_id', 'notification_title', 'description');
        return $query->get();
    }

    public static function getNotificationById($notification_id)
    {
        $query = self::select(['send_notifications.notification_id', 'send_notifications.notification_title','send_notifications.description'])
                    ->where('send_notifications.notification_id', $notification_id);
        return $query->first();
    }

    public static function createNotification($data)
    {
        return self::create([
            'notification_title' => $data['notification_title'],
            'description' => $data['description'], 
            'created_at' => date('Y-m-d h:i:s'),
        ]);
    }

    public static function updateNotification($data)
    {
        $isUpdated = self::where('notification_id', $data["id"])->update([
            'notification_title' => $data['notification_title'],
            'description' => $data['description'],
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        
        if($isUpdated){
            return $data['id'];
        }else{
            return null;
        }
    }

    public static function removeNotification($removeNotification)
    {
        return self::where('notification_id', $removeNotification)->delete();
    }
}
