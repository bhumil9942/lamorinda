<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'schools';

    protected $primaryKey = 'school_id';

    protected $fillable = [
        'school_code', 'school_name', 'school_image', 'school_city_id', 'school_year', 
        'route_type','route_id','alternate_route_id','created_at', 'updated_at',
    ];

    public static function getSchools()
    {
        $query = self::select();
        return $query->get();
    }

    public static function createSchool($data)
    {
        return self::create([
            'school_code'       => $data['school_code'],
            'school_name'       => $data['school_name'],
            'school_image'      => $data['schoolimage_name'],
            'school_city_id'    => $data['school_city_id'],
            'school_year'       => $data['school_year'],
            'route_type'        => $data['route_type'],
            'route_id'          => json_encode($data['route_id'],TRUE),
            'alternate_route_id'=> json_encode($data['alternate_route_id'],TRUE),
        ]);
    }

    public static function getSchoolById($school_id)
    {
        $query = self::select()->where('schools.school_id', $school_id);
        return $query->first();
    }

    public static function updateSchool($data)
    {
        if (array_key_exists("awardimage_name",$data)){
            $schoolimage_name = $data['schoolimage_name'];
        }else{
            $schoolimage_name = $data['school_old_image'];
        }
        $isUpdated = self::where('school_id', $data['school_id'])->update([
            'school_code'       => $data['school_code'],
            'school_name'       => $data['school_name'],
            'school_image'      => $schoolimage_name,
            'school_city_id'    => $data['school_city_id'],
            'school_year'       => $data['school_year'],
            'route_type'        => $data['route_type'],
            'route_id'          => json_encode($data['route_id'],TRUE),
            'alternate_route_id'=> json_encode($data['alternate_route_id'],TRUE),
            'updated_at'        => date('Y-m-d h:i:s'),
        ]);
        
        if($isUpdated){
            return $data['school_id'];
        }else{
            return null;
        }
    }

    public static function removeSchool($schoolId)
    {
        return self::where('school_id', $schoolId)->delete();
    }
}
