<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $primaryKey = 'id';

    protected $fillable = [
        'city_name', 'status', 'created_at', 'updated_at',
    ];

    public static function getCities()
    {
        $query = self::select();
        return $query->get();
    }

    public static function createCity($data)
    {
        return self::create([
            'city_name' => $data['city_name'],
            'city_status' => $data['status'],
        ]);
    }

    public static function getCityById($cityId)
    {
        $query = self::select()->where('cities.id', $cityId);
        return $query->first();
    }

    public static function updateCity($data)
    {
        $isUpdated = self::where('id', $data['id'])->update([
            'city_name' => $data['city_name'],
            'city_status' => $data['status'],
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        
        if($isUpdated){
            return $data['id'];
        }else{
            return null;
        }
    }

    public static function removeCity($cityId)
    {
        return self::where('id', $cityId)->delete();
    }
}
