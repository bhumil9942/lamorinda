<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spice extends Model
{
    protected $fillable = [
        'spice_name', 'active_status',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function getSpices()
    {
        $query = self::select('spices.id as spice_id', 'spice_name', 'active_status');
        return $query->get();
    }

    public static function Spices()
    {
        $query = self::select('spices.id', 'spice_name')->where('active_status', 1);
        return $query->get();
    }

    public static function getSpiceById($spiceId)
    {
        $query = self::select(['spices.id','spices.spice_name', 'spices.active_status'])->where('spices.id', $spiceId);
        return $query->first();
    }

    public static function createSpice($data)
    {
        return self::create([
            'spice_name' => $data['spice_name'],
            'active_status' => $data['spice_status'],
            'created_at' => date('Y-m-d h:i:s'),
        ]);
    }

    public static function updateSpice($data)
    {
        $isUpdated = self::where('id', $data['id'])->update([
            'spice_name' => $data['spice_name'],
            'active_status' => $data['spice_status'],
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        
        if($isUpdated){
            return $data['id'];
        }else{
            return null;
        }
    }

    public static function removeSpice($spiceId)
    {
        return self::where('id', $spiceId)->delete();
    }
}
