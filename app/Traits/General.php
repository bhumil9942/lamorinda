<?php
namespace App\Traits;

use Carbon\Carbon;
use App\Models\State;
use DB, File, Mail, Log;
use Illuminate\Support\Facades\Storage;

trait General
{
    public function log($message, $data)
    {
        Log::info($message." ".$data);
        Log::info("****************************************************************************************************");
    }
    public function generateRandomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function print($array=null)
    {
        echo "<pre>";print_r($array);die;
    }
    public function execSP($SP, $params)
    {
        $data = json_decode(json_encode(DB::select('CALL '.$SP, $params)), true);
        return $data;
    }
    public function sendMail($emailFile, $data, $email, $subject)
    {
        $data['mail_to'] = $email;
        $data['subject'] = $subject;
        Mail::send('emails.'.$emailFile, $data, function($mail) use ($data) {
            $mail->to($data['mail_to'])
                    ->subject($data['subject']);
            $mail->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
        });
    }

    public function passwordPolicyTest($string)
    {
        if(
            preg_match('/(?=[A-Z][^A-Z]*)/', $string) && // Atleast 2 uppercase
            preg_match('/(?=[a-z][^a-z]*)/', $string) && // Atleast 1 lowercase
            preg_match('/(?=[0-9][^0-9]*)/', $string) && // Atleast 1 digit
            preg_match('/(?=[\x20-\x2F\x3A-\x40\x5B-\x60\x7B-\x7E\x80-\xFF][^\x20-\x2F\x3A-\x40\x5B-\x60\x7B-\x7E\x80-\xFF]*)/', $string) // Atleast 2 special characters
        )
            return TRUE;
        else
            return FALSE;
    }

    public function uploadFile($file, $dir_path, $withDate = '')
    {
        $imagename = $this->generateRandomString().'.'.$file->getClientOriginalExtension();
        Storage::disk('filestore')->putFileAs($dir_path, $file, $imagename);
        return empty($withDate) ? ['name' => $imagename, 'date' => date('Y-m-d')] : $imagename;
    }

    public function generateIdForStakeholders($prefix, $stateCode)
    {
        $state = State::where('code', $stateCode)->value('short_name');
        return $prefix.'-'.$state.'-'.date('Ymd').'-'.rand(10,100);
    }

    public function parseDateResponse($date)
    {
        return !empty($date) ? date('d-m-Y', strtotime($date)) : NULL;
    }

    public function parseDateRequest($date)
    {
        return !empty($date) ? date('Y-m-d', strtotime($date)) : NULL;
    }

    public function previewDocs($doc)
    {
        $doc = base64_decode($doc);
        return Storage::disk('filestore')->response($doc);
    }

    public function downloadDocs($doc)
    {
        $doc = base64_decode($doc);
        return Storage::disk('filestore')->download($doc);
    }
}
