@extends('layouts.mnre')
@section('title', 'Dashboard')
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua small-box-primary">
            <div class="inner">
                <h3>{{$routes}}</h3>
                <h4>Number Of Routes</h4>
               
            </div>
            <div class="icon">
            <!-- <i class="bx:bxs-bus-school"></i>  -->
            <span class="iconify" data-icon="bx:bxs-bus-school" style="color: #f6b51f;"></span>
            </div>
            <a href="{{URL::to($prefix.'/route')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow small-box-success">
            <div class="inner">
                <h3>{{$cities}}</h3>
                <h4>Number Of Cities</h4>
            </div>
            <div class="icon">
                <!-- <i class="ion ion-person-add"></i>  -->
                <span class="iconify" data-icon="emojione-v1:cityscape" style="color: white;"></span>
            </div>
            <a href="{{URL::to($prefix.'/city')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red small-box-danger">
            <div class="inner">
                <h3>{{$schools}}</h3>

                <h4>Total Schools</h4>
            </div>
            <div class="icon">
            <span class="iconify" data-icon="fa-solid:school"style="color: black;"></span>
                <!-- {{-- <i class="ion ion-pie-graph"></i> --}} -->
            </div>
            <a href="{{URL::to($prefix.'/school')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>
<!-- /.row -->
@endsection
<script src="https://code.iconify.design/2/2.1.2/iconify.min.js"></script>