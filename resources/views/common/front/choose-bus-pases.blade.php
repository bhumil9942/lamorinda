<!DOCTYPE html>
<html lang="en">

<body>
    @include('partials.header')
    
    <!-- Hero Section Start-->
    <section class="hero hero--small" style="background-image: url('{{ asset('images/choose-bus-pases.png')}}');">
        <h1 class="hero__heading">Choose Bus Pases</h1>
    </section>
    <!-- Hero Section End -->
    

<!-- Choose Bus Pases Section Start-->
    <section class="choose-bus-pases">
        <div class="container">
            <div class="choose-bus-pases__block">
                <h2>Choose Bus Pases</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text.</p>
            </div>
            <div class="row">
                @foreach ($pass_type as $pass_type)
                    <div class="col-lg-6">
                        <div class="row no-gutters">
                            <div class="col-lg-8">
                                <div class="choose-bus-pases__column blue-background">
                                    <div class="choose-bus-pases__title ">
                                        <i class="fas fa-bus-alt"></i>
                                        <h4> Bus Pass</h4>
                                        <i class="fas fa-bus-alt"></i>
                                    </div>
                                    <div class="choose-bus-pases__contant ">
                                        <a href={{ url('/bus-pass-detail-step1/'.$pass_type->pass_id.'/') }}>
                                            <h4> {{ $pass_type->pass_name .' : '. $pass_type->pass_monthly }} </h4>
                                        </a>
                                    </div>
                                    <div class="choose-bus-pases__blank ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            <div class="choose-bus-pases__column yellow-background">
                                <div class="choose-bus-pases__blank">
                                </div>
                                <div class="choose-bus-pases__image ">
                                    <img src={{ URL::asset("images/barcode.png")}}>
                                </div>
                                <div class="choose-bus-pases__blank ">
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
<!-- Choose Bus Pases Section End-->

@include('partials.footer')


</body>

</html>