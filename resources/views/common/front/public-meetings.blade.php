<!DOCTYPE html>
<html lang="en">
<body>
@include('partials.header')

  <!-- Hero Section Start-->
  <section class="hero hero--small" style="background-image: url(images/meetings.png);">
    <h1 class="hero__heading"> Public Meetings </h1>
  </section>
  <!-- Hero Section End -->

 <!-- safty information section -->
<div class="meeting-block" style="background-image: url(images/meeting-bg-image.png);">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6">
        <div class="meeting-block__information">
          <div class="heading-patten">
            <h1>Public Meetings</h1>
            <h2>Choose your bus pass option</h2>
          </div>
          <p>The Lamorinda School Bus Transportation Agency meets as 
            needed at the Lafayette City Offices, 3675 Mt. Diablo Blvd., Lafayette, Ca. 
            Agendas and packets for regular meetings are posted here 72 hours in 
            advance of meetings. Questions? </p>
            
           <p>Please call Whitney Simon, Program Manager, at 925-299-3216. </p>
            <h3 class="meeting-block__link"><a href="#"><span><i class="fas fa-file-pdf"></i></span>LSBTA Agenda and Packet</a></h3>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="meeting__image">
          <img src="images/travel-bus.png" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</div>


<!-- End safty information section -->

<div class="full-width-block">
  <div class="container">
        <h3 class="full-width-block__link"><a href="#">LSBTA Agenda and Packet</a></h3>
  </div>
</div>

@include('partials.footer')

</body>

</html>