<!DOCTYPE html>
<html lang="en">
<body>

 @include('partials.header')

  <!-- Hero Section Start-->
  <section class="hero" style="background-image: url(images/hero.png);">
  </section>
  <!-- Hero Section End -->

  <!-- Three-block-image Section Start-->
  <section class="three-block-image">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <a href="/select-your-school">
            <div class="three-block-image__column hover">
              <img src="images/image1.png" class="img-fluid">
              <div class="three-block-image__button">
                <div class="btn-white">Orinda Schools</div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4">
          <a href="/select-your-school">
            <div class="three-block-image__column hover">
              <img src="images/image2.png" class="img-fluid">
              <div class="three-block-image__button">
                <div class="btn-white">Moraga Schools</div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4">
          <a href="/select-your-school">
            <div class="three-block-image__column hover">
              <img src="images/image3.png" class="img-fluid">
              <div class="three-block-image__button">
                <div class="btn-white">Lafayette School</div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  <!-- Three-block-image Section End -->

  <!-- About-us Section Start-->
  <section class="about-us" style="background-image: url(images/about-us-bg.png);">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 col-md-12">
          <div class="about-us__column">
            <div class="about-us__image">
              <img src="images/bus1.png" class="img-fluid">
            </div>
            <div class="about-us__image1">
              <img src="images/bus.png" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-12">
          <div class="about-us__column">
            <div class="heading-patten">
              <h1>About us</h1>
              <h2>Welcome to Lamorinda School Bus</h2>
            </div>
            <div class="about-us__contant">
              <p>Cupidatat non proident sunt culpa qui officia deserunt
                mollit anim
                idest laborum sed ut perspiciatis unde omnis
                iste natuserror sit
                voluptatem accusantium doloremque
                laudantium, totam rem
                aperiam eaque ipsa quae ab illo
                inventore veritatis et quas.</p><br>

              <p>In publishing and graphic design, Lorem ipsum is a
                Placeholder
                text commonly used to demonstrate the
                Visual form of a
                document or a typeface without relying
                On meaningful content.
                Lorem ipsum may be used as a
                Placeholder before final copy. </p>
            </div>
            <a href="#" class="btn-1">Contact us</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- About-us Section End -->

  <section class="slider" style="background-image: url(images/slider-bg-image.jpg)" ;>
    <div class="slider__block">

      <div class="slider__column">
        <div class="slider__contant" style="background-image: url(images/slider-image1.png);">
          <div class="slider__slide-number">
            <span>01</span>
          </div>
          <div class="slider__heading">
            <h4>Purchase Bus Passes</h4>
          </div>
          <div class="slider__button">
            <a href="#" class="btn-1">View More</a>
          </div>


        </div>
      </div>

      <div class="slider__column">
        <div class="slider__contant" style="background-image: url(images/slider-image2.png);">
          <div class="slider__slide-number">
            <span>02</span>
          </div>
          <div class="slider__heading">
            <h4>Schedule & Maps</h4>
          </div>
          <div class="slider__button">
            <a href="/schedules-maps" class="btn-1">View More</a>
          </div>

        </div>
      </div>

      <div class="slider__column">
        <div class="slider__contant" style="background-image: url(images/slider-image3.png);">
          <div class="slider__slide-number">
            <span>03</span>
          </div>
          <div class="slider__heading">
            <h4>Safty Info</h4>
          </div>
          <div class="slider__button">
            <a href="/safety-information" class="btn-1">View More</a>
          </div>
        </div>
      </div>


      <div class="slider__column">
        <div class="slider__contant" style="background-image: url(images/slider-image4.png);">
          <div class="slider__slide-number">
            <span>04</span>
          </div>
          <div class="slider__heading">
            <h4>Online Pass</h4>
          </div>
          <div class="slider__button">
            <a href="#" class="btn-1">View More</a>
          </div>
        </div>
      </div>

      <div class="slider__column">
        <div class="slider__contant" style="background-image: url(images/slider-image1.png);">
          <div class="slider__slide-number">
            <span>05</span>
          </div>
          <div class="slider__heading">
            <h4>Purchase Bus Passes</h4>
          </div>
          <div class="slider__button">
            <a href="#" class="btn-1">View More</a>
          </div>
        </div>
      </div>

      <div class="slider__column">
        <div class="slider__contant" style="background-image: url(images/slider-image2.png);">
          <div class="slider__slide-number">
            <span>06</span>
          </div>
          <div class="slider__heading">
            <h4>Schedule & Maps</h4>
          </div>
          <div class="slider__button">
            <a href="/schedules-maps" class="btn-1">View More</a>
          </div>
        </div>
      </div>

    </div>
  </section>

  @include('partials.footer')


</body>

</html>