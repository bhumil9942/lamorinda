<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="{{ URL::asset('front-font/fonts.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/slick.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/slick-theme.css') }}">
    <title>Lamorinda - Safety Information </title>
</head>

<body>
    
    @include('partials.header')

    <!-- Hero Section Start-->
    <section class="hero hero--small" style="background-image: url(images/choose-bus-pases.png);">
        <h1 class="hero__heading">Choose Bus Pases</h1>
    </section>
    <!-- Hero Section End -->

    <!-- Form Step Start -->
    <section class="form-step">
        <div class="container">
            <p>ALL IVY SCHOOL, ALL THE TIME</p>
            <ul class="form-step__bar">
                <li>Trip Detail<i class="fas fa-caret-right"></i></li>
                <li class="active">Student Detail<i class="fas fa-caret-right"></i></li>
                <li>Booking<i class="fas fa-caret-right"></i></li>
                <li>Confirmation<i class="fas fa-caret-right"></i></li>
            </ul>
        </div>
    </section>
    <!-- Form Step End -->

    <!-- <Trip Detail Start> -->
    <section class="trip-detail">
        <div class="container">
            <div class="trip-detail__from">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="trip-detail__title ">
                            <i class="fas fa-bus-alt"></i>
                            <h4> Bus Pass Trip Detail</h4>
                            <i class="fas fa-bus-alt"></i>
                        </div>
                    </div>
                </div>
                <div class="row light-blue-background">
                    <div class="col-lg-8">
                        <div class="trip-detail__column">
                            <label for="city">City:-</label>
                            <br>
                            <input type="text" name="city" id="city" placeholder="Orinda" />
                            <label for="school-name">School Name :-</label>
                            <br>
                            <input type="text" name="school-name" id="school-name"
                                placeholder="Orinda Intermediate School" />
                            <label for="pass-type">Pass Type :- </label>
                            <br>
                            <input type="text" name="pass-type" id="pass-type" placeholder="Round Trip" required />
                            <label for="bus-stop">Bus Stop :-</label>
                            <br>
                            <input type="text" name="bus-stop" id="bus-stop" placeholder="57 Van Ripper">
                            <label for="bus-root">Root :-</label>
                            <br>
                            <input type="text" name="bus-root" id="bus-root" placeholder="OIS Route">
                            <label for="student-name">Student Name*</label>
                            <br>
                            <input type="text" name="student-name" id="student-name" required>
                            <label for="student-grade">Grade*</label>
                            <br>
                            <select id="student-grade" name="student-grade" required>
                                <option value="Select Grade"></option>
                                <option value="grade">1</option>
                                <option value="grade">2</option>
                                <option value="grade">3</option>
                                <option value="grade">4</option>
                                <option value="grade">5</option>
                            </select>
                            <label for="city">City*</label>
                            <br>
                            <select id="city" name="city" required>
                                <option value="city"></option>
                                <option value="city">Chicago</option>
                                <option value="city">Houston</option>
                                <option value="city">Los Angeles</option>
                            </select>
                            <label for="state">State*</label>
                            <br>
                            <select id="state" name="state" required>
                                <option value="state"></option>
                                <option value="state">Alabama</option>
                                <option value="state">California</option>
                                <option value="state">New Jersey</option>
                                <option value="state">New York</option>
                            </select>
                            <label for="parent-mobile-number">Parent Mobile Number*</label>
                            <br>
                            <input type="text" name="parent-mobile-number" id="parent-mobile-number" required>
                            <label for="confirm e-mail">Confirm E-Mail*</label>
                            <br>
                            <input type="text" name="confirm e-mail" id="confirm e-mail" required>

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="trip-detail__column">
                            <div class="trip-detail__image ">
                                <img src="images/barcode.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="trip-detail__blank ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <Trip Detail End> -->


        <div class="center-btn">
            <input type="submit" class="btn-1" value="Next">
        </div>
         <!-- <button class="loginButton">Next</button> -->
        <div id="student-pass-popup">
            <div class="closeButton">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <div class="pass-popup">
                <h4>Do You Want To Another Pass</h4>
                <div class="pass-popup__image">
                    <img src="images/pass-img.png" class="img-fluid">
                </div>
                <div class="pass-popup__button">
                    <div class="register-btn">
                        <a href="#" class="btn" id="register-btn">No Thanks</a>
                    </div>
                    <div class="login-btn">
                        <input type="submit" class="btn-1" value="Yes">
                    </div>
                </div>
            </div>
        </div>

        @include('partials.footer')

        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script src="js/slick.min.js"></script>

</body>

</html>