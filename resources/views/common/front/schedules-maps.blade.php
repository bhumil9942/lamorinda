<!DOCTYPE html>
<html lang="en">

<body>
@include('partials.header')

  <!-- Hero Section Start-->
  <section class="hero hero--small" style="background-image: url(images/maps.jpg);">
    <h1 class="hero__heading"> Schedules & Maps </h1>
  </section>
  <!-- Hero Section End -->


  <!-- TAB SECTION -->

  <section class="schedule-tabs">
    <div class="container">
      <div class="map-heading-block map-heading-block--top-radius">
        <h2 class="map-heading-block__title">Schedules & Maps</h2>
      </div>
      <div class="accordian-tabs">
        <div class="row">
          <div class="col-lg-3">
            <div class="accordian-tabs__left">
              <ul class="nav accordian-tabs__block">
                <li><a href="#tab1" data-toggle="tab" class="active">ORINDA SCHOOLS</a></li>
                <li><a href="#tab2" data-toggle="tab">MORAGA SCHOOLS</a></li>
                <li><a href="#tab3" data-toggle="tab">LAFAYETTE SCHOOLS</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-9">
            <!-- Tab panes -->

            <div class="tab-content accordian-tabs__right">
              <div class="tab-pane active" id="tab1">

                <div id="accordion" class="accordion">
                  <!-- <div class="accordian__card">
                                <div class="accordion__card-header" id="headingOne">
                                  <h5 class="mb-0">
                                    <div class="accordion__link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">     
                                      <i class="fa" aria-hidden="true"></i>
                                      Collapsible Group Item #1
                                    </div>
                                  </h5>
                                </div>
                          
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                  <div class="accordion__card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                  </div>
                                </div>
                              </div> -->


                  <ul class="faq-list accordion__list">
                    <li data-aos="fade-up" data-aos-delay="100"
                      class="accordion__Block yelllow-broder aos-init aos-animate">
                      <div data-toggle="collapse" class="accordion__link collapsed" data-target="#faq1"
                        aria-expanded="false">
                        <h2 class="blue-color">Glorietta Elementary Schedules & Maps<span><i class="fa"></i></span></h2>
                      </div>
                      <div id="faq1" class="accordion__desc collapse" data-parent=".faq-list" style="">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="200"
                      class="accordion__Block blue-broder aos-init aos-animate">
                      <div data-toggle="collapse" data-target="#faq2" class="accordion__link collapsed">
                        <h2 class="yellow-color">Sleepy Hollow Schedules & Maps<span><i class="fa"></i></span>
                        </h2>
                      </div>
                      <div id="faq2" class="accordion__desc collapse" data-parent=".faq-list">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="300"
                      class="accordion__Block yelllow-broder aos-init aos-animate">
                      <div data-toggle="collapse" data-target="#faq3" class="accordion__link collapsed">
                        <h2 class="blue-color">Wagner Ranch Schedules & Maps<span><i class="fa"></i></span></h2>
                      </div>
                      <div id="faq3" class="accordion__desc collapse" data-parent=".faq-list">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="100"
                      class="accordion__Block blue-broder aos-init aos-animate">
                      <div data-toggle="collapse" class="accordion__link collapsed" data-target="#faq4"
                        aria-expanded="false">
                        <h2 class="yellow-color">Miramonte High School Schedules & Maps<span><i class="fa"></i></span>
                        </h2>
                      </div>
                      <div id="faq4" class="accordion__desc collapse" data-parent=".faq-list" style="">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="100"
                      class="accordion__Block yelllow-broder aos-init aos-animate">
                      <div data-toggle="collapse" class="accordion__link collapsed" data-target="#faq5"
                        aria-expanded="false">
                        <h2 class="blue-color">Orinda Intermediate School Schedules & Maps<span><i class="fa"></i></span></h2>
                      </div>
                      <div id="faq5" class="accordion__desc collapse" data-parent=".faq-list" style="">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>

                  </ul>

                </div>

              </div>
              <div class="tab-pane" id="tab2">
                 <ul class="faq-list accordion__list">
                    <li data-aos="fade-up" data-aos-delay="100"
                      class="accordion__Block yelllow-broder aos-init aos-animate">
                      <div data-toggle="collapse" class="accordion__link collapsed" data-target="#faq6"
                        aria-expanded="false">
                        <h2 class="blue-color">Glorietta Elementary Schedules & Maps<span><i class="fa"></i></span></h2>
                      </div>
                      <div id="faq6" class="accordion__desc collapse" data-parent=".faq-list" style="">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="200"
                      class="accordion__Block blue-broder aos-init aos-animate">
                      <div data-toggle="collapse" data-target="#faq7" class="accordion__link collapsed">
                        <h2 class="yellow-color">Glorietta Elementary Schedules & Maps<span><i class="fa"></i></span>
                        </h2>
                      </div>
                      <div id="faq7" class="accordion__desc collapse" data-parent=".faq-list">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="300"
                      class="accordion__Block yelllow-broder aos-init aos-animate">
                      <div data-toggle="collapse" data-target="#faq8" class="accordion__link collapsed">
                        <h2 class="blue-color">Glorietta Elementary Schedules & Maps<span><i class="fa"></i></span></h2>
                      </div>
                      <div id="faq8" class="accordion__desc collapse" data-parent=".faq-list">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>


                  </ul>
              </div>
              <div class="tab-pane" id="tab3">
                 <ul class="faq-list accordion__list">
                    <li data-aos="fade-up" data-aos-delay="100"
                      class="accordion__Block yelllow-broder aos-init aos-animate">
                      <div data-toggle="collapse" class="accordion__link collapsed" data-target="#faq9"
                        aria-expanded="false">
                        <h2 class="blue-color">Glorietta Elementary Schedules & Maps<span><i class="fa"></i></span></h2>
                      </div>
                      <div id="faq9" class="accordion__desc collapse" data-parent=".faq-list" style="">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="200"
                      class="accordion__Block blue-broder aos-init aos-animate">
                      <div data-toggle="collapse" data-target="#faq10" class="accordion__link collapsed">
                        <h2 class="yellow-color">Glorietta Elementary Schedules & Maps<span><i class="fa"></i></span>
                        </h2>
                      </div>
                      <div id="faq10" class="accordion__desc collapse" data-parent=".faq-list">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="300"
                      class="accordion__Block yelllow-broder aos-init aos-animate">
                      <div data-toggle="collapse" data-target="#faq11" class="accordion__link collapsed">
                        <h2 class="blue-color">Glorietta Elementary Schedules & Maps<span><i class="fa"></i></span></h2>
                      </div>
                      <div id="faq11" class="accordion__desc collapse" data-parent=".faq-list">
                        <p>GLORIETTA SCHEDULE - VIEW PDF </p>
                        <p>GLOR 15 AM - MAP </p>
                        <p>GLOR 15 PM - MAP</p>
                      </div>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="map-heading-block map-heading-block--bottom-radius">
        <h2 class="map-heading-block__title">Thank You</h2>
      </div>
    </div>
  </section>

  <!-- END TAB SECTION -->

  @include('partials.footer')

</body>

</html>