
@include('partials.header')

<!-- Hero Section Start-->
<section class="hero hero--small" style="background-image: url(images/select-city.png);">
    <h1 class="hero__heading"> Select Your City </h1>
</section>
<!-- Hero Section End -->

<!-- Select Your City Section Start-->
<section class="select-your-city">
    <div class="container">
      <h2 class="yellow-color">WHAT WE OFFER</h2>
      <h2 class="blue-color">Select Your City</h2>
      <div class="row">
      @foreach ($city as $citi)
          <div class="col-lg-6">
            if(auth->user)
            <a href={{ url('select-your-school/'.$citi->city_id.'/') }}>
                <div class="select-your-city__column hover">
                  <img src="images/select-school-image.png" class="img-fluid">
                  <div class="select-your-city__button">
                      <div class="btn-white">{{ $citi->city_name }}</div>                
                  </div>
                </div>
            </a>
            else

            <a href="javascript:;" class="">
                <div class="select-your-city__column hover">
                  <img src="images/select-school-image.png" class="img-fluid">
                  <div class="select-your-city__button">
                      <div class="btn-white">{{ $citi->city_name }}</div>                
                  </div>
                </div>
            </a>

          </div>
          @endforeach
          
      </div>
    </div>
</section>
<!-- Select Your City Section End -->

@include('partials.footer')
