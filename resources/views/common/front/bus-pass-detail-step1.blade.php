<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="{{ URL::asset('front-font/fonts.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/slick.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/slick-theme.css') }}">
    <title>Lamorinda - Safety Information </title>
</head>

<body>
@include('partials.header')

<!-- Hero Section Start-->
<section class="hero hero--small" style="background-image:url('{{ asset('images/choose-bus-pases.png')}}');">

    <h1 class="hero__heading">Choose Bus Pases</h1>
</section>
<!-- Hero Section End -->

<!-- Form Step Start -->
<section class="form-step">
    <div class="container">
        <p>ALL IVY SCHOOL, ALL THE TIME</p>
        <ul class="form-step__bar">
            <li class="active">Trip Detail<i class="fas fa-caret-right"></i></li>         
            <li>Student Detail<i class="fas fa-caret-right"></i></li>
            <li>Booking<i class="fas fa-caret-right"></i></li>
            <li>Confirmation<i class="fas fa-caret-right"></i></li>
      </ul>
    </div>
</section>
<!-- Form Step End -->

<!-- <Trip Detail Start> -->
    <section class="trip-detail">
        <div class="container">
            <div class="trip-detail__from">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="trip-detail__title ">
                            <i class="fas fa-bus-alt"></i>
                            <h4> Bus Pass Trip Detail</h4>
                            <i class="fas fa-bus-alt"></i>
                        </div>
                    </div>
                </div>
                <div class="row light-blue-background">
                    <div class="col-lg-8">
                        <div class="trip-detail__column">
                                <label for="city">City:-</label>
                                <br>
                                <input type="text" name="city" id="city" placeholder="Orinda" />
                                <label for="school-name">School Name :-</label>
                                <br>
                                <input type="text" name="school-name" id="school-name"
                                    placeholder="Orinda Intermediate School" />
                                <label for="pass-type">Pass Type :- </label>
                                <br>
                                <input type="text" name="pass-type" id="pass-type" placeholder="Round Trip" required />
                                <label for="bus-stop">Bus Stop*</label>
                                <br>
                                <select id="bus-stop" name="bus-stop">
                                    <option value="Orinda Schools">57 Van Ripper</option>
                                    <option value="Orinda Schools">57 Van Ripper</option>
                                    <option value="Orinda Schools">57 Van Ripper</option>
                                    <option value="Orinda Schools">57 Van Ripper</option>
                                    <option value="Orinda Schools">57 Van Ripper</option>
                                </select>
                                <label for="bus-root">Root*</label>
                                <br>
                                <select id="bus-root" name="bus-root">
                                    <option value="Orinda Schools">OIS Route</option>
                                    <option value="Orinda Schools">OIS Route</option>
                                    <option value="Orinda Schools">OIS Route</option>
                                    <option value="Orinda Schools">OIS Route</option>
                                    <option value="Orinda Schools">OIS Route</option>
                                </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="trip-detail__column">
                            <div class="trip-detail__image ">
                                <img src={{ URL::asset("images/barcode.png")}}>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="trip-detail__blank ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- <Trip Detail End> -->


<div class="center-btn">
    <input type="submit" class="btn-1" value="Next">
</div>


@include('partials.footer')

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/slick.min.js"></script>

</body>

</html>