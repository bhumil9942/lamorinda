<!DOCTYPE html>
<html lang="en">

<body>

    @include('partials.header')

    <!-- Hero Section Start-->
    <section class="hero hero--small" style="background-image: url('{{ asset('images/select-city.png')}}');">
        <h1 class="hero__heading"> Select Your School </h1>
    </section>
    <!-- Hero Section End -->

    <!-- Our category Section Start-->
    <section class="our-category">
        <div class="container">
            <div class="our-category__block">
                <h2>Our Category</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text.</p>
            </div>
            <div class="row">
                @foreach ($schools as $school)
                @if($loop->iteration % 2 != 0)
                    <div class="col-lg-4 col-md-6">
                        <div class="our-category__column blue-background">
                            <div class="our-category__image">
                                <img src="{{ URL::asset('/api/assets/user'). '/'.$school->school_image }}" height="250" width="350">
                            </div>
                            <div class="our-category__contant">
                            <a href={{ url('/choose-bus-pases/'.$school->school_city_id.'/') }}> <p>{{ $school->school_name }} </p>  </a>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="col-lg-4 col-md-6">
                        <div class="our-category__column yellow-background">
                            <div class="our-category__image">
                                <img src="{{ URL::asset('/api/assets/user'). '/'.$school->school_image }}" height="250" width="350">
                            </div>
                            <div class="our-category__contant">
                            <a href={{ url('/choose-bus-pases/'.$school->school_city_id.'/') }}> <p>{{ $school->school_name }} </p>  </a>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
    <!-- Our category Section End-->

    @include('partials.footer')

</body>

</html>