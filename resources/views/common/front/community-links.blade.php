<!DOCTYPE html>
<html lang="en">

<body>

@include('partials.header')

  <!-- Hero Section Start-->
  <section class="hero hero--small" style="background-image: url(images/Community-links.png);">
    <h1 class="hero__heading"> Community Links </h1>
  </section>
  <!-- Hero Section End -->

<!-- Community Links -->

<section class="community-section community-section--left">
  
  <div class="center-content">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-9">          
    <h2 class="center-content__title">
      Community Links
    </h2>
    <p>For 45 years we've continually ranked #1 for excellence. We would like to take 
      this opportunity to thank you for making this possible.</p>
        </div>
      </div>
    </div>
  </div> 

  <div class="container-fluid p-0">
    <div class="row align-items-center">
      <div class="col-lg-6 p-0">
        <div class="community-section__block">
          <div class="heading-patten">
            <h1>Public Meetings</h1>
          </div>
          <ul>
            <li>community-section</li>
            <li>City of Lafayette</li>
            <li>Contra Costa Transportation Authority</li>
            <li>511 Contra Costa</li>
          </ul>
        </div>
      </div>
      <div class="col-lg-6 p-0">
        <div class="community-section__image">
          <img src="images/community_linkss.png" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="community-section community-section--right">
  <div class="container-fluid p-0">
    <div class="row align-items-center">
      <div class="col-lg-6 p-0 community-section__desc">
        <div class="community-section__block">
          <div class="heading-patten">
            <h1>Public Meetings</h1>
          </div>
          <ul>
            <li>community-section</li>
            <li>City of Lafayette</li>
            <li>Contra Costa Transportation Authority</li>
            <li>511 Contra Costa</li>
          </ul>
        </div>
      </div>
      <div class="col-lg-6 p-0">
        <div class="community-section__image">
          <img src="images/community_linkss.png" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="community-section community-section--left">
  <div class="container-fluid p-0">
    <div class="row align-items-center">
      <div class="col-lg-6 p-0">
        <div class="community-section__block">
          <div class="heading-patten">
            <h1>Public Meetings</h1>
          </div>
          <ul>
            <li>community-section</li>
            <li>City of Lafayette</li>
            <li>Contra Costa Transportation Authority</li>
            <li>511 Contra Costa</li>
          </ul>
        </div>
      </div>
      <div class="col-lg-6 p-0">
        <div class="community-section__image">
          <img src="images/community_linkss.png" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="community-section community-section--right">
  <div class="container-fluid p-0">
    <div class="row align-items-center">
      <div class="col-lg-6 p-0 community-section__desc">
        <div class="community-section__block">
          <div class="heading-patten">
            <h1>Public Meetings</h1>
          </div>
          <ul>
            <li>community-section</li>
            <li>City of Lafayette</li>
            <li>Contra Costa Transportation Authority</li>
            <li>511 Contra Costa</li>
          </ul>
        </div>
      </div>
      <div class="col-lg-6 p-0">
        <div class="community-section__image">
          <img src="images/community_linkss.png" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Community Links -->

  @include('partials.footer')

</body>

</html>