<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="{{ URL::asset('front-font/fonts.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/slick.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/slick-theme.css') }}">
    <title>Lamorinda - Safety Information </title>
</head>

<body>
@include('partials.header')

<!-- Hero Section Start-->
<section class="hero hero--small" style="background-image: url(images/choose-bus-pases.png);">
    <h1 class="hero__heading">Choose Bus Pases</h1>
</section>
<!-- Hero Section End -->

<!-- Form Step Start -->
<section class="form-step">
    <div class="container">
        <p>ALL IVY SCHOOL, ALL THE TIME</p>
        <ul class="form-step__bar">
            <li>Trip Detail<i class="fas fa-caret-right"></i></li>         
            <li>Student Detail<i class="fas fa-caret-right"></i></li>
            <li class="active">Payment-info<i class="fas fa-caret-right"></i></li>
            <li>Confirmation<i class="fas fa-caret-right"></i></li>
      </ul>
    </div>
</section>
<!-- Form Step End -->

<!-- <Trip Detail Start> -->
    <section class="trip-detail">
        <div class="container">
            <div class="trip-detail__from">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="trip-detail__title ">
                            <i class="fas fa-bus-alt"></i>
                            <h4> Bus Pass Trip Detail</h4>
                            <i class="fas fa-bus-alt"></i>
                        </div>
                    </div>
                </div>
                <div class="row light-blue-background">
                    <div class="col-lg-8">
                        <div class="trip-detail__column">
                            <h4>Trip Detail</h4>
                                <label for="city">City:-</label>
                                <br>
                                <input type="text" name="city" id="city" placeholder="Orinda" />
                                <label for="school-name">School Name :-</label>
                                <br>
                                <input type="text" name="school-name" id="school-name"
                                    placeholder="Orinda Intermediate School" />
                                <label for="pass-type">Pass Type :- </label>
                                <br>
                                <input type="text" name="pass-type" id="pass-type" placeholder="Round Trip" required />
                                <label for="bus-stop">Bus Stop :-</label>
                                <br>
                                <input type="text" name="bus-stop" id="bus-stop" placeholder="57 Van Ripper">                            
                                <label for="bus-root">Root :-</label>
                                <br>
                                <input type="text" name="bus-root" id="bus-root" placeholder="OIS Route"> 
                                <h4>Student Detail</h4>
                                <label for="student-name">Student Name*</label>
                                <br>
                                <input type="text" name="student-name" id="student-name" placeholder="Robi Fauzi" required >
                                <label for="student-grade">Grade*</label>
                                <br>
                                <input type="student-grade" name="student-grade" id="student-grade" placeholder="3" required >
                                <!-- <select id="student-grade" name="student-grade" placeholder="OIS Route"required >
                                    <option value="Select Grade">3</option>
                                    <option value="grade">1</option>
                                    <option value="grade">2</option>
                                    <option value="grade">3</option>
                                    <option value="grade">4</option>
                                    <option value="grade">5</option>
                                </select> -->
                                <label for="state">State*</label>
                                <br>
                                <input type="state" name="state" id="state" placeholder="California" required >
                                <!-- <select id="state" name="state" required >
                                    <option value="state">California</option>
                                    <option value="state">Alabama</option>
                                    <option value="state">California</option>
                                    <option value="state">New Jersey</option>
                                    <option value="state">New York</option>
                                </select> -->
                                <label for="parent-mobile-number">Parent Mobile Number*</label>
                                <br>
                                <input type="text" name="parent-mobile-number" id="parent-mobile-number" placeholder="+123 456 9870" required>
                                <label for="confirm e-mail">Confirm E-Mail*</label>
                                <br>
                                <input type="text" name="confirm e-mail" id="confirm e-mail" placeholder="parent@gmail.com" required>
                                <label for="state">Payment Type*</label>
                                <br>
                                <select id="payment-type" name="payment-type" required >
                                    <option value="payment-type">Pay with Credit Card</option>
                                    <option value="payment-type">Pay with Credit Card</option>
                                    <option value="payment-type">Pay with Debit Card</option>
                                </select>

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="trip-detail__column">
                            <div class="trip-detail__image ">
                                <img src="images/barcode.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="trip-detail__blank ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- <Trip Detail End> -->


<div class="center-btn">
    <input type="submit" class="btn-1" value="Next">
</div>

@include('partials.footer')

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/slick.min.js"></script>

</body>

</html>