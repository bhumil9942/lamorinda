@extends('layouts.'.$layout)
@section('button')
@if (Auth::getDefaultDriver() == "mnre")
<!-- <a href="{{URL::to('/'.$prefix.'/view-school_bus_route')}}" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE BUS ROUTE
</a> -->
<div class="box-header with-border">
    <a href="{{URL::to($prefix.'/school')}}" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
</div>
@endif
@endsection
@section('content')
@section('title', 'Manage School Bus Routes')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                @include('elements.CommonElements._flash')
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>School Name</th>
                            <th>Route Type</th>
                            <th>Route</th>
                            <th>Alternate Route</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $schools['school_id'] }}</td>
                            <td>{{ $schools['school_name'] }}</td>
                            <td>
                                @if($schools['route_type'] == 1) 
                                    AM
                                @elseif($schools['route_type'] == 2)
                                    PM
                                @endif
                            </td>
                            <td>{{ $rName }}</td>
                            <td>{{ $aName }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


