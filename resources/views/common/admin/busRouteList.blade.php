@extends('layouts.'.$layout)
@section('button')
@if (Auth::getDefaultDriver() == "mnre")
<a href="{{URL::to('/'.$prefix.'/create-route/')}}" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE ROUTE
</a>
@endif
@endsection
@section('content')
@section('title', 'Manage Bus Routes')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                @include('elements.CommonElements._flash')
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Route ID</th>
                            <th>Route Name</th>
                            <th>Available Pass</th>
                            <th>Current Pass</th>
                            <th>Route Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($routes as $route)
                        <tr>
                            <td>{{ $route->bus_route_id }}</td>
                            <td>{{ $route->bus_route_name }}</td>
                            <td>{{ $route->available_pass }}</td>
                            <td>{{ $route->current_pass }}</td>
                            <td>
                                @if($route->bus_route_status == 1) 
                                    Active
                                @elseif($route->bus_route_status == 2)
                                    In-Active
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{url('/'.$prefix.'/edit-route/'.$route->bus_route_id)}}">Edit</a>
                                <a class="btn btn-xs btn-danger" href="{{url('/'.$prefix.'/delete-route/'.$route->bus_route_id)}}">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


