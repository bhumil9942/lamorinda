@extends('layouts.'.$layout)
@section('button')
@if (Auth::getDefaultDriver() == "mnre")
<a href="{{URL::to('/'.$prefix.'/create-pass_type/')}}" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE PASS
</a>
@endif
@endsection
@section('content')
@section('title', 'Manage Pass Types')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                @include('elements.CommonElements._flash')
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Pass ID</th>
                            <th>Pass Name</th>
                            <th>Pass Price</th>
                            <th>Pass Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pass_type as $type)
                        <tr>
                            <td>{{ $type->pass_id }}</td>
                            <td>{{ $type->pass_name }}</td>
                            <td>$ {{ $type->pass_monthly }}</td>
                            <td>
                                @if($type->pass_status == 1) 
                                    Active
                                @elseif($type->pass_status == 2)
                                    In-Active
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{url('/'.$prefix.'/edit-pass_type/'.$type->pass_id)}}">Edit</a>
                                <a class="btn btn-xs btn-danger" href="{{url('/'.$prefix.'/delete-pass_type/'.$type->pass_id)}}">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


