@extends('layouts.'.$layout)
@section('title', 'Change Password')
@section('content')

<div class="box box-primary">
	<form class="form-horizontal" id="changeform" action="{{url($prefix.'/changepassword')}}" method="post">
		{{csrf_field()}}
		<div class="box-body">
		<div class="col-md-12">
			<label>Current Password</label>
			<input type="password" class="form-control required passwordcheck" id="oldpass" name="oldpass" placeholder="Current Password" autocomplete="off">
		</div>
		<div class="col-md-12">
			<label>New Password</label>
			<input type="password" data-placement="bottom" data-toggle="popover" data-trigger="focus" data-html="true" data-content='<div id="errors"></div>' class="form-control required passwordStrength" id="newpass" name="newpass" placeholder="New Password" autocomplete="off">
		</div>
		<div class="col-md-12">
			<label>Confirm New Password</label>
			<input type="password" class="form-control required" id="cpass" name="cpass" placeholder="Confirm New Password" autocomplete="off">
		</div>
		<div class="clearfix"></div>
		<div class="box-footer">
		<a href="{{url($prefix.'/dashboard')}}" class="btn btn-sm btn-danger">{{__('Cancel')}}</a>
		<button type="submit" class="btn btn-sm btn-primary">Save</button>
		</div>
	</form>
</div>

@endsection
@section('scripts')
<script>
	$(function(){
		jQuery.validator.addMethod("passwordcheck", function() {
		   var isSuccess = false;
		   $.ajax({ url: '{{url("ajax")}}/passwordcheck',
					method: 'POST',
					data: { password: btoa($('#oldpass').val()), type: '{{$layout}}' },
		            async: false,
		            success:
		                function(resp) { isSuccess = resp.success == true ? true : false }
		          });
		    return isSuccess;
		},"Old password incorrect!");
		$('#changeform').validate();
		$("#cpass").rules('add',{
									equalTo: "#newpass",
									messages: {
										equalTo: "New password and confirm password field don't match."
									}
								}
						);
		$("#newpass").passwordValidation({}, function(element, valid, match, failedCases) {
        	let html = '<span>Password must contain at least:</span><p style="list-style:none;">';
			$.each(failedCases, (indx, node) => { html += '<span class="req"><i class="fa fa-caret-right" aria-hidden="true"></i>   ' + node + '</span><br>'; });
			html += '</p>';
			if(failedCases.length){
				passwordStrengthValidity = false;
				$("#errors").html(html);
			}
			else{
				$('[data-toggle="popover"]').popover('hide');
				passwordStrengthValidity = true;
			}
		});
		$.validator.addMethod("passwordStrength", function(value, element) {
			return passwordStrengthValidity;
		}, "The password isn't strong enough");
	});

</script>
@endsection
