@extends('layouts.'.$layout)
@section('content')
@section('title', 'Add Bus Route')
@php
    $editable = empty($editable) ? '' : $editable;
@endphp
<div class="row">
    <div class="col-md-12">
        @include('elements.CommonElements._flash')
        <div class="box box-primary">
            <form action="{{URL::to('/'.$prefix.'/create-route')}}" id="awardRequestForm" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-header with-border">
                    <a href="{{URL::to($prefix.'/route')}}" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bus_route_name">{{ __('Route Name') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="bus_route_name" {{$editable ?? ''}} value="{{$routes['bus_route_name'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="bus_route_status">{{ __('Route Status') }} <span class="error">*</span></label>
                                <select class="form-control required" name="bus_route_status" {{$editable ?? ''}}>
                                    <option value="" selected>Select Status</option>
                                    <option value="1" @if(($routes->bus_route_status ?? '') == 1) selected @endif>Active</option>
                                    <option value="2" @if(($routes->bus_route_status ?? '') == 2) selected @endif>In-Active</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="available_pass">{{ __('Available Pass') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="available_pass" {{$editable ?? ''}} value="{{$routes['available_pass'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="current_pass">{{ __('Current Pass') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="current_pass" {{$editable ?? ''}} value="{{$routes['current_pass'] ?? ''}}">
                            </div>
                        </div>
                    </div>
                </div>
                @isset($routes->bus_route_id)
                    <input type="hidden" name="bus_route_id" value="{{$routes->bus_route_id ?? ''}}">
                @endisset
                @if($editable != 'disabled')
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function () {
        validator = $('#awardRequestForm').validate();
    });
</script>
@endsection
