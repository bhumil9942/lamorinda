@extends('layouts.'.$layout)
@section('button')
@if (Auth::getDefaultDriver() == "mnre")
<a href="{{URL::to('/'.$prefix.'/create-city/')}}" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE CITY
</a>
@endif
@endsection
@section('content')
@section('title', 'Manage Cities')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                @include('elements.CommonElements._flash')
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>City ID</th>
                            <th>City Name</th>
                            <th>City Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($city as $citi)
                        <tr>
                            <td>{{ $citi->city_id }}</td>
                            <td>{{ $citi->city_name }}</td>
                            <td>
                                @if($citi->city_status == 1) 
                                    Active
                                @elseif($citi->city_status == 2)
                                    In-Active
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{url('/'.$prefix.'/edit-city/'.$citi->city_id)}}">Edit</a>
                                <a class="btn btn-xs btn-danger" href="{{url('/'.$prefix.'/delete-city/'.$citi->city_id)}}">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


