@extends('layouts.'.$layout)
@section('button')
@if (Auth::getDefaultDriver() == "mnre")
<a href="{{URL::to('/'.$prefix.'/create-student/')}}" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE STUDENT
</a>
@endif
@endsection
@section('content')
@section('title', 'Manage Students')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                @include('elements.CommonElements._flash')
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Student ID</th>
                            <th>Student Name</th>
                            <th>Parent Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($bookings as $student)
                        <tr>
                            <td>{{ $student->student_id }}</td>
                            <td>{{ $student->first_name }} {{ $student->last_name }}</td>
                            <td>{{ $student->first_parent_name }} {{ $student->second_parent_name }}</td>
                            <td>{{ $student->email }}</td>
                            <td>{{ $student->home_phone }}</td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{url('/'.$prefix.'/studentDetails/'.$student->student_id)}}">View</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


