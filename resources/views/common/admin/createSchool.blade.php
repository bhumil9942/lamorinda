@extends('layouts.'.$layout)
@section('content')
@section('title', 'Add School')
@php
    $editable = empty($editable) ? '' : $editable;
@endphp
@push('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
<div class="row">
    <div class="col-md-12">
        @include('elements.CommonElements._flash')
        <div class="box box-primary">
            <form action="{{URL::to('/'.$prefix.'/create-school')}}" id="awardRequestForm" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-header with-border">
                    <a href="{{URL::to($prefix.'/school')}}" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="school_code">{{ __('School Code') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="school_code" {{$editable ?? ''}} value="{{$school['school_code'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="school_name">{{ __('School Name') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="school_name" {{$editable ?? ''}} value="{{$school['school_name'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="school_image">{{ __('School Image') }} <span class="error">*</span></label>  
                                @isset($school->school_image)
                                    <input type="file" class="form-control" name="school_image" {{$editable ?? ''}} value="{{ $school->school_image }}">
                                @else
                                    <input type="file" class="form-control required" name="school_image" {{$editable ?? ''}} value="">
                                @endisset
                            </div>
                            @isset($school->school_image)
                            <div class="form-group">
                                <img src="{{ URL::to('/api/assets/user'). '/'.$school->school_image }}" height="100" width="200">
                            </div>
                            @endisset
                            <br>
                            <div class="form-group">
                                <label for="school_city_id">{{ __('City') }} <span class="error">*</span></label>  
                                <select class="form-control required" name="school_city_id"  {{$editable ?? ''}}>
                                    <option value="" selected> Select City </option>
                                    @foreach($cities as $city)
                                        <option  value="{{ $city->city_id }}"  @if(($school->school_city_id ?? '') == $city->city_id) selected @endif>{{ $city->city_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="school_year">{{ __('School Year') }} <span class="error">*</span></label>
                                <select class="form-control required" name="school_year" {{$editable ?? ''}}>
                                    <option value="" selected>Select Year</option>
                                    <option value="1" @if(($school->school_year ?? '') == 1) selected @endif>2022-2023</option>
                                    <option value="2" @if(($school->school_year ?? '') == 2) selected @endif>2023-2024</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="route_type">{{ __('Route Type') }} <span class="error">*</span></label>
                                <select class="form-control required" name="route_type" {{$editable ?? ''}}>
                                    <option value="" selected>Select Route Type</option>
                                    <option value="1" @if(($schoolBusRoute->route_type ?? '') == 1) selected @endif>AM</option>
                                    <option value="2" @if(($schoolBusRoute->route_type ?? '') == 2) selected @endif>PM</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="route_id">{{ __('Routes') }} <span class="error">*</span></label>  
                                <select class="js-example-basic-single" multiple="multiple" name="route_id[]"  {{$editable ?? ''}}>
                                    @foreach($routes as $route)
                                        <option  value="{{ $route->bus_route_id }}"  @if(($schoolBusRoute->route_id ?? '') == $route->bus_route_id) selected @endif>{{ $route->bus_route_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alternate_route_id">{{ __('Alternate Routes') }} <span class="error">*</span></label>  
                                <select class="js-example-basic-single" multiple="multiple" name="alternate_route_id[]"  {{$editable ?? ''}}>
                                    @foreach($routes as $route)
                                        <option  value="{{ $route->bus_route_id }}"  @if(($schoolBusRoute->alternate_route_id ?? '') == $route->bus_route_id) selected @endif>{{ $route->bus_route_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- <div class="form-group">
                                <label for="school_year">{{ __('School Year') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="school_year" {{$editable ?? ''}} value="{{$school['school_year'] ?? ''}}">
                            </div> -->
                        </div>
                    </div>
                </div>
                @isset($school->school_id)
                    <input type="hidden" name="school_id" value="{{$school->school_id ?? ''}}">
                @endisset
                @isset($school->school_image)
                    <input type="hidden" name="school_old_image" value="{{$school->school_image ?? ''}}">
                @endisset
                @if($editable != 'disabled')
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function () {
        validator = $('#awardRequestForm').validate();
    });
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
            // placeholder: "Select a Alternate Route",
            // allowClear: true,
    });

</script>
@endsection
