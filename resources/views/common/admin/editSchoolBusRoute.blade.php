@extends('layouts.'.$layout)
@section('content')
@section('title', 'Add School Bus Routes')
@push('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@php
    $editable = empty($editable) ? '' : $editable;
@endphp
<div class="row">
    <div class="col-md-12">
        @include('elements.CommonElements._flash')
        <div class="box box-primary">
            <form action="{{URL::to('/'.$prefix.'/create-school_bus_route/'.$schoolId)}}" id="spiceRequestForm" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-header with-border">
                    <a href="{{URL::to($prefix.'/school')}}" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <input type="hidden" name="school_id" value="{{$schoolId}}">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="school_id">{{ __('School Name') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" disabled name="school_id" {{$editable ?? ''}} value="{{$schools}}">
                            </div>
                            <div class="form-group">
                                <label for="route_type">{{ __('Route Type') }} <span class="error">*</span></label>
                                <select class="form-control required" name="route_type" {{$editable ?? ''}}>
                                    <option value="" selected>Select Route Type</option>
                                    <option value="1" @if(($schoolBusRoute->route_type ?? '') == 1) selected @endif>AM</option>
                                    <option value="2" @if(($schoolBusRoute->route_type ?? '') == 2) selected @endif>PM</option>
                                </select>
                            </div>
                            <!-- <div class="form-group">
                                <label for="route_id">{{ __('Routes') }} <span class="error">*</span></label>  
                                <select class="form-control required" name="route_id"  {{$editable ?? ''}}>
                                    <option value="" selected> Select Route </option>
                                    @foreach($routes as $route)
                                        <option  value="{{ $route->bus_route_id }}"  @if(($schoolBusRoute->route_id ?? '') == $route->bus_route_id) selected @endif>{{ $route->bus_route_name }}</option>
                                    @endforeach
                                </select>
                            </div> -->
                            <div class="form-group">
                                <label for="route_id">{{ __('Routes') }} <span class="error">*</span></label>  
                                <select class="js-example-basic-single" multiple="multiple" name="route_id[]"  {{$editable ?? ''}}>
                                    @foreach($routes as $route)
                                        <option  value="{{ $route->bus_route_id }}"  @if(in_array($route->bus_route_id,$rIds)) selected @endif>{{ $route->bus_route_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alternate_route_id">{{ __('Alternate Routes') }} <span class="error">*</span></label>  
                                <select class="js-example-basic-single" multiple="multiple" name="alternate_route_id[]"  {{$editable ?? ''}}>
                                    @foreach($routes as $route)
                                        <option  value="{{ $route->bus_route_id }}"  @if(in_array($route->bus_route_id,$aIds)) selected @endif>{{ $route->bus_route_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                @isset($schoolBusRoute->school_bus_route_id)
                    <input type="hidden" name="school_bus_route_id" value="{{$schoolBusRoute->school_bus_route_id ?? ''}}">
                @endisset
                @if($editable != 'disabled')
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(function () {
        validator = $('#spiceRequestForm').validate();
    });
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
            // placeholder: "Select a Alternate Route",
            // allowClear: true,
    });
</script>
@endsection
