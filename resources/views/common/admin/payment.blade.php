@extends('layouts.'.$layout)
@section('button')
@if (Auth::getDefaultDriver() == "mnre")
<!-- <a href="{{URL::to('/'.$prefix.'/create-student/')}}" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE STUDENT
</a> -->
@endif
@endsection
@section('content')
@section('title', 'Payments')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                @include('elements.CommonElements._flash')
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Booking ID</th>
                            <th>Transaction ID</th>
                            <th>Student Name</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Paid Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($payments as $payment)
                        <tr>
                            <td>{{ $payment->booking_id }}</td>
                            <td>{{ $payment->transaction_id }}</td>
                            <td>{{ $payment->student_name }}</td>
                            <td>{{ $payment->amount }}</td>
                            <td>{{ $payment->status }}</td>
                            <td>{{ $payment->paid_date }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


