@extends('layouts.'.$layout)
@section('button')
@if (Auth::getDefaultDriver() == "mnre")
<a href="{{URL::to('/'.$prefix.'/create-school/')}}" class="btn btn-info pull-right">
    <span class="btn-icon-wrapper pr-2 opacity-7"><i class="fa fa-plus-circle fa-w-20"></i></span>   CREATE SCHOOL
</a>
@endif
@endsection
@section('content')
@section('title', 'Manage Schools')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                @include('elements.CommonElements._flash')
            </div>
            <div class="col-md-12">
                <table class="table table-bordered listtable" style="width:100%">
                    <thead>
                        <tr>
                            <th>School ID</th>
                            <th>School Code</th>
                            <th>School Name</th>
                            <th>School Image</th>
                            <th>City Name</th>
                            <th>School Year</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($schools as $school)
                        <tr>
                            <td>{{ $school->school_id }}</td>
                            <td>{{ $school->school_code }}</td>
                            <td>{{ $school->school_name }}</td>
                            <td><img src="{{ URL::to('/api/assets/user'). '/'.$school->school_image }}" width="120px" height="70px"></td>
                            <td>{{ $school->name }}</td>
                            <td>
                                @if($school->school_year == 1) 
                                    2022-2023
                                @elseif($school->school_year == 2)
                                    2023-2024
                                @endif
                            </td>
                            <!-- <td>{{ $school->school_year }}</td> -->
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{url('/'.$prefix.'/edit-school/'.$school->school_id)}}">Edit</a>
                                <a class="btn btn-xs btn-danger" href="{{url('/'.$prefix.'/delete-school/'.$school->school_id)}}">Delete</a>
                                <br>
                                <a style="margin-top:4%" class="btn btn-xs btn-success" href="{{url('/'.$prefix.'/view-school_bus_route/'.$school->school_id)}}">View Routes</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


