@extends('layouts.'.$layout)
@section('content')
@section('title', 'Add Student')
@php
    $editable = empty($editable) ? '' : $editable;
@endphp
<div class="row">
    <div class="col-md-12">
        @include('elements.CommonElements._flash')
        <div class="box box-primary">
            <form action="{{URL::to('/'.$prefix.'/create-student')}}" id="awardRequestForm" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-header with-border">
                    <a href="{{URL::to($prefix.'/students')}}" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="school_year">{{ __('School Year') }} <span class="error">*</span></label>
                                <select class="form-control required" name="school_year" {{$editable ?? ''}}>
                                    <option value="" selected>Select Year</option>
                                    <option value="1" @if(($students->school_year ?? '') == 1) selected @endif>2022-2023</option>
                                    <option value="2" @if(($students->school_year ?? '') == 2) selected @endif>2023-2024</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="date_downloaded">{{ __('Date Downloaded') }} <span class="error">*</span></label>
                                <select class="form-control required" name="date_downloaded[]" {{$editable ?? ''}}>
                                    <option value="" selected> Select Dates </option>
                                    @foreach($allDates as $date)
                                        <option  value="{{ $date }}"  @if(($dateD ?? '') == $date) selected @endif>{{ $date }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="fname">{{ __('Student First Name') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="fname" {{$editable ?? ''}} value="{{$students['fname'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="lname">{{ __('Student Last Name') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="lname" {{$editable ?? ''}} value="{{$students['lname'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="address">{{ __('Mailing Address') }} <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="address" {{$editable ?? ''}} value="{{$students['address'] ?? ''}}">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="city">{{ __('City') }} <span class="error">*</span></label>  
                                <select class="form-control required" name="city"  {{$editable ?? ''}}>
                                    <option value="" selected> Select City </option>
                                    @foreach($cities as $city)
                                        <option  value="{{ $city->city_id }}"  @if(($students->city ?? '') == $city->city_id) selected @endif>{{ $city->city_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="state">{{ __('State') }} <span class="error">*</span></label>
                                <select class="form-control required" name="state" {{$editable ?? ''}}>
                                    <option value="" selected>Select State</option>
                                    <option value="1" @if(($students->state ?? '') == 1) selected @endif>CA</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="zipcode">{{ __('Zip Code') }} <span class="error">*</span></label>
                                <select class="form-control required" name="zipcode[]" {{$editable ?? ''}}>
                                    <option value="" selected> Select Zip Code </option>
                                    @foreach($zipcodes as $code)
                                        <option  value="{{ $code }}"  @if(($students->zipcode ?? '') == $code) selected @endif>{{ $code }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="grade">{{ __('Grade(Fall)') }} <span class="error">*</span></label>
                                <select class="form-control required" name="grade[]" {{$editable ?? ''}}>
                                    <option value="" selected> Select Grade </option>
                                    @foreach($grades as $gra)
                                        <option  value="{{ $gra }}"  @if(($students->grade ?? '') == $gra) selected @endif>{{ $gra }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="school">{{ __('School') }} <span class="error">*</span></label>  
                                <select class="form-control required" name="school"  {{$editable ?? ''}}>
                                    <option value="" selected> Select School </option>
                                    @foreach($schools as $sch)
                                        <option  value="{{ $sch->school_id }}"  @if(($students->school ?? '') == $sch->school_id) selected @endif>{{ $sch->school_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="school_code">{{ __('School ID') }} <span class="error">*</span></label>  
                                <select class="form-control required" name="school_code"  {{$editable ?? ''}}>
                                    <option value="" selected> Select School Code</option>
                                    @foreach($schools as $sch)
                                        <option  value="{{ $sch->school_code }}"  @if(($students->school_code ?? '') == $sch->school_code) selected @endif>{{ $sch->school_code }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="rte">{{ __('Rte #') }} <span class="error">*</span></label>
                                <select class="form-control required" name="rte[]" {{$editable ?? ''}}>
                                    <option value="" selected> Select Rte </option>
                                    @foreach($grades as $gra)
                                        <option  value="{{ $gra }}"  @if(($students->rte ?? '') == $gra) selected @endif>{{ $gra }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pass_type">{{ __('Pass Type (1,2,3)') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="pass_type" {{$editable ?? ''}} value="{{$students['pass_type'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="service_requested">{{ __('Service Requested') }} <span class="error">*</span></label>
                                <select class="form-control required" name="service_requested[]" {{$editable ?? ''}}>
                                    <option value="" selected> Select Service Requested </option>
                                    @foreach($pass_types as $type)
                                        <option  value="{{ $type->pass_id }}"  @if(($students->service_requested ?? '') == $type->pass_id) selected @endif>{{ $type->pass_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="waitlist">{{ __('Waitlist') }} <span class="error">*</span></label>
                                <select class="form-control required" name="waitlist" {{$editable ?? ''}}>
                                    <option value="" selected>Select Waitlist</option>
                                    <option value="1" @if(($students->waitlist ?? '') == 1) selected @endif>1</option>
                                    <option value="2" @if(($students->waitlist ?? '') == 2) selected @endif>2</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="am_bus_stop">{{ __('AM Bus Stop') }} <span class="error">*</span></label>  
                                <select class="form-control required" name="am_bus_stop"  {{$editable ?? ''}}>
                                    <option value="" selected> Select AM Bus Stop</option>
                                    @foreach($routes as $route)
                                        <option  value="{{ $route->bus_route_id }}"  @if(($student->am_bus_stop ?? '') == $route->bus_route_id) selected @endif>{{ $route->bus_route_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pm_bus_stop">{{ __('PM Bus Stop') }} <span class="error">*</span></label>  
                                <select class="form-control required" name="pm_bus_stop"  {{$editable ?? ''}}>
                                    <option value="" selected> Select PM Bus Stop</option>
                                    @foreach($routes as $route)
                                        <option  value="{{ $route->bus_route_id }}"  @if(($student->pm_bus_stop ?? '') == $route->bus_route_id) selected @endif>{{ $route->bus_route_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="p1_fname">{{ __('Parent 1 Full Name') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="p1_fname" {{$editable ?? ''}} value="{{$student['p1_fname'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="p2_fname">{{ __('Parent 2 Full Name') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="p2_fname" {{$editable ?? ''}} value="{{$student['p2_fname'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="email">{{ __('Email Address') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="email" {{$editable ?? ''}} value="{{$student['email'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="phone">{{ __('Home Phone') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="phone" {{$editable ?? ''}} value="{{$student['phone'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="cell_phone">{{ __('Cell Phone') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="cell_phone" {{$editable ?? ''}} value="{{$student['cell_phone'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="alt_phone">{{ __('Alternate Cell Number') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="alt_phone" {{$editable ?? ''}} value="{{$student['alt_phone'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="authorize_1">{{ __('Authorized 1') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="authorize_1" {{$editable ?? ''}} value="{{$student['authorize_1'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="authorize_2">{{ __('Authorized 2') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="authorize_2" {{$editable ?? ''}} value="{{$student['authorize_2'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="authorize_3">{{ __('Authorized 3') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="authorize_3" {{$editable ?? ''}} value="{{$student['authorize_3'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="disability">{{ __('Disability') }} <span class="error">*</span></label>
                                <select class="form-control required" name="disability" {{$editable ?? ''}}>
                                    <option value="" selected>Select Disability</option>
                                    <option value="1" @if(($student->disability ?? '') == 1) selected @endif>Yes</option>
                                    <option value="2" @if(($student->disability ?? '') == 2) selected @endif>No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="explain">{{ __('Explain') }} <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="explain" {{$editable ?? ''}} value="{{$student['explain'] ?? ''}}">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="comments">{{ __('Comments or Special Requests') }} <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="comments" {{$editable ?? ''}} value="{{$student['comments'] ?? ''}}">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="initials">{{ __('Initials') }} <span class="error">*</span></label>
                                <select class="form-control required" name="initials" {{$editable ?? ''}}>
                                    <option value="" selected>Select Initials</option>
                                    <option value="1" @if(($student->initials ?? '') == 1) selected @endif>1</option>
                                    <option value="2" @if(($student->initials ?? '') == 2) selected @endif>2</option>
                                    <option value="3" @if(($student->initials ?? '') == 3) selected @endif>3</option>
                                    <option value="4" @if(($student->initials ?? '') == 4) selected @endif>4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="monthly_payments">{{ __('Monthly Payments') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="monthly_payments" {{$editable ?? ''}} value="{{$student['monthly_payments'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="discount">{{ __('Discount') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="discount" {{$editable ?? ''}} value="{{$student['discount'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="free_transportation">{{ __('Free Transportation') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="free_transportation" {{$editable ?? ''}} value="{{$student['free_transportation'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="f_payment_type">{{ __('1st Payment Type') }} <span class="error">*</span></label>
                                <select class="form-control required" name="f_payment_type" {{$editable ?? ''}}>
                                    <option value="" selected>Select 1st Payment Type</option>
                                    <option value="1" @if(($student->f_payment_type ?? '') == 1) selected @endif>Credit Card</option>
                                    <option value="2" @if(($student->f_payment_type ?? '') == 2) selected @endif>Cheque</option>
                                    <option value="3" @if(($student->f_payment_type ?? '') == 3) selected @endif>Cash</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="f_cc">{{ __('1st CC/Check#') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="f_cc" {{$editable ?? ''}} value="{{$student['f_cc'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="f_exp_date">{{ __('Exp Date') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="f_exp_date" {{$editable ?? ''}} value="{{$student['f_exp_date'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="f_amt_paid">{{ __('1st Amount Paid') }} <span class="error">*</span></label>
                                <select class="form-control required" name="f_amt_paid" {{$editable ?? ''}}>
                                    <option value="" selected>Select 1st Amount Paid</option>
                                    <option value="1" @if(($student->f_amt_paid ?? '') == 1) selected @endif>$225</option>
                                    <option value="2" @if(($student->f_amt_paid ?? '') == 2) selected @endif>$325</option>
                                    <option value="3" @if(($student->f_amt_paid ?? '') == 3) selected @endif>$125</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="f_date_payment_processed">{{ __('1st Date Payment Processed') }} <span class="error">*</span></label>
                                <select class="form-control required" name="f_date_payment_processed" {{$editable ?? ''}}>
                                    <option value="" selected>Select 1st Date Payment Processed</option>
                                    <option value="1" @if(($student->f_date_payment_processed ?? '') == 1) selected @endif>02/02/2022</option>
                                    <option value="2" @if(($student->f_date_payment_processed ?? '') == 2) selected @endif>02/04/2022</option>
                                    <option value="3" @if(($student->f_date_payment_processed ?? '') == 3) selected @endif>02/06/2022</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="s_payment_type">{{ __('2nd Payment Type') }} <span class="error">*</span></label>
                                <select class="form-control required" name="s_payment_type" {{$editable ?? ''}}>
                                    <option value="" selected>Select 2nd Payment Type</option>
                                    <option value="1" @if(($student->s_payment_type ?? '') == 1) selected @endif>Credit Card</option>
                                    <option value="2" @if(($student->s_payment_type ?? '') == 2) selected @endif>Cheque</option>
                                    <option value="3" @if(($student->s_payment_type ?? '') == 3) selected @endif>Cash</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="s_cc">{{ __('2nd CC/Check#') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="s_cc" {{$editable ?? ''}} value="{{$student['s_cc'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="s_exp_date">{{ __('Exp Date 2') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="s_exp_date" {{$editable ?? ''}} value="{{$student['s_exp_date'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="s_amt_paid">{{ __('2nd Amount Paid') }} <span class="error">*</span></label>
                                <select class="form-control required" name="s_amt_paid" {{$editable ?? ''}}>
                                    <option value="" selected>Select 2nd Amount Paid</option>
                                    <option value="1" @if(($student->s_amt_paid ?? '') == 1) selected @endif>$225</option>
                                    <option value="2" @if(($student->s_amt_paid ?? '') == 2) selected @endif>$325</option>
                                    <option value="3" @if(($student->s_amt_paid ?? '') == 3) selected @endif>$125</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="s_date_payment_processed">{{ __('2nd Date Payment Processed') }} <span class="error">*</span></label>
                                <select class="form-control required" name="s_date_payment_processed" {{$editable ?? ''}}>
                                    <option value="" selected>Select 2nd Date Payment Processed</option>
                                    <option value="1" @if(($student->s_date_payment_processed ?? '') == 1) selected @endif>02/02/2022</option>
                                    <option value="2" @if(($student->s_date_payment_processed ?? '') == 2) selected @endif>02/04/2022</option>
                                    <option value="3" @if(($student->s_date_payment_processed ?? '') == 3) selected @endif>02/06/2022</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="t_payment_type">{{ __('3rd Payment Type') }} <span class="error">*</span></label>
                                <select class="form-control required" name="t_payment_type" {{$editable ?? ''}}>
                                    <option value="" selected>Select 3rd Payment Type</option>
                                    <option value="1" @if(($student->t_payment_type ?? '') == 1) selected @endif>Credit Card</option>
                                    <option value="2" @if(($student->t_payment_type ?? '') == 2) selected @endif>Cheque</option>
                                    <option value="3" @if(($student->t_payment_type ?? '') == 3) selected @endif>Cash</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="t_cc">{{ __('3rd CC/Check#') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="t_cc" {{$editable ?? ''}} value="{{$student['t_cc'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="t_exp_date">{{ __('Exp Date 3') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="t_exp_date" {{$editable ?? ''}} value="{{$student['t_exp_date'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="t_amt_paid">{{ __('3rd Amount Paid') }} <span class="error">*</span></label>
                                <select class="form-control required" name="t_amt_paid" {{$editable ?? ''}}>
                                    <option value="" selected>Select 3rd Amount Paid</option>
                                    <option value="1" @if(($student->t_amt_paid ?? '') == 1) selected @endif>$225</option>
                                    <option value="2" @if(($student->t_amt_paid ?? '') == 2) selected @endif>$325</option>
                                    <option value="3" @if(($student->t_amt_paid ?? '') == 3) selected @endif>$125</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="t_date_payment_processed">{{ __('3rd Date Payment Processed') }} <span class="error">*</span></label>
                                <select class="form-control required" name="t_date_payment_processed" {{$editable ?? ''}}>
                                    <option value="" selected>Select 3rd Date Payment Processed</option>
                                    <option value="1" @if(($student->t_date_payment_processed ?? '') == 1) selected @endif>02/02/2022</option>
                                    <option value="2" @if(($student->t_date_payment_processed ?? '') == 2) selected @endif>02/04/2022</option>
                                    <option value="3" @if(($student->t_date_payment_processed ?? '') == 3) selected @endif>02/06/2022</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="received_by">{{ __('Received By') }} <span class="error">*</span></label>
                                <select class="form-control required" name="received_by" {{$editable ?? ''}}>
                                    <option value="" selected>Select Received By</option>
                                    <option value="1" @if(($student->received_by ?? '') == 1) selected @endif>WH</option>
                                    <option value="2" @if(($student->received_by ?? '') == 2) selected @endif>DS</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="refund_requested">{{ __('Refund Requested') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="refund_requested" {{$editable ?? ''}} value="{{$student['refund_requested'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="refund_issued">{{ __('Refund Issued') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="refund_issued" {{$editable ?? ''}} value="{{$student['refund_issued'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="amt_refunded">{{ __('Amount Refunded') }} <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="amt_refunded" {{$editable ?? ''}} value="{{$student['amt_refunded'] ?? ''}}">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="notes">{{ __('Notes') }} <span class="error">*</span></label>
                                <textarea type="text" class="form-control required" name="notes" {{$editable ?? ''}} value="{{$student['notes'] ?? ''}}">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="release_for_required">{{ __('Release Form Required') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="release_for_required" {{$editable ?? ''}} value="{{$student['release_for_required'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="release_for_received">{{ __('Release Form Received') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="release_for_received" {{$editable ?? ''}} value="{{$student['release_for_received'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="ride_bus_last_yr">{{ __('Did this student ride the bus last year?') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="ride_bus_last_yr" {{$editable ?? ''}} value="{{$student['ride_bus_last_yr'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="date_printed">{{ __('Date Printed') }} <span class="error">*</span></label>
                                <select class="form-control required" name="date_printed[]" {{$editable ?? ''}}>
                                    <option value="" selected> Select Date Printed </option>
                                    @foreach($allDates as $date)
                                        <option  value="{{ $date }}"  @if(($student->date_printed ?? '') == $date) selected @endif>{{ $date }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sub_id">{{ __('Subscription ID') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="sub_id" {{$editable ?? ''}} value="{{$student['sub_id'] ?? ''}}">
                            </div>
                        </div>
                    </div>
                        
                </div>
                @isset($student->student_id)
                    <input type="hidden" name="student_id" value="{{$student->student_id ?? ''}}">
                @endisset
                @if($editable != 'disabled')
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function () {
        validator = $('#awardRequestForm').validate();
    });
</script>
@endsection
