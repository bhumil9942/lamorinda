@extends('layouts.'.$layout)
@section('content')
@section('title', 'Add Pass Types')
@php
    $editable = empty($editable) ? '' : $editable;
@endphp
<div class="row">
    <div class="col-md-12">
        @include('elements.CommonElements._flash')
        <div class="box box-primary">
            <form action="{{URL::to('/'.$prefix.'/create-pass_type')}}" id="spiceRequestForm" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-header with-border">
                    <a href="{{URL::to($prefix.'/pass_type')}}" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="pass_name">{{ __('Pass Name') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="pass_name" {{$editable ?? ''}} value="{{$pass_type['pass_name'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="pass_monthly">{{ __('Price') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="pass_monthly" {{$editable ?? ''}} value="{{$pass_type['pass_monthly'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="pass_status">{{ __('Status') }} <span class="error">*</span></label>
                                <select class="form-control required" name="pass_status" {{$editable ?? ''}}>
                                    <option value="" selected>Select Status</option>
                                    <option value="1" @if(($pass_type->pass_status ?? '') == 1) selected @endif>Active</option>
                                    <option value="2" @if(($pass_type->pass_status ?? '') == 2) selected @endif>In-Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                @isset($pass_type->pass_id)
                    <input type="hidden" name="pass_id" value="{{$pass_type->pass_id ?? ''}}">
                @endisset
                @if($editable != 'disabled')
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function () {
        validator = $('#spiceRequestForm').validate();
    });
</script>
@endsection
