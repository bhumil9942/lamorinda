@extends('layouts.'.$layout)
@section('content')
@section('title', 'Student Information')
<div class="box box-primary">
<div class="box-header with-border">
@php $backurl = $prefix.'/tournaments'; @endphp
    <a href="{{URL::to($backurl)}}" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
</div>
<div class="row">
    <div class="col-md-12">
        <ul id="installations-tabs" class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#system"><b>Student Details</b></a></li>
            </ul>
            <div class="tab-content">
                <div id="system" class="tab-pane fade in active">
                    <div class="box box-primary">
                        <!-- <div class="box-header with-border">
                        @php $backurl = $prefix.'/activetournament'; @endphp
                            <a href="{{URL::to($backurl)}}" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                        </div> -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament ID <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament->tournament_id }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>User ID <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament->user_id }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Tournament Details</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Name <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament->tourny_name }}</p>
                                        <label>Location <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament->tourny_location }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Host Name <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament['fullname'] }}</p>
                                        <label>Geo tag <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament->tourny_lat.", ".$tournament->tourny_long}}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Image <span class="error">*</span></label>
                                        <p class="mb-25"><img src="{{ URL::to('/api/assets/user').'/'.$tournament->tourny_image }}" height="100" width="200" /></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Start Date & Time <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament->start_date." ".$tournament->start_time}}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>End Date & Time <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament->end_date." ".$tournament->end_time}}</p>
                                    </div>
                                </div>
                                <!-- <div class="col-md-3">
                                    <div class="form-group">
                                       
                                       
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Geo tag <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament->tourny_lat.", ".$tournament->tourny_long}}</p>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="box-header with-border">
                            <h3 class="box-title">Tournament Specifications</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Minimum Entry <span class="error">*</span></label>
                                        <p class="mb-25">${{ $tournament->tourny_min_entry }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Type <span class="error">*</span></label>
                                        @if($tournament->tourny_type == 1)
                                            <p class="mb-25">Weigh-In</p>
                                        @elseif($tournament->tourny_type == 2)
                                            <p class="mb-25">Voting Only</p>
                                        @elseif($tournament->tourny_type == 3)
                                            <p class="mb-25">Measurement</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Spices Name <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament['spice_name'] }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tournament Description <span class="error">*</span></label>
                                        <p class="mb-25">{{ $tournament->tourny_desc }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
