@extends('layouts.'.$layout)
@section('content')
@section('title', 'Add City')
@php
    $editable = empty($editable) ? '' : $editable;
@endphp
<div class="row">
    <div class="col-md-12">
        @include('elements.CommonElements._flash')
        <div class="box box-primary">
            <form action="{{URL::to('/'.$prefix.'/create-city')}}" id="awardRequestForm" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-header with-border">
                    <a href="{{URL::to($prefix.'/city')}}" class="btn-shadow btn btn-danger btn-xs pull-right">Back</a>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="city_name">{{ __('City Name') }} <span class="error">*</span></label>
                                <input type="text" class="form-control required" name="city_name" {{$editable ?? ''}} value="{{$city['city_name'] ?? ''}}">
                            </div>
                            <div class="form-group">
                                <label for="city_status">{{ __('City Status') }} <span class="error">*</span></label>
                                <select class="form-control required" name="city_status" {{$editable ?? ''}}>
                                    <option value="" selected>Select Status</option>
                                    <option value="1" @if(($city->city_status ?? '') == 1) selected @endif>Active</option>
                                    <option value="2" @if(($city->city_status ?? '') == 2) selected @endif>In-Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                @isset($city->city_id)
                    <input type="hidden" name="city_id" value="{{$city->city_id ?? ''}}">
                @endisset
                @if($editable != 'disabled')
                    <div class="box-footer">
                        <input type="submit" class="mt-1 btn btn-primary" value="Submit">
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function () {
        validator = $('#awardRequestForm').validate();
    });
</script>
@endsection
