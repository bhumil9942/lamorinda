<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sanction Request Receipt Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        <h2 style="text-align: center;">
            @if ($sanction->toMail == "state-agency")
                Sanction Request Receipt Confirmation
            @else
                Sanction request received
            @endif
        </h2>
        <br>

        @if ($sanction->toMail != "state-agency")
            <p>A sanction request with the following details has been received.</p>
            <br>
        @endif

        <p>
            Name of the Nodal Agency: {{$sanction->stateAgency}}<br>
            Contact person: {{$sanction->contact_person}}<br>
            Telephone number: {{$sanction->phone}}<br>
            Email address: {{$sanction->email}}
        </p>
        <br>

        @if ($sanction->toMail == "state-agency")
            <p>Your sanction request for the following proposal has been issued to MNRE.</p>
            <br>
        @endif

        @php
            $locations = json_decode($sanction->locations, true);
        @endphp

        <p>
            1 Agency ID : {{$sanction->stateAgencyCode}}<br>
            2 FY Year/ Period : {{$sanction->fin_year}}<br>
            3 Name of the Programme : {{$sanction->program}}<br>
            4 Number of solar street lights requested : {{$sanction->requested_ssl}}<br>
            5 No. of District to be covered : {{$sanction->covered_district}}<br>
            6 No. of Villages to be covered : {{$sanction->covered_villages}}<br>
            7 Special Areas : {{$sanction->special_areas}}<br>
            8 List of locations : Please check attachment (pdf)
        </p>
    </div>
</body>
</html>
