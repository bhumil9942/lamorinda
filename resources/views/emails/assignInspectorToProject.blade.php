<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sanction Request Receipt Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        @if ($project->toInstaller == 'true')
            <h2 style="text-align: center;">Allocation of an inspector</h2>
            <br>
            <p>Dear {{$project->installer}},</p>
            <p>{{$project->stateAgency}} has assigned Mr. {{$project->inspector}} to inspect the project bearing the id {{$project->project_code}} installed by you.</p>
            <br>
            <p>This is an auto-generated email. Please do not reply to this email.</p>
        @endif

        @if ($project->toInstaller == 'false')
            <h2 style="text-align: center;">Project Allocaton to an Inspector</h2>
            <p>Dear {{$project->inspector}},</p>
            <p>You have been assigned a project for inspection. The details of the system, project & installer are as follows:</p>
            <p>
                1 Project ID : {{$project->project_code}}<br>
                2 Name of the programe : {{$project->program}}<br>
                3 Sanction ID : {{$project->sanction_code}}<br>
                4 Allocation Date : {{date('d M Y', strtotime($project->allocation_date))}}<br>
                5 Name of the Installer : {{$project->installer}}<br>
                6 Contact number of the Installer : {{$project->installer_contact}}<br>
                7 Installer ID : {{$project->installer_code}}<br>
                8 Name of District : {{$project->district}}<br>
                9 Block Number : {{$project->block}}<br>
                10 Name of Village : {{$project->village}}<br>
                11 Area/Location : {{$project->area}}<br>
                12 Number of solar street lights : {{$project->installed_ssl}}<br>
            </p>
            <br>
            <p>This is an auto-generated email. Please do not reply to this email.</p>
        @endif
    </div>
</body>
</html>
