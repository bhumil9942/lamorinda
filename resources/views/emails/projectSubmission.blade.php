<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sanction Request Receipt Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        @if ($project->toInstaller == 'true')
            <h2 style="text-align: center;">Project Installation Confirmation</h2>
            <br>
            <p>This is to inform you that the inspection request for the project with the following details has been submitted to {{$project->stateAgency}}</p>
            <br>
            <p>
                1 Project ID : {{$project->project_code}}<br>
                2 Name of the programme : {{$project->program}}<br>
                3 Sanction ID : {{$project->sanction_code}}<br>
                4 Allocation Date : {{date('d M Y', strtotime($project->allocation_date))}}<br>
                5 Name of the Installer : {{$project->installer}}<br>
                6 Installer ID : {{$project->installer_code}}<br>
                7 Name of District : {{$project->district}}<br>
                8 Block Number : {{$project->block}}<br>
                9 Name of Village : {{$project->village}}<br>
                10 Area/Location : {{$project->area}}<br>
                11 Number of solar street lights installed : {{$project->installed_ssl}}<br>
            </p>
            <br>
            <p>This is an auto-generated email. Please do not reply to this email.</p>
        @endif

        @if ($project->toInstaller == 'false')
            <h2 style="text-align: center;">Project Installation Completion Confirmation</h2>
            <br>
            <p>This is to inform you that the inspection request for the project with the following details has been received.</p>
            <br>
            <p>
                1 Project ID : {{$project->project_code}}<br>
                2 Name of the programme : {{$project->program}}<br>
                3 Sanction ID : {{$project->sanction_code}}<br>
                4 Allocation Date : {{date('d M Y', strtotime($project->allocation_date))}}<br>
                5 Name of the Installer : {{$project->installer}}<br>
                6 Installer ID : {{$project->installer_code}}<br>
                7 Name of District : {{$project->district}}<br>
                8 Block Number : {{$project->block}}<br>
                9 Name of Village : {{$project->village}}<br>
                10 Area/Location : {{$project->area}}<br>
                11 Number of solar street lights installed : {{$project->installed_ssl}}<br>
            </p>
            <p>You are requested to assign an inspector/ provide approval/ comments through the portal.</p>
            <br>
            <p>This is an auto-generated email. Please do not reply to this email.</p>
        @endif
    </div>
</body>
</html>
