<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sanction Request Receipt Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        @if ($project->action == 'approve')
            <h2 style="text-align: center;">Approval</h2>
            <br>
            <p>Dear {{$project->installer}},</p>
            <p>{{$project->stateAgency}} has approved the project bearing the id {{$project->project_code}} installed by you. This information is for your reference.</p>
            <br>
            <p>This is an auto-generated email. Please do not reply to this email.</p>
        @endif

        @if ($project->action == 'reject')
            <h2 style="text-align: center;">Rejection</h2>
            <br>
            <p>Dear {{$project->installer}},</p>
            <p>{{$project->stateAgency}} has rejected the project bearing the id {{$project->project_code}} installed by you. Following are the reasons of rejection.</p>
            <p><em>{{$project->feedback ?? ''}}</em></p>
            <br>
            <p>This is an auto-generated email. Please do not reply to this email.</p>
        @endif

        @if ($project->action == 'feedback')
            <h2 style="text-align: center;">Comments</h2>
            <br>
            <p>Dear {{$project->installer}},</p>
            <p>{{$project->stateAgency}} has provided comments for the project bearing the id {{$project->project_code}} installed by you. Following are the details:</p>
            <p><em>{{$project->feedback ?? ''}}</em></p>
            <br>
            <p>This is an auto-generated email. Please do not reply to this email.</p>
        @endif
    </div>
</body>
</html>
