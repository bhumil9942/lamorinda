<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sanction Request Receipt Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        <h2 style="text-align: center;">Sanction Approval Confirmation</h2>
        <br>

        <p>This is to inform you that your organization has been sanctioned for the support of implementation of "Offgrid and Decentralized Solar PV Application Programme" .</p>
        <p>Details of projects sanction are as follows:</p>
        <br>

        @php
            $locations = json_decode($sanction->locations, true);
            $sanction_letter = json_decode($sanction->sanction_letter, true);
        @endphp

        <p>
            Name of the Agency : {{$sanction->stateAgency}}<br>
            1 Agency ID : {{$sanction->stateAgencyCode}}<br>
            2 FY Year/ Period : {{$sanction->fin_year}}<br>
            3 Name of the Programme : {{$sanction->program}}<br>
            4 Number of solar street lights requested : {{$sanction->requested_ssl}}<br>
            5 No. of District to be covered : {{$sanction->covered_district}}<br>
            6 No. of Villages to be covered : {{$sanction->covered_villages}}<br>
            7 Special Areas : {{$sanction->special_areas}}<br>
            8 Proposed date of completion : {{$sanction->completion_date}}<br>
            9 List of locations : Please check attachment (pdf)
            10 Number of solar street lights approved : {{$sanction->alloted_ssl}}<br>
            11 Sanction Letter Upload : Please check attachment (pdf)
        </p>
        <br>
        <p>This is an auto-generated email. Please do not reply to this email.</p>
    </div>
</body>
</html>




