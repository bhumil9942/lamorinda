<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        <h2 style="text-align: center;">Registration Confirmation</h2>
        <br>
        <p>Dear {{$installer->name}},</p>
        <br>
        <p>You have been selected by <b>{{$installer->stateAgency}}</b> to work as an installer for the “"Offgrid and Decentralized Solar PV Application Programme" of MNRE.</p>
        <br>
        <p>This is an auto-generated email. Please do not reply to this email.</p>
    </div>
</body>
</html>
