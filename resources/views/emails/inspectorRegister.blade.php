<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        <h2 style="text-align: center;">Inspector Registration</h2>
        <br>
        <p>Dear {{$inspector->name}},</p>
        <p>You have been registered to the MNRE solar street light portal.</p>
        <br>
        <p>This is to inform you that you have been successfully registered in the Solar Street Light Monitoring Portal of MNRE. Your unique registration no. in the portal is: {{$inspector->inspector_code}}. You can access your account with the details below:</p>
        <br>
        <p>Username : <b>{{$inspector->email}}</b></p>
        <p>Password : <b>12345678</b></p>
        <br>
        <p>The password can be changed through your account.</p>
        <p>This is an auto-generated email. Please do not reply to this email.</p>
    </div>
</body>
</html>

