<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        <h2 style="text-align: center;">Registration Confirmation</h2>
        <br>
        <p>Dear {{$installer->name}},</p>
        <br>
        <p>You have been selected by <b>{{$installer->stateAgency}}</b> to work as an installer for the “"Offgrid and Decentralized Solar PV Application Programme" of MNRE.</p>
        <p>You have been successfully registered in the Solar Street Light Monitoring Web Portal of MNRE and your unique registration no. is <b>{{$installer->installer_code}}</b>. You can access your account in the portal with details below:</p>
        <br>
        <p>Username : <b>{{$installer->email}}</b></p>
        <p>Password : <b>12345678</b></p>
        <br>
        <p>The password can be changed through your account.</p>
        <p>This is an auto-generated email. Please do not reply to this email.</p>
    </div>
</body>
</html>
