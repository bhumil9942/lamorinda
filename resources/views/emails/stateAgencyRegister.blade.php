<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        <p>This is to inform you that your agency <b>{{$stateAgency->name}}</b> has been successfully registered in the Solar Street Light Monitoring Web Portal of MNRE. Your unique registration no. is <b>{{$stateAgency->agency_code}}</b>. You can access your account in the portal with details below:</p>
        <br>
        <p>Username : <b>{{$stateAgency->email}}</b></p>
        <p>Password : <b>12345678</b></p>
        <br>
        <p>The password can be changed through your account.</p>
        <p>This is an auto-generated email. Please do not reply to this email.</p>
    </div>
</body>
</html>
