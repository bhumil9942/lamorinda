<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sanction Request Receipt Confirmation</title>
</head>
<body>
    <div style="padding:20px;">
        @if ($project->toInstaller == 'true')
            @if ($project->action == 'comment')
                <h2 style="text-align: center;">Comments</h2>
                <br>
                <p>Dear {{$project->installer}},</p>
                <p>The Inspector {{$project->inspector}} has provided comments for the project bearing the id {{$project->project_code}} installed by you. Following are the details:</p>
                @if (isset($logs) && !empty($logs))
                    <li><em>{{$logs->review}}</em></li>
                @endif
                <br>
                <p>This is an auto-generated email. Please do not reply to this email.</p>
            @endif
            @if ($project->action == 'approve')
                <h2 style="text-align: center;">Approval</h2>
                <br>
                <p>Dear {{$project->installer}},</p>
                <p>The Inspector {{$project->inspector}} has approved the project bearing the id {{$project->project_code}} installed by you. This information is for your reference. the details have been forwarded to SNA for any additional comments/approval.</p>
                <br>
                <p>This is an auto-generated email. Please do not reply to this email.</p>
            @endif
        @endif

        @if ($project->toInstaller == 'false')
            @if ($project->action == 'comment')
                <h2 style="text-align: center;">Comments</h2>
                <br>
                <p>Dear Sir/ Madam,</p>
                <p>The Inspector {{$project->inspector}} has provided comments for the project bearing the id {{$project->project_code}}. Following are the details:</p>
                @if (isset($logs) && !empty($logs))
                    <li><em>{{$logs->review}}</em></li>
                @endif
                <br>
                <p>This is an auto-generated email. Please do not reply to this email.</p>
            @endif
            @if ($project->action == 'approve')
                <h2 style="text-align: center;">Approval</h2>
                <br>
                <p>Dear {{$project->installer}},</p>
                <p>The Inspector {{$project->inspector}} has approved the project bearing the id {{$project->project_code}}. This information is for your reference.</p>
                <p>You are requested to provide approval/additional comments through the portal.</p>
                <br>
                <p>This is an auto-generated email. Please do not reply to this email.</p>
            @endif
        @endif
    </div>
</body>
</html>

