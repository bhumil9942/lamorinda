<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Project Allocation</title>
</head>
<body>
    <div style="padding:20px;">
        <h2 style="text-align: center;">Project Allocation</h2>
        <br>
        <p>This is to inform you that your organization has been allocated a project under the "Offgrid and Decentralized Solar PV Application Programme" .</p>
        <p>Details of project allocation are as follows:</p>
        <br>

        @php
            $allocation_letter = json_decode($project->allocation_letter, true);
        @endphp

        <p>
            1 Project ID : {{$project->project_code}}<br>
            2 Name of the programme : {{$project->program}}<br>
            3 Sanction ID : {{$project->sanction_code}}<br>
            4 Allocation Date : {{date('d M Y', strtotime($project->allocation_date))}}<br>
            5 Name of the Installer : {{$project->installer}}<br>
            6 Installer ID : {{$project->installer_code}}<br>
            7 Name of District : {{$project->district}}<br>
            8 Block Number : {{$project->block}}<br>
            9 Name of Village : {{$project->village}}<br>
            10 Area/Location : {{$project->area}}<br>
            11 Village contact person name : {{$project->village_contact_person}}<br>
            12 Village contact person number : {{$project->village_contact_person_mobile}}<br>
            13 Number of solar street lights allocated : {{$project->allocated_ssl}}<br>
            14 Last date for completion of installation : {{$project->installation_completion_date}}<br>
            15 Letter of allocation Upload : Please check attachment (pdf)
        </p>
        <br>
        <p>If you have any querry, please contact:</p>
        <p>
            Name of the Contact person: {{$project->contact_person}}<br>
            Phone number: {{$project->phone}}<br>
            Email: {{$project->email}}
        </p>
        <br>
        <p>This is an auto-generated email. Please do not reply to this email.</p>
    </div>
</body>
</html>
