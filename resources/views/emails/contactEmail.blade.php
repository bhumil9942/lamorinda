<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Email</title>
</head>
<body>
<div style="padding:20px;">
    <h3>Contact Email</h3>
    <p>Name: {{ $mail_data['name'] }}</p>
    <p>Email: {{ $mail_data['email'] }}</p>
    <p>Contact No: {{ $mail_data['contact_no'] }}</p>
    <p>Description: {{ $mail_data['desc'] }}</p>
</div>
</body>
</html>
