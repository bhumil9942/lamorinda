<footer id="footer">
    <div class="col-md-3 text-center pr-0">
        <img class="footflags pull-left" src="{{URL::asset('public/images/EU.png')}}">
        <div class="pull-left m-0-15">
            <label class="lh-10">EU-India: Technical</label><br>
            <label>Cooperation - Energy Project</label>
        </div>
        <img class="footflags pull-left" src="{{URL::asset('public/images/IN.jpg')}}">
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-2 text-left mt-8">
        
    </div>
    <div class="col-md-4 mt-8">
        <ul class="pull-right list-inline">
            <li><a class="footerlinks" href="javascript:void(0)">Privacy Policy</a></li>
            <li><a class="footerlinks" href="javascript:void(0)">Terms of use</a></li>
        </ul>
    </div>
    <div class="clearfix"></div>
</footer>
