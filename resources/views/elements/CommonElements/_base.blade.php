
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ URL::asset('js/base_forms_validation.js') }}"></script>
<script src="{{ URL::asset('js/password-validation.js') }}"></script>
<script src="{{ URL::asset('js/icheck.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/daterangepicker.js') }}"></script>
<script src="{{ URL::asset('js/Chart.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ URL::asset('js/adminlte.min.js') }}"></script>
<script src="{{ URL::asset('js/select2.full.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('js/dataTables.bootstrap.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.2/dist/Chart.min.js"></script>
<script src="{{ URL::asset('js/custom.js') }}"></script>
<script>
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(function () {
        baseUrl = "{{url('/')}}"+"/";
        authUserType = '{{Auth::getDefaultDriver()}}';

        $('.fa-toggle-on, .fa-toggle-off, .editbutt').click(function(){
		    $('#loader').css({'display': 'block'});
        });
        $('.listtable').DataTable({ 'paging': true,'lengthChange': false,'searching': true,'ordering': true,'info': true,'autoWidth': false,'pageLength': 20 });
        $('.select2').select2();
        $('#ddlParamValue').select2({tags: true});
        $('#dataForm').validate();
        $('.datepicker').datepicker({ autoclose: true, format: 'dd-mm-yyyy', orientation: 'bottom', todayHighlight: true, startDate: new Date() });
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-red'
        });
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        @if(Session::has('msg'))
            $('#msgModal').modal('show');
        @endif
    });
</script>
