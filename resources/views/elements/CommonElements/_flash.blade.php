@if(Session::has('success'))
<div id="successmsg" class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  	<b>Success: </b>{{ Session::get('success') }}
</div>
@endif

@if(Session::has('fail'))
<div id="failmsg" class="alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  	<b>Failure: </b>{{ Session::get('fail') }}
</div>
@endif
<script>
	window.setTimeout(function() {
	    $("#successmsg").fadeTo(500, 0).slideUp(500, function(){
	        $(this).remove(); 
	    });
	}, 2000);
	window.setTimeout(function() {
	    $("#failmsg").fadeTo(500, 0).slideUp(500, function(){
	        $(this).remove(); 
	    });
	}, 2000);
</script>