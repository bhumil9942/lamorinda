<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="{{ request()->is('state-agency/dashboard') ? 'active' : '' }}"><a href="{{url(Auth::getDefaultDriver().'/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i><span>User Management</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('/state-agency/installers')}}"><i class="fa fa-user-plus"></i>Installers</a></li>
                    <li><a href="{{URL::to('/state-agency/inspectors')}}"><i class="fa fa-user-plus"></i>Inspectors</a></li>
                </ul>
            </li>
            <li class="{{ request()->is('state-agency/sanctions') || request()->is('state-agency/sanction-detail*') || request()->is('state-agency/edit-sanction*') ? 'active' : '' }}"><a href="{{url('/state-agency/sanctions')}}"><i class="fa fa-file-archive-o"></i> <span>Manage Sanctions</span></a></li>
            <li class="{{ request()->is('state-agency/projects') || request()->is('state-agency/create-project') ||  request()->is('state-agency/project-detail*') || request()->is('state-agency/edit-project*') ? 'active' : '' }}"><a href="{{url('/state-agency/projects')}}"><i class="fa fa-file"></i> <span>Manage Projects</span></a></li>
            <li class="{{ request()->is('state-agency/systems') || request()->is('state-agency/system-detail*') ? 'active' : '' }}"><a href="{{url('/state-agency/systems')}}"><i class="fa fa-rocket"></i> <span>Systems</span></a></li>
            <li class="{{ request()->is('state-agency/help') ? 'active' : '' }}"><a href="{{url('state-agency/help')}}"><i class="fa fa-exclamation-circle"></i> <span>Help</span></a></li>
        </ul>
        <div id="side-sticker">
            <a href="https://faqs.solar/about-the-programme">
                <span><b>Developed under</b></span>
                <div class="clearfix"></div>
                <img class="w95 mb-5" src="{{URL::asset('images/footer.png')}}">
            </a>
        </div>
    </section>
</aside>
