<header class="main-header">
  <!-- Logo -->
  <a href="javascript:void(0)" class="logo">
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg fs22" id="logo_txt">
      <img class="dash-logo" src="{{URL::asset('images/logo.png')}}">  
    </span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-state="1" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu MobileView">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="">Welcome  {{Auth::user()->name}}<b></b></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <p>
                <span><b>{{Auth::user()->name}}</b></span><br>
                <span></span><span id="datetime" style="font-size: 12px;line-height: 30px;"></span>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="p-5-10">
                <a href="{{url('state-agency/changepassword')}}" class="">Change Password</a>
              </div>
              <div class="p-5-10">
                <a title="Logout" class="req" href="{{ url('state-agency/logout') }}">Logout  <i class="fa fa-sign-out"></i></a>
              </div>
              <div>
                <span style="float: right !important;font-size: 10px;">
                  <a style="font-size: 10px!important;color: #404040!important;" href="{{ url('privacy-policy') }}">Privacy Policy</a>
                   | 
                  <a style="font-size: 10px!important;color: #404040!important;" href="{{ url('terms-of-use') }}">Terms of use</a>
                </span>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>