<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="{{ request()->is('admin/dashboard') ? 'active' : '' }}"><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="{{ request()->is('admin/city') ? 'active' : '' }}"><a href="{{url('admin/city')}}"><i class="fa fa-building"></i> <span>City</span></a></li>
            <li class="{{ request()->is('admin/route') ? 'active' : '' }}"><a href="{{url('admin/route')}}"><i class="fa fa-bus"></i> <span>Bus Routes</span></a></li>
            <li class="{{ request()->is('admin/school') ? 'active' : '' }}"><a href="{{url('admin/school')}}"><i class="fa fa-university"></i> <span>Schools</span></a></li>
            <li class="{{ request()->is('admin/pass_type') ? 'active' : '' }}"><a href="{{url('admin/pass_type')}}"><i class="fa fa-ticket"></i> <span>Passes</span></a></li>
            <li class="{{ request()->is('admin/students') ? 'active' : '' }}"><a href="{{url('admin/students')}}"><i class="fa fa-user"></i> <span>Students</span></a></li>
            <li class="treeview"><a href="{{url('admin/bookings')}}"><i class="fa fa-book"></i> <span>Bookings</span></a>
            <?php 
                $cities = DB::table('cities')->get();
            ?>
                <ul class="treeview-menu">
                    
                <?php foreach($cities as $city){?>
                    <li>
                        <a href="{{URL::to('/'.$prefix.'/getStudentsByCityId/'.$city->id)}}"><span><?php echo $city->name ?></span></a>
                    </li>
                <?php }?>
                </ul>
            </li>
            <li class="{{ request()->is('admin/payments') ? 'active' : '' }}"><a href="{{url('admin/payments')}}"><i class="fa fa-cc-stripe"></i> <span>Payments</span></a></li>
        </ul>
    </section>
</aside>
