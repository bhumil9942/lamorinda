@extends('layouts.home')
@section('content')
<div class="login-box text-center container-fluid">
    <h4>Password reset successfully</h4>
    <p>Now you can login in mobile application with your updated credentials</p>
</div>
@endsection
@section('styles')
<style>
    label.error{ bottom: initial; right: 0px; top: 35px; }
</style>
@endsection
