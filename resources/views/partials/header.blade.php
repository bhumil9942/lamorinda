<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="{{ URL::asset('front-font/fonts.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/slick.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('front-css/slick-theme.css') }}">
    <title>Lamorinda - Safety Information </title>
</head>

<body>

  <!-- Header Start -->
  <header class="header">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/homepage" target="_self"><img src="{{ URL::asset("images/logo.png")
             }}" alt=""></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto py-4 py-md-0">
                <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                  <a class="nav-link" href="/city">Buy Passes</a>
                </li>
                <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                  <a class="nav-link" href="/schedules-maps">Schedules & Maps</a>
                </li>
                <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                  <a class="nav-link" href="/community-links">Community Links</a>
                </li>
                <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                  <a class="nav-link" href="/public-meetings">Public Meetings</a>
                </li>
                <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                  <a class="nav-link" href="/safety-information">Safety Infomation</a>
                </li>
                <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                  <button class="loginButton">Login</button>
                  <div id="customer-login-from">
                    <div class="closeButton">
                      <i class="fa fa-times" aria-hidden="true"></i>
                    </div>

                    <form action="{{ route('getStudent') }}" method="POST" enctype="multipart/form-data">
                      @csrf
                      <div class="login">
                        <h4>Login</h4>
                        <div class="form">
                          <label for="pass-type">E-Mail Address* </label>
                          <br>
                          <input type="email" name="email" id="e-mail-address" required />
                          <label for="email">Password*</label>
                          <br>
                          <input type="password" name="password" id="e-mail-address" required />
                        </div>
                        <div class="forgo-password" style="display: block;">
                          <a href="#">Forgot your password?</a>
                        </div>
                        <div class="login-btn">
                          <input type="submit" class="btn-1" value="Login">
                        </div>
                        <div class="register-btn">
                          <a href="#" class="btn" id="register-btn">Register</a>
                        </div>
                      </div>
                    </form>

                    <!-- <div id="forget-password"  style="display: none;">
                      <h4>Forgot your password?</h4>
                      <div class="form">
                        <label for="pass-type">E-Mail Address</label>
                        <br>
                        <input type="email" name="e-mail-address" id="e-mail-address" >
                      </div>
                        <div class="login-btn">
                          <input type="submit" class="btn-1" value="Submit">
                        </div>
                        <div class="register-btn">
                          <a href="#" class="btn" id="register-btn">Register</a>
                        </div>  
                    </div> -->

                    <!-- <form method="POST" action="register"> -->
                    <form action="{{ route('addStudent') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                      <div class="register" style="display: none;">
                        <h4>Register</h4>
                        <div class="form">
                          <label for="fname">First Name*</label>
                          <br>
                          <input type="fname" name="fname" id="first-name" required />

                          <label for="lname">Last Name*</label>
                          <br>
                          <input type="lname" name="lname" id="last-name" required />

                          <label for="email">Email*</label>
                          <br>
                          <input type="email" name="email" id="email" required />

                          <label for="password">Password*</label>
                          <br>
                          <input type="password" name="password" id="e-mail-address" required />
                        </div>
                        <div class="register-btn">
                          <!-- <a href="#" class="btn-" id="register-btn1">Register</a> -->
                          <input type="submit" class="btn-1" value="Register">
                        </div>
                        <div class="login-btn">
                          <a href="#" class="btn" id="login-btn1">Login</a>
                        </div>
                      </div>
                    </form>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- Header End -->

  </body>

</html>