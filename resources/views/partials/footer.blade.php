<!-- Footer Start-->
<section class="footer" style="background-image: url('{{ asset('images/footer-bg.png')}}')">
    <div class="container">
      <div class="footer__links">
        <div class="row">
          <div class="col-lg-4">
            <div class="footer__column">
              <div class="footer__logo">
                <img src="{{ URL::asset("images/footer-logo.png")}}" class="img-fluid">
              </div>
              <div class="footer__contact">
                <p>Lamorida is a powerful booking platform
                  That lets you run a residential or commercial
                  Rental business online with no hassle</p>
                <br>
                <p>In publishing and graphic design, Lorem ipsum
                  is a placeholder text commonly used to
                  demonstrate the visual form of a document </p>
              </div>
            </div>
          </div>
          <div class="col-lg-1"></div>
          <div class="col-lg-3">
            <div class="footer__column">
              <h2 class="footer__heading">Explore</h2>
              <ul class="menu-link">
                <li><a href="/city"><i class="fas fa-chevron-right"></i>Buy Passes</a></li>
                <li><a href="/schedules-maps"><i class="fas fa-chevron-right"></i>Schedules & Map</a></li>
                <li><a href="/community-links"><i class="fas fa-chevron-right"></i>Community links</a></li>
                <li><a href="/public-meetings"><i class="fas fa-chevron-right"></i>Public Meeting</a></li>
                <li><a href="/safety-information"><i class="fas fa-chevron-right"></i>Safety information</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="footer__column">
              <h2 class="footer__heading">Get In Touch</h2>
              <div class="footer__contact-info">
                <div class="footer__contant">
                  <i class="fas fa-map-marker-alt"></i>
                  <p>3675 Mt Diablo Blvd,<br>
                    Lafayette, CA 94549</p>
                </div>
                <div class="footer__contant">
                  <i class="fas fa-phone-alt"></i>
                  <a href="#">(925) 299-3216</a>
                </div>
                <div class="footer__contant">
                  <i class="fas fa-envelope"></i>
                  <a href="#">wsimon@ci.lafayette.ca.us</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="footer__bottom">
      <p><a href="#">Lamorinda</a> All rights reserved - Designed 2021 </p>
    </div>
  </section>
  <!-- Fotter End -->

    <script src="{{ URL::asset('front-js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ URL::asset('front-js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('front-js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('front-js/main.js') }}"></script>
    <script src="{{ URL::asset('front-js/slick.min.js') }}"></script>