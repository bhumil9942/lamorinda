<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MNRE | @yield('title')</title>
		<!--Has all the sylesheets attached already!-->
		@include('elements/CommonElements/_head')
		<link rel="stylesheet" href="{{ URL::asset('css/adminstyle.css') }}">
		<!--Custom CSS or CSS Files for particular page-->
		@yield('styles')	
	</head>
	<body class="hold-transition skin-blue sidebar-mini" onload="startTime()">
		<div class="wrapper">
			<!--Application Header-->
			@include('elements.AgencyElements._header')
			<!--Application Sidebar-->
			@include('elements.AgencyElements._side')
			<!--Page Content Main-->
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				  <h1>
				    @yield('title')@yield('button')
				    <small></small>
				  </h1>
				</section>

				<!-- Main content -->
				<section class="content">
					<div id="loader" class="overlay">
						<div class="overlay__inner">
							<div class="overlay__content">
								<span class="spinner"></span>
								<div class="clearfix mb-15"></div>
								<span class="colorWhite">Processing, Please wait</span>
							</div>
						</div>
					</div>
					@yield('content')
				</section>
			</div>
			@if(Session::has('msg'))
				<div class="clearfix"></div>
				@include('modals.msgModal') 
			@endif
		</div>
		<!--Has all the scripts already attached-->  
		@include('elements/CommonElements/_base') 
		<!--Custom scripts for particular pages-->
		@yield('scripts')
  </body>
</html>