<div id="installerAssociationModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="{{url('/state-agency/associate-installer')}}" method="POST">
                @csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Associate installer</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="installerEmail" id="installerEmail" value="">
                    <h6>Installer with this email, is already registered.</h6>
                    <h6>Do you want to associate with him ?</h6>
                </div>
                <div class="modal-footer">
                   <button type="submit" class="btn btn-sm btn-info">Associate</button>
                   <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
