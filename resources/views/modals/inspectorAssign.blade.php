<div id="inspectorAssignModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="{{url('/state-agency/assign-inspector')}}" method="POST">
                @csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Associate installer</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Select Inspector</label>
                        <select name="inspector_id">
                            <option value="" selected disabled>Select Inspector</option>
                            @foreach ($inspectors as $inspector)
                                <option value="{{$inspector->id}}">{{$inspector->name}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="projectId" value="{{$project['id']}}">
                    </div>
                </div>
                <div class="modal-footer">
                   <button type="submit" class="btn btn-sm btn-info">Associate</button>
                   <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
